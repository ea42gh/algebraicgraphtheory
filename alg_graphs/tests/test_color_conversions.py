import pytest
import warnings

from alg_graphs.color_conversions import (
    convert_to_rgb,
    convert_to_rgb_batch,
    convert_tuple_or_numeric_to_rgb,
    convert_rgba_to_rgb,
    convert_rgb_to_rgb,
    convert_str_triplet_to_rgb,
    convert_triplet_to_rgb,
    convert_rgba_to_rgb,
    convert_hsl_to_rgb,
    convert_hex_to_rgb,
    convert_named_color_to_rgb,
    convert_rgb_to_hex,
)

def test_convert_to_rgb():
    assert convert_to_rgb("red") == "rgb(255,0,0)"
    assert convert_to_rgb("#ff0000") == "rgb(255,0,0)"
    assert convert_to_rgb("rgb(255,0,0)") == "rgb(255,0,0)"
    assert convert_to_rgb("rgba(255,0,0,0.5)") == "rgb(255,0,0)"
    assert convert_to_rgb("hsl(0,100%,50%)") == "rgb(255,0,0)"
    assert convert_to_rgb((255, 0, 0)) == "rgb(255,0,0)"
    assert convert_to_rgb(128) == "rgb(128,128,128)"
    with pytest.warns(UserWarning):
        assert convert_to_rgb({"r": 255}) == "rgb(0,0,0)"
        assert convert_to_rgb("123,255,0") == "rgb(0,0,0)"  # Bad format
        assert convert_to_rgb("invalid") == "rgb(0,0,0)"
        assert convert_to_rgb("#gggggg") == "rgb(0,0,0)"
        assert convert_to_rgb("hsl(-10,150%,200%)") == "rgb(0,0,0)"
        assert convert_to_rgb((260, -10, 500)) == "rgb(0,0,0)"
        assert convert_to_rgb("rgb(500,-20,300)") == "rgb(0,0,0)"
        assert convert_to_rgb(None) == "rgb(0,0,0)"

def test_convert_to_rgb_batch_identical_formats():
    """Test convert_to_rgb_batch with dictionaries containing identical color formats."""

    # Test with all named colors
    named_colors = {
        "color1": "red",
        "color2": "blue",
        "color3": "green"
    }
    expected_named = {
        "color1": "rgb(255,0,0)",
        "color2": "rgb(0,0,255)",
        "color3": "rgb(0,128,0)"
    }
    assert convert_to_rgb_batch(named_colors) == expected_named

    # Test with all RGB format
    rgb_colors = {
        "color1": "rgb(255,0,0)",
        "color2": "rgb(0,255,0)",
        "color3": "rgb(0,0,255)"
    }
    assert convert_to_rgb_batch(rgb_colors) == rgb_colors  # Should remain unchanged

    # Test with all RGBA format
    rgba_colors = {
        "color1": "rgba(255,0,0,1)",
        "color2": "rgba(0,255,0,0.5)",
        "color3": "rgba(0,0,255,0)"
    }
    expected_rgba = {
        "color1": "rgb(255,0,0)",
        "color2": "rgb(0,255,0)",
        "color3": "rgb(0,0,255)"
    }
    assert convert_to_rgb_batch(rgba_colors) == expected_rgba

    # Test with all HSL format
    hsl_colors = {
        "color1": "hsl(0,100%,50%)",
        "color2": "hsl(120,100%,50%)",
        "color3": "hsl(240,100%,50%)"
    }
    expected_hsl = {
        "color1": "rgb(255,0,0)",
        "color2": "rgb(0,255,0)",
        "color3": "rgb(0,0,255)"
    }
    assert convert_to_rgb_batch(hsl_colors) == expected_hsl

    # Test with all HEX format
    hex_colors = {
        "color1": "#ff0000",
        "color2": "#00ff00",
        "color3": "#0000ff"
    }
    expected_hex = {
        "color1": "rgb(255,0,0)",
        "color2": "rgb(0,255,0)",
        "color3": "rgb(0,0,255)"
    }
    assert convert_to_rgb_batch(hex_colors) == expected_hex

def test_convert_to_rgb_batch_unknown_colors():
    """Test that convert_to_rgb_batch issues warnings for unknown colors."""
    unknown_colors = {
        "color1": "unknown_color",
        "color2": "not_a_color",
        "color3": "???"
    }

    expected_output = {
        "color1": "rgb(0,0,0)",
        "color2": "rgb(0,0,0)",
        "color3": "rgb(0,0,0)"
    }

    with pytest.warns(UserWarning) as record:
        result = convert_to_rgb_batch(unknown_colors)

    # Check the number of warnings
    assert len(record) == 4, f"Expected 4 warnings, but got {len(record)}"

    # Validate the first warning (batch processing warning)
    assert any("Unknown color format:" in str(w.message) for w in record)

    # Validate individual conversion warnings
    for color in unknown_colors.values():
        assert any(f"Unable to convert '{color}' to RGB. Returning black." in str(w.message) for w in record)

    # Check the function output
    assert result == expected_output

def test_convert_to_rgb_batch_invalid_tuple_string():
    """Test that convert_to_rgb_batch handles a malformed tuple string correctly."""
    invalid_color = {1: "(1,(),2)"}
    expected_output = {1: "rgb(0,0,0)"}

    with pytest.warns(UserWarning) as record:
        result = convert_to_rgb_batch(invalid_color)

    # Ensure two warnings are issued
    assert len(record) == 2, f"Expected 2 warnings, but got {len(record)}"

    # Validate the first warning (batch processing warning)
    assert any("Unknown color format: (1,(),2)" in str(w.message) for w in record)

    # Validate the second warning (conversion failure)
    assert any("Unable to convert '(1,(),2)' to RGB. Returning black." in str(w.message) for w in record)

    # Ensure the function returns the expected output
    assert result == expected_output

def test_convert_to_rgb_batch():
    colors = {
        "color1": "red",
        "color2": "#00ff00",
        "color3": "rgb(0,0,255)",
        "color4": "rgba(255,255,0,0.5)",
        "color5": "hsl(240,100%,50%)",
        "color6": None
    }
    expected = {
        "color1": "rgb(255,0,0)",
        "color2": "rgb(0,255,0)",
        "color3": "rgb(0,0,255)",
        "color4": "rgb(255,255,0)",
        "color5": "rgb(0,0,255)",
        "color6": "rgb(0,0,0)"
    }
    with pytest.warns(UserWarning):
        assert convert_to_rgb_batch(colors) == expected
        assert convert_to_rgb_batch("invalid") == {}
        assert convert_to_rgb_batch({}) == {}

    colors = {"a": None, "b": None, "c": None}  # All values are None
    assert convert_to_rgb_batch(colors) == {}  # Should return an empty dictionary

def test_convert_tuple_or_numeric_to_rgb():
    assert convert_tuple_or_numeric_to_rgb((255, 255, 255)) == "rgb(255,255,255)"
    assert convert_tuple_or_numeric_to_rgb([0, 0, 0]) == "rgb(0,0,0)"
    assert convert_tuple_or_numeric_to_rgb(128) == "rgb(128,128,128)"
    assert convert_tuple_or_numeric_to_rgb((255.0, 0.0, 128.5)) == "rgb(255,0,128)"
    assert convert_tuple_or_numeric_to_rgb(128.9) == "rgb(129,129,129)"  # Rounded grayscale

    with pytest.raises(ValueError):
        convert_tuple_or_numeric_to_rgb((300, 0, 0))
        convert_tuple_or_numeric_to_rgb((-1, 0, 0))
        convert_tuple_or_numeric_to_rgb("string")


def test_convert_tuple_or_numeric_to_rgb_invalid_length():
    """Test that tuples/lists with invalid lengths raise ValueError."""

    with pytest.raises(ValueError, match="Invalid tuple/list length: \\(255, 255\\). Expected \\(R,G,B\\) or \\(R,G,B,A\\)."):
        convert_tuple_or_numeric_to_rgb((255, 255))  # Only 2 values

    with pytest.raises(ValueError, match="Invalid tuple/list length: \\(255, 255, 255, 255, 255\\). Expected \\(R,G,B\\) or \\(R,G,B,A\\)."):
        convert_tuple_or_numeric_to_rgb((255, 255, 255, 255, 255))  # Too many values

    with pytest.raises(ValueError, match="Invalid tuple/list length: \\[0\\]. Expected \\(R,G,B\\) or \\(R,G,B,A\\)."):
        convert_tuple_or_numeric_to_rgb([0])  # Only 1 value

    with pytest.raises(ValueError, match="Invalid tuple/list length: \\[255, 255, 255, 255, 255, 255\\]. Expected \\(R,G,B\\) or \\(R,G,B,A\\)."):
        convert_tuple_or_numeric_to_rgb([255, 255, 255, 255, 255, 255])  # Too many values


def test_convert_tuple_or_numeric_to_rgb_invalid_types():
    """Test that a tuple/list with invalid types raises ValueError."""
    with pytest.raises(ValueError, match="Invalid value types in \\(255, 'blue', 0\\). RGB values must be integers or floats."):
        convert_tuple_or_numeric_to_rgb((255, "blue", 0))

    with pytest.raises(ValueError, match="Invalid value types in \\[255, None, 0\\]. RGB values must be integers or floats."):
        convert_tuple_or_numeric_to_rgb([255, None, 0])

    with pytest.raises(ValueError, match="Invalid value types in \\(255, 128, \\[0\\]\\). RGB values must be integers or floats."):
        convert_tuple_or_numeric_to_rgb((255, 128, [0]))

    with pytest.raises(ValueError, match="Invalid value types in \\(255, 128, \\{0: 'black'\\}\\). RGB values must be integers or floats."):
        convert_tuple_or_numeric_to_rgb((255, 128, {0: "black"}))

    with pytest.raises(ValueError, match="Unsupported input type: blue. Expected tuple, list, int, or float."):
        convert_tuple_or_numeric_to_rgb("blue")  # String input

    with pytest.raises(ValueError, match="Unsupported input type: None. Expected tuple, list, int, or float."):
        convert_tuple_or_numeric_to_rgb(None)  # None input

    with pytest.raises(ValueError, match="Unsupported input type: \\{255: 'red', 0: 'green'\\}. Expected tuple, list, int, or float."):
        convert_tuple_or_numeric_to_rgb({255: "red", 0: "green"})  # Dictionary input

    with pytest.raises(ValueError, match="Unsupported input type: True. Expected tuple, list, int, or float."):
        convert_tuple_or_numeric_to_rgb(True)  # Boolean input

    with pytest.raises(ValueError):
        convert_tuple_or_numeric_to_rgb({255, 0, 0})  # Set input

def test_convert_tuple_or_numeric_to_rgb_grayscale():
    """Test that single numeric values are correctly converted to grayscale RGB."""

    # Valid grayscale values (int and float)
    assert convert_tuple_or_numeric_to_rgb(0) == "rgb(0,0,0)"
    assert convert_tuple_or_numeric_to_rgb(128) == "rgb(128,128,128)"
    assert convert_tuple_or_numeric_to_rgb(255) == "rgb(255,255,255)"
    assert convert_tuple_or_numeric_to_rgb(200.5) == "rgb(200,200,200)"  # Float should round down

    # Out-of-range values should raise ValueError
    with pytest.raises(ValueError, match="Gray-scale value -10 out of range \\(0-255\\)."):
        convert_tuple_or_numeric_to_rgb(-10)

    with pytest.raises(ValueError, match="Gray-scale value 300 out of range \\(0-255\\)."):
        convert_tuple_or_numeric_to_rgb(300)

    with pytest.raises(ValueError, match="Gray-scale value -1.5 out of range \\(0-255\\)."):
        convert_tuple_or_numeric_to_rgb(-1.5)

    with pytest.raises(ValueError, match="Gray-scale value 256.2 out of range \\(0-255\\)."):
        convert_tuple_or_numeric_to_rgb(256.2)

def test_convert_rgba_to_rgb():
    assert convert_rgba_to_rgb("rgba(255,0,0,1)") == "rgb(255,0,0)"
    assert convert_rgba_to_rgb("rgba(0,255,0,0.5)") == "rgb(0,255,0)"
    assert convert_rgba_to_rgb("rgba(255,0,0,0)") == "rgb(255,0,0)"
    with pytest.raises(ValueError):
        convert_rgba_to_rgb("rgba(255,0,0)")
        convert_rgba_to_rgb("rgba(255,0,0,2)")
        convert_rgba_to_rgb("rgba(255,0,0,-1)")
        convert_rgba_to_rgb("rgba(255,0,0,none)")  # Malformed
        convert_rgba_to_rgb("rgba(-255,0,0,0.5)")  # Negative RGB value

def test_convert_rgba_to_rgb_out_of_range_rgb():
    """Test that RGB values outside the range 0-255 raise ValueError."""

    with pytest.raises(ValueError, match="RGB values out of range in 'rgba\\(300,0,0,1\\)': \\(300, 0, 0\\). Must be between 0–255."):
        convert_rgba_to_rgb("rgba(300,0,0,1)")  # R > 255

    with pytest.raises(ValueError, match="RGB values out of range in 'rgba\\(-10,50,50,0.5\\)': \\(-10, 50, 50\\). Must be between 0–255."):
        convert_rgba_to_rgb("rgba(-10,50,50,0.5)")  # R < 0

    with pytest.raises(ValueError, match="RGB values out of range in 'rgba\\(0,260,0,0.5\\)': \\(0, 260, 0\\). Must be between 0–255."):
        convert_rgba_to_rgb("rgba(0,260,0,0.5)")  # G > 255

    with pytest.raises(ValueError, match="RGB values out of range in 'rgba\\(0,0,-5,1\\)': \\(0, 0, -5\\). Must be between 0–255."):
        convert_rgba_to_rgb("rgba(0,0,-5,1)")  # B < 0

    with pytest.raises(ValueError, match="RGB values out of range in 'rgba\\(500,500,500,0.5\\)': \\(500, 500, 500\\). Must be between 0–255."):
        convert_rgba_to_rgb("rgba(500,500,500,0.5)")  # All values > 255

    with pytest.raises(ValueError, match=r"Alpha value out of range in 'rgba\(255,0,0,1.5\)': 1.5. Must be between 0–1."):
        convert_rgba_to_rgb("rgba(255,0,0,1.5)")  # Alpha > 1

    with pytest.raises(ValueError, match=r"Alpha value out of range in 'rgba\(255,0,0,-0.2\)': -0.2. Must be between 0–1."):
        convert_rgba_to_rgb("rgba(255,0,0,-0.2)")  # Alpha < 0

    with pytest.raises(ValueError, match=r"Alpha value out of range in 'rgba\(0,255,0,2\)': 2.0. Must be between 0–1."):
        convert_rgba_to_rgb("rgba(0,255,0,2)")  # Alpha = 2 (invalid)

    with pytest.raises(ValueError, match=r"Alpha value out of range in 'rgba\(100,100,100,-1\)': -1.0. Must be between 0–1."):
        convert_rgba_to_rgb("rgba(100,100,100,-1)")  # Alpha = -1 (invalid)


def test_convert_rgb_to_rgb():
    assert convert_rgb_to_rgb("rgb(255,0,0)") == "rgb(255,0,0)"
    with pytest.raises(ValueError):
        convert_rgb_to_rgb("rgb(300,0,0)")
        convert_rgb_to_rgb("rgb(-10,0,0)")
        convert_rgb_to_rgb("invalid")
        convert_rgb_to_rgb("rgb(255,255)")
        convert_rgb_to_rgb("rgb(,,)")
    with pytest.raises(ValueError, match="Invalid RGB format: 255,0,0. Expected 'rgb\\(R,G,B\\)'."):
        convert_rgb_to_rgb("255,0,0")  # Missing "rgb()"

    with pytest.raises(ValueError):
        convert_rgb_to_rgb("rgb(255,0)")  # Missing one value

    with pytest.raises(ValueError):
        convert_rgb_to_rgb("rgb(255,0,0,128)")  # Extra value

    with pytest.raises(ValueError):
        convert_rgb_to_rgb("rgb(255,red,0)")  # Contains a string

    with pytest.raises(ValueError):
        convert_rgb_to_rgb("rgb 255,0,0")  # Missing parentheses

def test_convert_hsl_to_rgb():
    assert convert_hsl_to_rgb("hsl(0,100%,50%)") == "rgb(255,0,0)"
    assert convert_hsl_to_rgb("hsl(120,100%,50%)") == "rgb(0,255,0)"
    assert convert_hsl_to_rgb("hsl(240,100%,50%)") == "rgb(0,0,255)"
    with pytest.raises(ValueError):
        convert_hsl_to_rgb("hsl(400,100%,50%)")
        convert_hsl_to_rgb("hsl(0,150%,50%)")
        convert_hsl_to_rgb("hsl(180,100,50)")
    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(not_a_number,50%,50%\)"):
        convert_hsl_to_rgb("hsl(not_a_number,50%,50%)")  # Invalid first value

    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(120,50\)"):
        convert_hsl_to_rgb("hsl(120,50)")  # Missing third value

    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(120,50%,\)"):
        convert_hsl_to_rgb("hsl(120,50%,)")  # Missing third value after comma

    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(120,,50%\)"):
        convert_hsl_to_rgb("hsl(120,,50%)")  # Missing second value

    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(120,50,50\)"):
        convert_hsl_to_rgb("hsl(120,50,50)")  # Missing percent signs on 2nd and 3rd values

    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl120,50%,50%\)"):
        convert_hsl_to_rgb("hsl120,50%,50%)")  # Missing opening parenthesis

    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(120,50%,50%"):
        convert_hsl_to_rgb("hsl(120,50%,50%")  # Missing closing parenthesis

    with pytest.raises(ValueError, match=r"Invalid HSL color format: 120,50%,50%"):
        convert_hsl_to_rgb("120,50%,50%")  # Missing `hsl()` wrapper

    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(120, 50, 50%\)"):
        convert_hsl_to_rgb("hsl(120, 50, 50%)")  # Second value missing `%`

def test_convert_hex_to_rgb_valid():
    """Test conversion of valid 6-character HEX colors."""
    assert convert_hex_to_rgb("#ff0000") == "rgb(255,0,0)"
    assert convert_hex_to_rgb("#00ff00") == "rgb(0,255,0)"
    assert convert_hex_to_rgb("#0000ff") == "rgb(0,0,255)"
    assert convert_hex_to_rgb("#abcdef") == "rgb(171,205,239)"
    assert convert_hex_to_rgb("#123456") == "rgb(18,52,86)"
    assert convert_hex_to_rgb("#ffffff") == "rgb(255,255,255)"
    assert convert_hex_to_rgb("#000000") == "rgb(0,0,0)"

def test_convert_hex_to_rgb_shorthand():
    """Test conversion of 3-character shorthand HEX colors."""
    assert convert_hex_to_rgb("#f00") == "rgb(255,0,0)"   # Expands to "#ff0000"
    assert convert_hex_to_rgb("#0f0") == "rgb(0,255,0)"   # Expands to "#00ff00"
    assert convert_hex_to_rgb("#00f") == "rgb(0,0,255)"   # Expands to "#0000ff"
    assert convert_hex_to_rgb("#abc") == "rgb(170,187,204)"  # Expands to "#aabbcc"
    assert convert_hex_to_rgb("#fff") == "rgb(255,255,255)"  # Expands to "#ffffff"
    assert convert_hex_to_rgb("#000") == "rgb(0,0,0)"  # Expands to "#000000"

def test_convert_hex_to_rgb_case_insensitivity():
    """Test that HEX input is case insensitive."""
    assert convert_hex_to_rgb("#ABCDEF") == "rgb(171,205,239)"
    assert convert_hex_to_rgb("#abcdef") == "rgb(171,205,239)"
    assert convert_hex_to_rgb("#AaBbCc") == "rgb(170,187,204)"  # Mixed case

def test_convert_hex_to_rgb_no_hash():
    """Test HEX values provided without '#'."""
    assert convert_hex_to_rgb("ff0000") == "rgb(255,0,0)"
    assert convert_hex_to_rgb("00ff00") == "rgb(0,255,0)"
    assert convert_hex_to_rgb("abcdef") == "rgb(171,205,239)"

def test_convert_hex_to_rgb_invalid():
    """Test invalid HEX inputs that should raise ValueError."""
    with pytest.raises(ValueError, match="Invalid HEX color: xyzxyz"):
        convert_hex_to_rgb("#xyzxyz")

    with pytest.raises(ValueError, match="Invalid HEX color: 1234"):
        convert_hex_to_rgb("#1234")  # Invalid length

    with pytest.raises(ValueError, match="Invalid HEX color: 1234567"):
        convert_hex_to_rgb("#1234567")  # Too long

    with pytest.raises(ValueError, match="Invalid HEX color: g12h34"):
        convert_hex_to_rgb("#g12h34")  # Contains non-hex characters

    with pytest.raises(ValueError, match="Invalid HEX color: "):
        convert_hex_to_rgb("#")  # Empty after stripping '#'

    with pytest.raises(ValueError, match="Invalid HEX color: "):
        convert_hex_to_rgb("")  # Completely empty string

    with pytest.raises(ValueError, match="Invalid HEX color: None"):
        convert_hex_to_rgb(None)  # None input

    with pytest.raises(ValueError, match="Invalid HEX color: 123"):
        convert_hex_to_rgb("123")  # No '#' but assumed invalid format

def test_convert_hex_to_rgb_whitespace():
    """Test HEX strings with leading/trailing whitespace."""
    assert convert_hex_to_rgb("  #ff0000  ") == "rgb(255,0,0)"
    assert convert_hex_to_rgb("\t#00ff00\n") == "rgb(0,255,0)"
    assert convert_hex_to_rgb("\n#abcdef\t") == "rgb(171,205,239)"

def test_convert_hsl_to_rgb_grayscale():
    """Test that HSL with saturation 0% converts to grayscale correctly."""
    
    assert convert_hsl_to_rgb("hsl(0,0%,50%)") == "rgb(127,127,127)"  # Middle gray
    assert convert_hsl_to_rgb("hsl(120,0%,100%)") == "rgb(255,255,255)"  # Pure white
    assert convert_hsl_to_rgb("hsl(240,0%,0%)") == "rgb(0,0,0)"  # Pure black

def test_hue_to_rgb_t():
    """Test that hue_to_rgb correctly handles t > 1 by wrapping it with t -= 1."""
    
    # H = 300 should cause t > 1 for some calculations
    assert convert_hsl_to_rgb("hsl(300,100%,50%)") == "rgb(255,0,255)"  # Magenta

    assert convert_hsl_to_rgb("hsl(60,100%,50%)") == "rgb(255,255,0)"  # Yellow


def test_convert_hsl_to_rgb_out_of_range_saturation_lightness():
    """Test that out-of-range saturation and lightness values raise a ValueError."""

    # Out-of-range saturation values
    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(120,-10%,50%\)"):
        convert_hsl_to_rgb("hsl(120,-10%,50%)")  # Saturation < 0%

    with pytest.raises(ValueError, match=r"Saturation 120% is out of range \(0-100%\)."):
        convert_hsl_to_rgb("hsl(120,120%,50%)")  # Saturation > 100% (should pass format check)

    # Out-of-range lightness values
    with pytest.raises(ValueError, match=r"Invalid HSL color format: hsl\(240,50%,-5%\)"):
        convert_hsl_to_rgb("hsl(240,50%,-5%)")  # Lightness < 0% (invalid format)

    with pytest.raises(ValueError, match=r"Lightness 150% is out of range \(0-100%\)."):
        convert_hsl_to_rgb("hsl(240,50%,150%)")  # Lightness > 100% (should pass format check)


def test_convert_named_color_to_rgb():
    assert convert_named_color_to_rgb("red") == "rgb(255,0,0)"
    assert convert_named_color_to_rgb("blue") == "rgb(0,0,255)"
    assert convert_named_color_to_rgb("green") == "rgb(0,128,0)"
    with pytest.raises(ValueError):
        convert_named_color_to_rgb("not_a_color")
        convert_named_color_to_rgb(123)

def test_convert_str_triplet_to_rgb_valid():
    """Test valid (R,G,B) triplet conversions."""
    assert convert_str_triplet_to_rgb("(255, 0, 0)") == (255, 0, 0)
    assert convert_str_triplet_to_rgb("( 0 , 255 , 0 )") == (0, 255, 0)
    assert convert_str_triplet_to_rgb("( 0 , 0 , 255 )") == (0, 0, 255)
    assert convert_str_triplet_to_rgb("( 128, 128, 128 )") == (128, 128, 128)
    assert convert_str_triplet_to_rgb("(0,0,0)") == (0, 0, 0)
    assert convert_str_triplet_to_rgb("( 255 , 255 , 255 )") == (255, 255, 255)

def test_convert_str_triplet_to_rgb_out_of_range():
    """Test values that are outside the valid range (0-255)."""
    with pytest.raises(ValueError, match="RGB values out of range: \\(-10, 50, 50\\). Must be between 0-255."):
        convert_str_triplet_to_rgb("(-10, 50, 50)")

    with pytest.raises(ValueError, match="RGB values out of range: \\(300,0,0\\). Must be between 0-255."):
        convert_str_triplet_to_rgb("(300,0,0)")

    with pytest.raises(ValueError, match="RGB values out of range: \\(255,255,500\\). Must be between 0-255."):
        convert_str_triplet_to_rgb("(255,255,500)")

def test_convert_str_triplet_to_rgb_invalid_inputs():
    """Test non-string inputs that should raise ValueError."""
    with pytest.raises(ValueError, match="Invalid RGB format: None. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb(None)

    with pytest.raises(ValueError, match="Invalid RGB format: 123. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb(123)

    with pytest.raises(ValueError, match="Invalid RGB format: \\[255, 0, 0\\]. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb([255, 0, 0])

    with pytest.raises(ValueError, match="Invalid RGB format: \\{255: 'red', 0: 'green', 1: 'blue'\\}. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb({255: "red", 0: "green", 1: "blue"})

    with pytest.raises(ValueError, match=r"Invalid RGB format: rgb\(255,255,255\). Expected '\(R,G,B\)'."):
        convert_str_triplet_to_rgb("rgb(255,255,255)")  # Wrong format (should not include 'rgb')

    with pytest.raises(ValueError, match=r"Invalid RGB format: \(255,,0\). Expected '\(R,G,B\)'."):
        convert_str_triplet_to_rgb("(255,,0)")  # Missing middle value

    with pytest.raises(ValueError, match=r"Invalid RGB format: \(255,red,0\). Expected '\(R,G,B\)'."):
        convert_str_triplet_to_rgb("(255,red,0)")  # Non-numeric value

def test_convert_str_triplet_to_rgb_non_string_input():
    """Test non-string inputs that should raise a ValueError."""

    with pytest.raises(ValueError, match="Invalid RGB format: 123. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb(123)

    with pytest.raises(ValueError, match="Invalid RGB format: None. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb(None)

    with pytest.raises(ValueError, match="Invalid RGB format: \\[255, 255, 255\\]. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb([255, 255, 255])  # This now expects ValueError

    with pytest.raises(ValueError, match="Invalid RGB format: \\{255: 'red', 128: 'green', 0: 'blue'\\}. Expected '\\(R,G,B\\)'."):
        convert_str_triplet_to_rgb({255: "red", 128: "green", 0: "blue"})


def test_convert_triplet_to_rgb_valid():
    """Test conversion of valid (R, G, B) tuples."""
    assert convert_triplet_to_rgb((255, 0, 0)) == "rgb(255,0,0)"
    assert convert_triplet_to_rgb((0, 255, 0)) == "rgb(0,255,0)"
    assert convert_triplet_to_rgb((0, 0, 255)) == "rgb(0,0,255)"
    assert convert_triplet_to_rgb((128, 128, 128)) == "rgb(128,128,128)"
    assert convert_triplet_to_rgb((255, 255, 255)) == "rgb(255,255,255)"
    assert convert_triplet_to_rgb((0, 0, 0)) == "rgb(0,0,0)"

def test_convert_triplet_to_rgb_invalid_format():
    """Test invalid formats that should raise ValueError."""
    with pytest.raises(ValueError, match="Invalid format: \\[255, 0, 0\\]. Expected a tuple of \\(R,G,B\\)."):
        convert_triplet_to_rgb([255, 0, 0])  # List instead of tuple

    with pytest.raises(ValueError, match="Invalid format: 255. Expected a tuple of \\(R,G,B\\)."):
        convert_triplet_to_rgb(255)  # Integer instead of tuple

    with pytest.raises(ValueError, match="Invalid format: \\(255, 255\\). Expected a tuple of \\(R,G,B\\)."):
        convert_triplet_to_rgb((255, 255))  # Tuple of length 2

    with pytest.raises(ValueError, match="Invalid format: \\(255, 255, 255, 255\\). Expected a tuple of \\(R,G,B\\)."):
        convert_triplet_to_rgb((255, 255, 255, 255))  # Tuple of length 4

def test_convert_triplet_to_rgb_invalid_values():
    """Test tuples containing non-integer values."""
    with pytest.raises(ValueError, match="Invalid values: \\(255, 'blue', 0\\). RGB values must be integers."):
        convert_triplet_to_rgb((255, "blue", 0))  # String in tuple

    with pytest.raises(ValueError, match="Invalid values: \\(255, 128, 128.5\\). RGB values must be integers."):
        convert_triplet_to_rgb((255, 128, 128.5))  # Float in tuple

    with pytest.raises(ValueError, match="Invalid values: \\(None, 255, 255\\). RGB values must be integers."):
        convert_triplet_to_rgb((None, 255, 255))  # None in tuple

    with pytest.raises(ValueError, match="Invalid values: \\(\\{255: 'red'\\}, 0, 0\\). RGB values must be integers."):
        convert_triplet_to_rgb(({255: "red"}, 0, 0))  # Dictionary in tuple

def test_convert_triplet_to_rgb_out_of_range():
    """Test tuples with out-of-range values."""
    with pytest.raises(ValueError, match="RGB values out of range: \\(300, 0, 0\\). Must be between 0-255."):
        convert_triplet_to_rgb((300, 0, 0))  # R > 255

    with pytest.raises(ValueError, match="RGB values out of range: \\(-10, 50, 50\\). Must be between 0-255."):
        convert_triplet_to_rgb((-10, 50, 50))  # R < 0

    with pytest.raises(ValueError, match="RGB values out of range: \\(0, 0, 256\\). Must be between 0-255."):
        convert_triplet_to_rgb((0, 0, 256))  # B > 255

    with pytest.raises(ValueError, match="RGB values out of range: \\(255, 255, -1\\). Must be between 0-255."):
        convert_triplet_to_rgb((255, 255, -1))  # B < 0

import pytest

def test_rgb_to_hex_invalid_inputs():
    """Test that rgb_to_hex raises ValueError on invalid inputs."""
    
    with pytest.raises(ValueError, match="Invalid RGB format: 255,0,0"):
        convert_rgb_to_hex("255,0,0")  # String instead of tuple

    with pytest.raises(ValueError, match="Invalid RGB format: \\(255, 0\\)"):
        convert_rgb_to_hex((255, 0))  # Missing one value

    with pytest.raises(ValueError, match="Invalid RGB format: \\(255, 0, 0, 128\\)"):
        convert_rgb_to_hex((255, 0, 0, 128))  # Extra value

    with pytest.raises(ValueError, match="RGB values out of range or invalid: \\(255, -5, 300\\)"):
        convert_rgb_to_hex((255, -5, 300))  # Out-of-range values

    with pytest.raises(ValueError, match="RGB values out of range or invalid: \\(255, 'blue', 0\\)"):
        convert_rgb_to_hex((255, "blue", 0))  # Non-integer value

def test_rgb_to_hex_valid_execution():
    """Test that the function correctly formats RGB tuples to HEX."""
    assert convert_rgb_to_hex((0, 0, 0)) == "#000000"  # Black
    assert convert_rgb_to_hex((255, 255, 255)) == "#ffffff"  # White
    assert convert_rgb_to_hex((255, 0, 0)) == "#ff0000"  # Red
    assert convert_rgb_to_hex((0, 255, 0)) == "#00ff00"  # Green
    assert convert_rgb_to_hex((0, 0, 255)) == "#0000ff"  # Blue
    assert convert_rgb_to_hex((127, 127, 127)) == "#7f7f7f"  # Mid-gray
    assert convert_rgb_to_hex((4, 128, 255)) == "#0480ff"  # Test leading zeros
