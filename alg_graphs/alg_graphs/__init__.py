"""
alg_graphs: A Python package for graph algorithms and visualization.
"""

from .state_transitions import ParamStateMachine
from .graphvisualization import GraphVisualization

from .graphproperties import (
    compute_node_centrality,
    compute_graph_diameter,
    compute_shortest_paths,
    remove_isolated_nodes,
    convert_to_unweighted_graph,
    assign_random_edge_weights,
    assign_node_community_labels  
)


from .assign_colors_and_sizes import (
    all_values_equal, normalize_values, compute_node_size, compute_edge_width, assign_colors
)

from .color_conversions import (
    convert_to_rgb,
    convert_to_rgb_batch,
    convert_tuple_or_numeric_to_rgb,
    convert_rgba_to_rgb,
    convert_rgb_to_rgb,
    convert_str_triplet_to_rgb,
    convert_triplet_to_rgb,
    convert_rgba_to_rgb,
    convert_hsl_to_rgb,
    convert_hex_to_rgb,
    convert_named_color_to_rgb,
    convert_rgb_to_hex
)

__all__ = [
    "all_values_equal", "normalize_values", "compute_node_size", "compute_edge_width", "assign_colors",
    "convert_to_rgb",
    "convert_to_rgb_batch",
    "convert_tuple_or_numeric_to_rgb",
    "convert_rgba_to_rgb",
    "convert_rgb_to_rgb",
    "convert_str_triplet_to_rgb",
    "convert_triplet_to_rgb",
    "convert_rgba_to_rgb",
    "convert_hsl_to_rgb",
    "convert_hex_to_rgb",
    "convert_named_color_to_rgb",
    "convert_rgb_to_hex",
    "compute_node_centrality",
    "compute_graph_diameter",
    "compute_shortest_paths",
    "remove_isolated_nodes",
    "convert_to_unweighted_graph",
    "assign_random_edge_weights",
    "assign_node_community_labels",
    "ParamStateMachine",
    "GraphVisualization",
]

# ✅ Import functions into the package namespace (this ensures `dir(alg_graphs)` lists them)
globals().update({name: eval(name) for name in __all__})
#print(__all__)
