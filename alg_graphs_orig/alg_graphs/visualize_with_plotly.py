# ==================================================================================================
# Original version: https://gist.github.com/mogproject/50668d3ca60188c50e6ef3f5f3ace101
#                   https://users.cs.utah.edu/~yos/2021/02/02/plotly-python.html
# added arrows, signal displays and the PlotlyDisplay class: ea42gh
# ==================================================================================================
import holoviews as hv; hv.extension('bokeh', 'plotly', 'matplotlib', logo=False)
import panel as pn;     pn.extension('plotly', 'katex', 'mathjax')
import param

from collections import defaultdict
import networkx as nx
import plotly.graph_objects as go
from typing import Any, List, Dict, Tuple, Union, Callable
import numpy as np

# ==================================================================================================
Vertex = Any
Edge   = Tuple[Vertex, Vertex]
Num    = Union[int, float]

# ==================================================================================================
class GraphVisualization:
    def __init__(
        self,
        G:                      nx.Graph,
        pos:                    Dict[Vertex, Union[Tuple[Num, Num], Tuple[Num, Num, Num]]],
        node_text:              Union[Dict[Vertex, str], Callable]                              = None,
        node_text_position:     Union[Dict[Vertex, str], Callable, str]                         = None,
        node_text_font_color:   Union[Dict[Vertex, str], Callable, str]                         = None,
        node_text_font_family:  Union[Dict[Vertex, str], Callable, str]                         = None,
        node_text_font_size:    Union[Dict[Vertex, Num], Callable, str]                         = None,
        node_size:              Union[Dict[Vertex, Num], Callable, Num]                         = None,
        node_color:             Union[Dict[Vertex, Union[str, Num]], Callable, Union[str, Num]] = None,
        node_color_range:       Tuple[Num,Num]                                                  = None,
        node_border_width:      Union[Dict[Vertex, Num], Callable, Num]                         = None,
        node_border_color:      Union[Dict[Vertex, str], Callable, str]                         = None,
        node_opacity:           Num                                                             = None,
        edge_width:             Union[Dict[Edge, Num], Callable, Num]                           = None,
        edge_color:             Union[Dict[Edge, str], Callable, str]                           = None,
        edge_opacity:           Num                                                             = None,
        arrow_spec:             Union[Num,Tuple[Num,Num],Tuple[Num,Num,Num]]                    = None,
    ):
        # check dimensions
        if   all(len(pos.get(v, [])) == 2 for v in G): self.is_3d = False
        elif all(len(pos.get(v, [])) == 3 for v in G): self.is_3d = True
        else:                                          raise ValueError

        # default settings
        self.default_settings = dict(
            node_text             = str,  # show node label
            node_text_position    = "middle center",
            node_text_font_color  = '#000000',
            node_text_font_family = 'Arial',
            node_text_font_size   = 14,
            node_size             = 10 if self.is_3d else 18,
            node_color            = '#fcfcfc',
            node_color_range      = None,
            node_border_width     = 2,
            node_border_color     = '#333333',
            node_opacity          = 0.8,
            edge_width            = 4 if self.is_3d else 2,
            edge_color            = '#808080',
            edge_opacity          = 0.8,
            arrow_spec            = None,
        )

        # save settings
        self.G                         = G
        self.pos                       = pos
        self.node_text                 = node_text
        self.node_text_position        = node_text_position
        self.node_text_font_color      = node_text_font_color
        self.node_text_font_family     = node_text_font_family
        self.node_text_font_size       = node_text_font_size
        self.node_size                 = node_size
        self.node_color                = node_color
        self.node_color_range          = node_color_range,
        self.node_border_width         = node_border_width
        self.node_border_color         = node_border_color
        self.node_opacity              = node_opacity
        self.edge_width                = edge_width
        self.edge_color                = edge_color
        self.edge_opacity              = edge_opacity
        self.arrow_spec                = arrow_spec

    def _get_edge_traces(self) -> List[Union[go.Scatter, go.Scatter3d]]:
        # group all edges by (color, width)
        groups = defaultdict(list)

        for edge in self.G.edges():
            color                   = self._get_setting('edge_color', edge)
            width                   = self._get_setting('edge_width', edge)
            groups[(color, width)] += [edge]

        # process each group
        traces = []
        for (color, width), edges in groups.items():
            x, y, z = [], [], []
            for v, u in edges:
                x += [self.pos[v][0], self.pos[u][0], None]
                y += [self.pos[v][1], self.pos[u][1], None]
                if self.is_3d:
                    z += [self.pos[v][2], self.pos[u][2], None]

            params = dict(
                x          = x,
                y          = y,
                mode       = 'lines',
                hoverinfo  = 'none',
                line       = dict(color=color, width=width),
                opacity    = self._get_setting('edge_opacity'),
            )

            if (not self.is_3d) and (self.arrow_spec is not None):
                arrow_size = self.arrow_spec[0] if isinstance(self.arrow_spec,tuple) else self.arrow_spec
                params |= { 'mode': 'lines+markers',
                            'marker': dict( symbol="arrow", size=arrow_size, angleref="previous", ),
                }

            traces += [go.Scatter3d(z=z, **params) if self.is_3d else go.Scatter(**params)]

            if self.is_3d and (self.arrow_spec is not None):
                arrow_sizeref = 0.5
                if isinstance(self.arrow_spec,tuple):
                    arrow_tip_ratio      = self.arrow_spec[1] #  0.1
                    arrow_starting_ratio = self.arrow_spec[0] # 0.98
                    if len(self.arrow_spec)==3:
                        arrow_sizeref= self.arrow_spec[2]
                else:
                    arrow_tip_ratio      = 0.1
                    arrow_starting_ratio = self.arrow_spec

                cx, cy, cz, cu, cv, cw = [],[],[], [],[],[]
                for u,v in edges:
                    px =  [self.pos[v][0], self.pos[u][0]]
                    py =  [self.pos[v][1], self.pos[u][1]]
                    pz =  [self.pos[v][2], self.pos[u][2]]

                    dx = px[1] - px[0]
                    dy = py[1] - py[0]
                    dz = pz[1] - pz[0]
                    dp = np.linalg.norm([dx,dy,dz])

                    cx += [px[0] + arrow_starting_ratio*dx]
                    cy += [py[0] + arrow_starting_ratio*dy]
                    cz += [pz[0] + arrow_starting_ratio*dz]

                    cu += [arrow_tip_ratio*dx/dp]
                    cv += [arrow_tip_ratio*dy/dp]
                    cw += [arrow_tip_ratio*dz/dp]

                traces += [ go.Cone( x=cx, y=cy, z=cz, u=cu, v=cv, w=cw, colorscale=[[0, color], [1, color]], opacity = self._get_setting('edge_opacity'),
                                     showlegend=False, showscale=False,     sizemode="absolute", sizeref=arrow_sizeref,
                )]
        
        showlegend=False,
        showscale=False,

        return traces

    def _get_node_trace(self, showlabel, colorscale, showscale, colorbar_title, reversescale) -> Union[go.Scatter, go.Scatter3d]:
        x, y, z = [], [], []
        for v in self.G.nodes():
            x += [self.pos[v][0]]
            y += [self.pos[v][1]]
            if self.is_3d:
                z += [self.pos[v][2]]

        params = dict(
            x         = x,
            y         = y,
            mode      = 'markers' + ('+text' if showlabel else ''),
            hoverinfo = 'text',
            marker = dict(
                showscale        = showscale,
                colorscale       = colorscale,
                reversescale     = reversescale,
                color            = self._get_setting('node_color'),
                size             = self._get_setting('node_size'),
                line_width       = self._get_setting('node_border_width'),
                line_color       = self._get_setting('node_border_color'),
                colorbar         = dict(
                                     thickness    = 15,
                                     title        = colorbar_title,
                                     xanchor      = 'left',
                                     titleside    = 'right'
                ),
            ),
            text                 = self._get_setting('node_text'),
            textfont             = dict(
                color            = self._get_setting('node_text_font_color'),
                family           = self._get_setting('node_text_font_family'),
                size             = self._get_setting('node_text_font_size')
            ),
            textposition         = self._get_setting('node_text_position'),
            opacity              = self._get_setting('node_opacity'),
        )
        if len(self.node_color_range) == 1: self.node_color_range = self.node_color_range[0] # ???
        if self.node_color_range == (None,) : self.node_color_range = None # ???
        if self.node_color_range is not None:
            params['marker']['cmin'] = self.node_color_range[0]
            params['marker']['cmax'] = self.node_color_range[1]

        trace = go.Scatter3d(z=z, **params) if self.is_3d else go.Scatter(**params)
        return trace

    def _get_setting(self, setting_name, edge=None):
        default_setting = self.default_settings.get(setting_name)
        def_func        = default_setting if callable(default_setting) else lambda x: default_setting
        setting         = self.__dict__.get(setting_name)

        if edge is None:  # vertex-specific
            if setting is None:  # default is used
                if callable(default_setting):  # default is a function
                    return [def_func(v) for v in self.G.nodes()]
                else:  # default is a constant
                    return default_setting
            elif callable(setting):  # setting is a function
                return [setting(v) for v in self.G.nodes()]
            elif isinstance(setting, dict):  # setting is a dict
                return [setting.get(v, def_func(v)) for v in self.G.nodes()]
            else:  # setting is a constant
                return setting
        else:  # edge-specific
            if setting is None:  # default is used
                return def_func(edge)
            elif callable(setting):  # setting is a function
                return setting(edge)
            elif isinstance(setting, dict):  # setting is a dict
                return setting.get(edge, def_func(edge))
            else:  # setting is a constant
                return setting

    def create_figure(
        self,
        showlabel      = True,
        colorscale     = 'YlGnBu',
        showscale      = False,
        colorbar_title = '',
        reversescale   = False,
        **params
    ) -> go.Figure:
        axis_settings = dict(
                         autorange      = True,
                         showgrid       = False,
                         zeroline       = False,
                         showline       = False,
                         visible        = False,
                         ticks          = '',
                         showticklabels = False,
        )
        scene = dict(
            xaxis=axis_settings,
            yaxis=axis_settings,
            zaxis=axis_settings,
        )

        layout_params = dict(
            paper_bgcolor  = 'rgba(255,255,255,255)',  # white
            plot_bgcolor   = 'rgba(0,0,0,0)',          # transparent
            autosize       = False,
            height         = 400,
            width          = 450 if showscale else 375,
            title          = '',
            titlefont_size = 16,
            showlegend     = False,
            hovermode      = 'closest',
            margin         = dict(b=5, l=0, r=0, t=20),
            annotations    = [],
            xaxis          = axis_settings,
            yaxis          = axis_settings,
            scene          = scene,
        )

        # override with the given parameters
        layout_params.update(params)

        # create figure
        fig = go.Figure(layout=go.Layout(**layout_params))
        fig.add_traces(self._get_edge_traces())
        fig.add_trace(self._get_node_trace(showlabel, colorscale, showscale, colorbar_title, reversescale))
        return fig
# ===========================================================================================================================
def _graph_signal( s, x,y,z=None, line_color='red', marker_color='red', colorscale='Viridis', colorbar=dict(title='Signal'), width=2, size=8):
    ''' vertical lines with markers of length proportional to a given signal s
        the ordering of the signal s and the node positions x,y,z must be consistent.
        color, width and size could be arrays
    '''
    nones = len(x)*[None]
    if z is None:
        ytop = np.add(y, s)
        xpos = np.c_[x,    x, nones ]
        ypos = np.c_[y, ytop, nones ]
        trace_lines = go.Scatter(
                        x=xpos.flatten(),
                        y=ypos.flatten(),
                        mode='lines',
                        line=dict(color=line_color, width=width)
        )
        trace_markers = go.Scatter(
            x=x,
            y=ytop,
            mode='markers',
            marker=dict(color=marker_color, size=size, colorscale=colorscale, colorbar=colorbar)
        )

    else:
        ztop = np.add(z, s)
        xpos = np.c_[x,     x, nones ]  # Create an array of x positions for the data
        ypos = np.c_[y,     y, nones ]  # Create an array of x positions for the data
        zpos = np.c_[z,  ztop, nones ]  # Create an array of y positions for the data
        trace_lines = go.Scatter3d(
                        x=xpos.flatten(),
                        y=ypos.flatten(),
                        z=zpos.flatten(),
            mode='lines',
            line=dict(color=line_color, width=width)
        )
        trace_markers = go.Scatter3d(
            x=x,
            y=y,
            z=ztop,
            mode='markers',
            marker=dict(color=marker_color, size=size, colorscale=colorscale, colorbar=colorbar)
        )
    return [trace_lines, trace_markers]
# -------------------------------------------------------------------------------------------------
def graph_signal( G, pos, signal, line_color='red', marker_color='red', colorscale='Viridis', colorbar=dict(title='Signal'), width=2, size=8):
    is_3d = len( next(iter(pos.values())) ) == 3
    x = [ pos[v][0] for v in G.nodes() ]
    y = [ pos[v][1] for v in G.nodes() ]
    z = [ pos[v][2] for v in G.nodes() ] if is_3d else None
    s = [ signal[v] for v in G.nodes() ] if isinstance(signal,  dict) else signal # assumed a vector ordered like G.nodes()

    cl = [ line_color[v]   for v in G.nodes() ] if isinstance(line_color,  dict) else line_color
    cm = [ marker_color[v] for v in G.nodes() ] if isinstance(marker_color,dict) else marker_color

    return _graph_signal( s, x,y,z, line_color=cl, marker_color=cm, colorscale=colorscale, colorbar=colorbar, width=width, size=size)
# =================================================================================================
def _nx_planar_layout(G):
    try:    return nx.planar_layout(G)
    except nx.NetworkXException:
        return nx.random_layout(G)
# --------------------------------------------------------------------------------------------------------------
#class PlotlyDisplay(param.Parameterized):
class PlotlyDisplay(pn.viewable.Viewer):
    # --------------------------------------------------------------------------------
    reset_layout = param.Event(False, 'Reset Layout')

    layout       = param.Selector(objects = [ nx.spring_layout, nx.spectral_layout,
                                              _nx_planar_layout,
                                              nx.circular_layout, nx.kamada_kawai_layout,
                                              nx.random_layout,
                                              nx.shell_layout, nx.fruchterman_reingold_layout,
                                              nx.spiral_layout, nx.arf_layout
                                            ], default= nx.spring_layout )
    height      = param.Number( 600, bounds=(100,None), softbounds=(100, 2000))
    width       = param.Number( 400, bounds=(100,None), softbounds=(100, 2000))
    node_color  = param.Selector(objects = [ None, "lightgray", "lemonchiffon", "lightblue", "lightgreen", "pink", "khaki", "olive", ], default=None )
    edge_color  = param.Selector(objects = [ None, "black",     "darkkhaki",    "blue",      "green", "darkorchid", "olive", ], default=None )

    node_size   = param.Number(15,  bounds=(0,None), softbounds=(None,50))
    edge_width  = param.Number( 4,  bounds=(0,None), softbounds=(None,30))

    arrow_size             = param.Number( 0,     bounds=(0,None), softbounds=(None,5))
    arrow_starting_ratio   = param.Number( 0.98,  bounds=(0,None), softbounds=(None,1))
    arrow_tip_ratio        = param.Number( 0.1,   bounds=(0,None), softbounds=(None,2))

    allow_signal           = param.Boolean(False)
    signal_display_option  = param.Selector( objects = [ "colorize_node", "show_vertical_bars"], default = "colorize_node")
    signal_cmap            = param.Selector( objects = ['viridis', 'hot', 'peach', 'reds_r', 'greys_r', 'blues_r', 'greens_r', 'earth'], default = 'viridis')
    signal_marker_size     = param.Number( 3.5,  bounds=(0,None), softbounds=(None,30))
    signal_bar_width       = param.Number( 2,    bounds=(0,None), softbounds=(None,20))

    SAVE                   = param.Event(False)
    RESET                  = param.Event(False)
    camera_r               = param.Number( 2,   bounds=(0,None),   softbounds=(0,20))
    camera_azimuth         = param.Number( 30., bounds=(-360,360), softbounds=(-360,360) )
    camera_elevation       = param.Number( 30., bounds=(-90.,90),  softbounds=(-90,90) )

    trigger_update = param.Event(False, precedence=-1)

    # --------------------------------------------------------------------------------------------------------------
    def __init__(self, G, pos=None, signal=None, signal_bounds=None, eye=None, **params):
        super(PlotlyDisplay, self).__init__(**params )
        self.G                                = G
        self.pos                              = pos
        self.custom_pos                       = pos
        if pos is None: self._change_layout()
        self.signal                           = signal
        self.signal_bounds                    = signal_bounds

        # set up the camera
        self.inhibit                          = True
        if eye is None:
            eye = (self.camera_r, self.camera_azimuth, self.camera_elevation )

        self._set_camera_params( *eye,
                                 up     = dict(x=0, y=0, z=1),
                                 center = dict(x=0, y=0, z=0) )
        with param.parameterized.discard_events(self):
            self.camera_r         = eye[0]
            self.camera_azimuth   = eye[1]
            self.camera_elevation = eye[2]


        self.saved_camera_params = self.camera_params

        #self._force_current_param_view()

        # create the gui and the plot
        self._build_gui()
        self._view()

    def _build_gui(self):
        # collect the parameters into tabs
        tab_general   = pn.Param(self, parameters=['reset_layout', 'layout', 'height', 'width', ],           show_name=False)
        tab_styling   = pn.Param(self, parameters=['node_color', 'node_size', 'edge_color', 'edge_width',],  show_name=False)
        tab_arrows    = pn.Param(self, parameters=['arrow_size', 'arrow_starting_ratio', 'arrow_tip_ratio'], show_name=False)
        tab_signal    = pn.Param(self, parameters=['allow_signal', 'signal_display_option', 'signal_cmap', 
                                                   'signal_marker_size', 'signal_bar_width'],                show_name=False)
        tab_camera    = pn.Param(self, parameters=['SAVE', 'RESET',
                                                   'camera_r', 'camera_azimuth', 'camera_elevation'],        show_name=False)

        # create the tabs
        self.controls = pn.Tabs(('General', tab_general), ('Styling', tab_styling), ('Arrows', tab_arrows),
                                ('Signal',  tab_signal),  ('Camera',  tab_camera))

    @param.depends('reset_layout', watch=True)
    def custom_layout(self):
        if self.custom_pos is None:
            self._change_layout()
        else:
            self.pos = self.custom_pos
            self.trigger_update = True

    def _pos2d_to_3d( self, pos2d ):
        self.pos  = { k : (*pos2d[k], 0.) for k in pos2d }

    @param.depends('layout', watch=True)
    def _change_layout(self):
        self._pos2d_to_3d( self.layout(self.G) )

    def show_signal(self, signal):
        self.signal = None if signal is None else signal
        if self.allow_signal: self.trigger_update = True

    def _set_camera_params( self, r, azimuth, elevation,
                     up     = dict(x=0, y=0, z=1),
                     center = dict(x=0, y=0, z=0) ):
        x,y,z = PlotlyDisplay.spherical_to_cartesian( r, azimuth, elevation )
        self.camera_params = dict( up=up, center=center, eye=dict(x=x, y=y, z=z) )
        #print( "DBG: _set_camera_params", self.camera_params)

    def set_camera_controls_to_current_camera(self):
        import math
        up      = self.fig.layout.scene.camera.up
        center  = self.fig.layout.scene.camera.center
        eye     = self.fig.layout.scene.camera.eye

        self.camera_params = dict( up=up, center=center, eye=eye )

        x = eye['x']; y=eye['y']; z=eye['z']
        if True: #with param.parameterized.discard_events(self):
            self.camera_r, self.camera_azimuth, self.camera_elevation = PlotlyDisplay.cartesian_to_spherical(x,y,z)
        #print( "DBG: set_camera_controls_to_current_camera", self.camera_params)

    def _force_current_param_view( self ):
        x,y,z = PlotlyDisplay.spherical_to_cartesian( self.camera_r, self.camera_azimuth, self.camera_elevation )
        self.fig.layout.scene.camera.up     = self.camera_params['up']
        self.fig.layout.scene.camera.center = self.camera_params['center']
        self.fig.layout.scene.camera.eye    = dict( x = x, y=y, z=z)
        #print( "DBG: _force_current_param_view", self.camera_params)

    def debug_camera(self):
        print( f"gui parm: r={self.camera_r:.2f}, a={self.camera_azimuth:.2f}, e={self.camera_elevation:.2f}")
        cp       = self.camera_params
        scp      = self.saved_camera_params
        d_actual =  dict(
                        up      = self.fig.layout.scene.camera.up,
                        center  = self.fig.layout.scene.camera.center,
                        eye     = self.fig.layout.scene.camera.eye
                    )
        eye      = PlotlyDisplay.spherical_to_cartesian( self.camera_r, self.camera_azimuth, self.camera_elevation )
        def sh_one( nm, a ):
            print( ".  ", nm, f" {a['x']:.2f}, {a['y']:.2f}, {a['z']:.2f}" )
        def sh( txt, a ):
            print( txt )
            sh_one( "eye    ", a['eye'])
            #sh_one( "center ", a['center'])
            #sh_one( "up     ", a['up'])
        print( f"gui eye      {eye[0]:.2f}, {eye[1]:.2f}, {eye[2]:.2f}" )
        sh("actual       ", d_actual)
        sh("camera_params", cp)
        sh("saved        ", scp)

    @param.depends( 'SAVE', watch=True )
    def _save_camera_params(self):
        self.set_camera_controls_to_current_camera()
        self.saved_camera_params = self.camera_params
        #print( "DBG: _save_camera_params", self.camera_params)

    @param.depends( 'RESET', watch=True )
    def _reset_camera_params(self):
        self.camera_params = self.saved_camera_params.copy()
        if True: #with param.parameterized.discard_events(self):
            self.camera_r, self.camera_azimuth, self.camera_elevation = PlotlyDisplay._xyz_to_rae(self.camera_params['eye'])
        self._force_current_param_view()
        #self.trigger_update = True
        #print( "DBG: _reset_camera_params", self.camera_params)

    @param.depends( 'camera_r', 'camera_azimuth', 'camera_elevation', watch=True )
    def set_view_point_from_params( self ):
        self._set_camera_params( self.camera_r, self.camera_azimuth, self.camera_elevation,
                                 up = self.camera_params['up'], center = self.camera_params['center'] )
        #print( "DBG: set_view_point_from_params", self.camera_params)
        if self.inhibit:
            self.inhibit=False
        else:
            self._force_current_param_view()
            #   DBG CHANGE!!!    self.trigger_update = True
    def _view(self):
        #print( "DBG: _view")
        arrow_spec    = None if self.arrow_size == 0 else (self.arrow_starting_ratio , self.arrow_tip_ratio , self.arrow_size )
        traces        = None
        params        = dict()
        node_color    = self.signal if self.allow_signal else self.node_color

        if self.allow_signal and self.signal is not None:
            trace_params = dict()
            if self.signal_display_option == "show_vertical_bars":
                trace_params['line_color']   = "darkred"
                trace_params['marker_color'] = "darkred"
                trace_params['colorbar']     = None
                traces = graph_signal( self.G, self.pos,
                                       self.signal, width=self.signal_bar_width,size=self.signal_marker_size,
                                       **trace_params)

            elif self.signal_display_option == "colorize_node":
                params['showscale']      = True
                params['colorscale']     = self.signal_cmap
                params['colorbar_title'] = 'Signal'
                #params['coloraxis_colorbar'] = dict(
                #        tickformatstops=[
                #            {'dtickrange': [None, 1e-3],   'value': '.3e'},
                #            {'dtickrange': [1e-3,   -0],   'value': '.3f'},
                #            {'dtickrange': [0,       1],   'value': '0.3f'},
                #            {'dtickrange': [1,      10],   'value': '0.1f'},
                #            {'dtickrange': [10,   1000],   'value': '0.0f'},
                #            {'dtickrange': [1000, None],   'value': '0.3e'},
                #        ]
                #    )
                
                for k in params.keys():
                    if isinstance( params[k], tuple): params[k] = params[k][0]   # HUH???? WHY


        vis   = GraphVisualization(self.G, self.pos,
                                     node_color = node_color,
                                     node_size  = self.node_size,
                                     edge_color = self.edge_color,
                                     edge_width = self.edge_width,
                                     arrow_spec = arrow_spec,
                                     node_color_range = self.signal_bounds,)

        self.fig = vis.create_figure( scene_camera = self.camera_params, height=self.height, width=self.width,
                                       **params)
        #self.fig.update_layout(scene_camera=camera_params)

        if traces is not None: self.fig.add_traces(traces)

    @param.depends('layout', 'height', 'width', 'trigger_update', 'node_color', 'node_size', 'edge_color', 'edge_width',
                   'arrow_size', 'arrow_starting_ratio', 'arrow_tip_ratio',
                   'allow_signal', 'signal_display_option', 'signal_cmap',
                   'signal_marker_size', 'signal_bar_width',
                  )
    def view(self):
        #print( "DBG: view")
        self._view()
        return pn.pane.Plotly(self.fig)

    def clone(self):
        obj                                                                                = PlotlyDisplay(self.G, self.pos)
        obj.trigger_update        = False
        obj.custom_pos            = self.custom_pos
        obj.pos                   = self.pos
        obj.inhibit               = False
        obj.saved_camera_params   = self.saved_camera_params
        obj.reset_layout          = self.reset_layout

        obj.layout                = self.layout
        obj.height                = self.height
        obj.width                 = self.width
        obj.node_color            = self.node_color
        obj.edge_color            = self.edge_color

        obj.node_size             = self.node_size
        obj.edge_width            = self.edge_width

        obj.arrow_size            = self.arrow_size
        obj.arrow_starting_ratio  = self.arrow_starting_ratio
        obj.arrow_tip_ratio       = self.arrow_tip_ratio

        obj.signal                = self.signal
        obj.signal_bounds         = self.signal_bounds
        obj.allow_signal          = self.allow_signal
        obj.signal_display_option = self.signal_display_option
        obj.signal_cmap           = self.signal_cmap
        obj.signal_marker_size    = self.signal_marker_size
        obj.signal_bar_width      = self.signal_bar_width

        obj.SAVE                  = self.SAVE
        obj.RESET                 = self.RESET
        obj.saved_camera_params   = self.saved_camera_params
        obj.camera_params         = self.camera_params
        obj.camera_r              = self.camera_r
        obj.camera_azimuth        = self.camera_azimuth
        obj.camera_elevation      = self.camera_elevation
        obj._view()

        return obj

    @staticmethod
    def spherical_to_cartesian(r, azimuth, elevation):
        import math
        a = azimuth*np.pi/180.; e=elevation*np.pi/180
        sa = math.sin(a); ca = math.cos(a)
        se = math.sin(e); ce = math.cos(e)
        x  = r * ce * ca
        y  = r * ce * sa
        z  = r * se
        return x, y, z

    @staticmethod
    def cartesian_to_spherical(x, y, z):
        import math
        r         = math.hypot(x, y, z)
        elevation = math.asin(z / r)
        azimuth   = math.atan2(y, x)
        return r, azimuth*180/np.pi, elevation*180/np.pi
    @staticmethod
    def _xyz_to_rae( eye ):
        return PlotlyDisplay.cartesian_to_spherical( eye['x'], eye['y'], eye['z'])
