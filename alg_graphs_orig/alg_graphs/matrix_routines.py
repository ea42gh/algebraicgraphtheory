import numpy as np
import sympy as sp
# -----------------------------------------------------------------------------------------------
def compute_phase(evals): return np.arctan2(np.imag(evals), np.real(evals))
# -----------------------------------------------------------------------------------------------
def format_matrix(matrix, environment="pmatrix", formatter=str, col_partition=None, row_partition = None):
    """Format a matrix using LaTeX syntax"""
    if col_partition is not None:
        environment="array"
        if isinstance( col_partition, int): col_partition=[col_partition]
    if row_partition is not None:
        if isinstance( row_partition, int): row_partition=[row_partition]

    if not isinstance(matrix, np.ndarray):
        try:
            matrix = np.array(matrix)
        except Exception:
            raise TypeError("Could not convert to Numpy array")

    if len(shape := matrix.shape) == 1:
        matrix = matrix.reshape(1, shape[0])
    elif len(shape) > 2:
        raise ValueError("Array must be 2 dimensional")

    body_lines = [" & ".join(map(formatter, row)) for row in matrix]
    if environment == "array":
        if col_partition is None:
            fmt = str(matrix.shape[1]*'r')
        else:
            diff = [col_partition[i]-col_partition[i-1] for i in range(1,len(col_partition))]
            fmt = "|".join([str(col_partition[0]*"r")]+
                  [k*"r" for k in diff] +
                  [str((matrix.shape[1]-col_partition[-1])*"r")])
    if row_partition is None:
        body = "\\\\".join(body_lines)
    else:
        num = len(row_partition)
        diff = [row_partition[i]-row_partition[i-1] for i in range(1,num)]
        mid  = [body_lines[row_partition[k-1]:row_partition[k]] for k in range(1,num) ]
        if not mid:
            body = " \\\\ \\hline ".join([ body_lines[0:row_partition[0]][0],
                   body_lines[row_partition[-1]:][0]
            ])
        else:
            body = " \\\\ \\hline ".join([ body_lines[0:row_partition[0]][0],
                   mid,
                   body_lines[row_partition[-1]:][0]
            ])

    if environment=='array':
        txt = r'\left(\begin{array}{'+ fmt +'}'+body+r'\end{array}\right)'
    else:
        txt = f"""\\begin{{{environment}}}
{body}
\\end{{{environment}}}"""
    return txt
def _format_matrix(matrix, environment="pmatrix", formatter=str, col_partition=None, row_partition = None):
    """Format a matrix using LaTeX syntax"""
    if col_partition is not None: environment="array"

    if not isinstance(matrix, np.ndarray):
        try:
            matrix = np.array(matrix)
        except Exception:
            raise TypeError("Could not convert to Numpy array")

    if len(shape := matrix.shape) == 1:
        matrix = matrix.reshape(1, shape[0])
    elif len(shape) > 2:
        raise ValueError("Array must be 2 dimensional")

    body_lines = [" & ".join(map(formatter, row)) for row in matrix]
    if environment == "array":
        if col_partition is not None:
            fmt = str(matrix.shape[1]*'r')
        else:
            diff = [col_partition[i]-col_partition[i-1] for i in range(1,len(col_partition))]
            fmt = str(col_partition[0])*"r" +\
                  "|".join([k*"r" for k in diff] ) +\
                  str((matrix.shape[1]-col_partition[-1])*"r")
        print(fmt)

    body = "\\\\".join(body_lines)
    if environment=='array':
        txt = r'\left(\begin{array}{'+ str(matrix.shape[1]*'r')+'}'+body+r'\end{array}\right)'
    else:
        txt = f"""\\begin{{{environment}}}
{body}
\\end{{{environment}}}"""
    return txt
# -----------------------------------------------------------------------------------------------
def normalized_laplacian_using_sympy(adjacency_matrix):
    degree_matrix          = np.diag(adjacency_matrix.sum(axis=1))
    laplacian_matrix       = sp.Matrix(degree_matrix - adjacency_matrix)
    
    degree_matrix_sqrt     = sp.Matrix(np.diag([sp.sqrt(d) for d in degree_matrix.diagonal()]))
    degree_matrix_sqrt_inv = degree_matrix_sqrt.inv()

    return degree_matrix_sqrt_inv @ laplacian_matrix @ degree_matrix_sqrt_inv
# -----------------------------------------------------------------------------------------------
def get_eigen_decomp_using_sympy( L ):
    '''return eigenvalues and eigenvectors sorted by increasing re(lambda)'''
    def flatten(xss):
        return [x for xs in xss for x in xs]
    eig   = L.eigenvects()
    evals = flatten( [[e[0]]* e[1] for e in eig] )
    ndx   = np.argsort([complex(x).real for x in evals])
    evals = [evals[i] for i in ndx]
    evecs = flatten([ e[2] for e in eig ])
    evecs = [evecs[i] for i in ndx]
    return evals, sp.Matrix.hstack(*evecs)
