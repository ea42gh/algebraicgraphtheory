import pytest
import networkx as nx
import numpy as np
import plotly.graph_objects as go


from alg_graphs.graphvisualization import (
    GraphVisualization
)
from alg_graphs.assign_colors_and_sizes import (
    all_values_equal, normalize_values, compute_node_size, compute_edge_width, assign_colors
)
from alg_graphs.color_conversions import (
    convert_to_rgb, convert_to_rgb_batch,
    convert_rgba_to_rgb, convert_hsl_to_rgb,
    convert_hex_to_rgb, convert_named_color_to_rgb
)
from alg_graphs.color_conversions import (
    convert_to_rgb_batch
)

@pytest.fixture
def sample_graph():
    """Returns a simple test graph."""
    G = nx.Graph()
    G.add_edges_from([(0, 1), (1, 2), (2, 3)])
    return G

def test_gv_initialization(sample_graph):
    """Test that GraphVisualization can be instantiated without errors."""
    gv = GraphVisualization(sample_graph)
    assert gv._G is sample_graph
    assert gv.pos is not None  # Default position should be assigned

def test_gv_default_parameters(sample_graph):
    """Test that GraphVisualization parameters are correctly set."""
    gv = GraphVisualization(sample_graph)
    
    # Check default node properties
    assert gv.node_size == 15
    assert gv.node_color == "rgb(173,216,230)"
    
    # Check default edge properties
    assert gv.edge_width == 2.0
    assert gv.edge_color == "rgb(169,169,169)"

    # Check visibility flags
    assert gv.show_nodes is True
    assert gv.show_node_labels is True

def test_gv_modify_pos_valid(sample_graph):
    """Test that modify_pos() correctly updates node positions."""
    gv = GraphVisualization(sample_graph)
    
    new_pos = {n: (n, n * 2) for n in sample_graph.nodes()}
    gv.modify_pos(new_pos)

    assert gv.pos == new_pos
    assert gv.node_x.tolist() == [n for n in sample_graph.nodes()]
    assert gv.node_y.tolist() == [n * 2 for n in sample_graph.nodes()]

def test_gv_modify_pos_invalid(sample_graph):
    """Ensure modify_pos() handles invalid inputs gracefully."""
    gv = GraphVisualization(sample_graph)
    
    invalid_pos = {0: (0, 0), 1: (1, 1)}  # Missing nodes
    with pytest.warns(UserWarning, match="Invalid pos dictionary"):
        gv.modify_pos(invalid_pos)

    assert gv.pos is not invalid_pos  # Should not update to invalid pos

#def test_gv_arrow_spec():
#    """Ensure _process_arrow_spec() updates arrow settings correctly."""
#    gv = GraphVisualization(nx.Graph())
#
#    gv.arrow_params = {"enabled": True, "offset": 0.75}
#    gv._process_arrow_spec()
#    
#    assert gv._arrow_spec["enabled"] is True
#    assert gv._arrow_spec["offset"] == 0.75

def test_gv_create_figure(sample_graph):
    """Test that create_figure() runs without errors and returns a figure."""
    gv = GraphVisualization(sample_graph)
    fig = gv.create_figure()
    
    assert fig is not None
    assert len(fig.data) > 0  # Must contain traces

#def test_gv_node_traces(sample_graph):
#    """Ensure _create_node_trace() generates valid node scatter traces."""
#    gv = GraphVisualization(sample_graph)
#    trace = gv._create_node_traces()
#
#    assert trace is not None
#    assert isinstance(trace, (go.Scatter, go.Scatter3d))

def test_gv_edge_traces(sample_graph):
    """Ensure _create_edge_traces() generates valid edge traces."""
    gv = GraphVisualization(sample_graph)
    traces = gv._create_edge_traces()

    assert isinstance(traces, list)
    assert len(traces) > 0
    assert all(isinstance(t, (go.Scatter, go.Scatter3d)) for t in traces)

