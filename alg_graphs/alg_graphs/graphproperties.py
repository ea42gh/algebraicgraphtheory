import networkx as nx
import random
from collections.abc import Sequence

def compute_node_centrality(G, centrality_type="betweenness", normalized=True):
    """
    Computes centrality measures for nodes.

    Args:
        G (nx.Graph): The input graph.
        centrality_type (str): Type of centrality ("betweenness", "degree", "closeness", "eigenvector").
        normalized (bool): Whether to normalize centrality values (where applicable).

    Returns:
        dict: Dictionary mapping nodes to centrality values.

    Raises:
        ValueError: If an unsupported centrality type is provided.
        RuntimeError: If the centrality calculation fails.
    """
    try:
        if centrality_type == "betweenness":
            return nx.betweenness_centrality(G, normalized=normalized)
        elif centrality_type == "degree":
            return dict(G.degree())
        elif centrality_type == "closeness":
            if not nx.is_connected(G):
                raise RuntimeError("Closeness centrality is undefined for disconnected graphs.")
            return nx.closeness_centrality(G, wf_improved=normalized)  # Improved closeness calculation
        elif centrality_type == "eigenvector":
            return nx.eigenvector_centrality(G, max_iter=1000, tol=1e-6)  # More efficient than NumPy version
        else:
            raise ValueError(f"Unsupported centrality type: {centrality_type}")
    except nx.NetworkXException as e:
        raise RuntimeError(f"Error computing {centrality_type} centrality: {e}")

def convert_to_unweighted_graph(G):
    """
    Converts a weighted graph into an unweighted graph.

    Args:
        G (nx.Graph): The input graph.

    Returns:
        nx.Graph: A new unweighted graph.
    """
    unweighted_G = nx.Graph()
    unweighted_G.add_edges_from(G.edges())
    return unweighted_G

def assign_node_community_labels(G):
    """
    Assigns nodes to communities using modularity clustering.

    Args:
        G (nx.Graph): The input graph.

    Returns:
        dict: Dictionary mapping nodes to community labels.
    """
    communities = list(nx_comm.greedy_modularity_communities(G))
    return {node: i for i, comm in enumerate(communities) for node in comm}

def compute_graph_diameter(G, approximate=False, weight=None):
    """
    Computes the diameter of a graph.

    Args:
        G (nx.Graph): The input graph.
        approximate (bool): If True, uses an approximation for faster computation.
        weight (str, optional): Edge attribute to use as weight. Defaults to None (unweighted).

    Returns:
        int or float: The graph diameter.

    Raises:
        ValueError: If the graph has no edges.
    """
    num_nodes = G.number_of_nodes()

    if num_nodes == 0:
        raise ValueError("Graph is empty, diameter is undefined.")
    if num_nodes == 1:
        return 0  # A single-node graph has a diameter of 0

    # Convert directed graphs to undirected only if necessary
    if G.is_directed():
        G = G.to_undirected(as_view=True)  # Avoids unnecessary memory duplication

    # If all nodes are isolated, return diameter = 0
    if G.number_of_edges() == 0:
        return 0

    # Approximate diameter for large graphs
    if approximate:
        return nx.approximation.diameter(G)

    if nx.is_connected(G):
        if weight is None:
            return nx.diameter(G)

        # **Optimized Weighted Diameter Calculation**:
        # Instead of computing all-pairs shortest paths, only calculate for periphery nodes
        periphery_nodes = nx.periphery(G)  # Nodes farthest from the center
        max_diameter = 0

        for u in periphery_nodes:
            lengths = nx.single_source_dijkstra_path_length(G, u, weight=weight)
            max_diameter = max(max_diameter, max(lengths.values()))

        return max_diameter

    # If the graph is disconnected, compute diameter for each component
    max_component_diameter = 0
    for component in nx.connected_components(G):
        subgraph = G.subgraph(component)
        if weight is None:
            component_diameter = nx.diameter(subgraph)
        else:
            periphery_nodes = nx.periphery(subgraph)
            component_diameter = max(
                max(nx.single_source_dijkstra_path_length(subgraph, u, weight=weight).values())
                for u in periphery_nodes
            )

        max_component_diameter = max(max_component_diameter, component_diameter)

    return max_component_diameter

def compute_shortest_paths(G, source, weight=None, reverse=False):
    """
    Computes shortest paths from a given source node.

    Args:
        G (nx.Graph): The input graph.
        source (node): The source node.
        weight (str, optional): Edge attribute to use as weight (for weighted graphs). Defaults to None (unweighted).
        reverse (bool): If True, computes shortest paths in the reverse direction (for directed graphs).

    Returns:
        dict: Dictionary mapping target nodes to shortest path distances.

    Raises:
        ValueError: If the source node is not in the graph.
    """
    if source not in G:
        raise ValueError(f"Source node {source} is not in the graph.")

    # Handle isolated node case (if source has no neighbors)
    if G.degree(source) == 0:
        return {source: 0}

    # Handle reverse mode efficiently for directed graphs
    if G.is_directed() and reverse:
        if weight is None:
            return {node: dist for node, dist in nx.single_source_shortest_path_length(G.reverse(copy=False), source).items()}
        return {node: dist for node, dist in nx.single_source_dijkstra_path_length(G.reverse(copy=False), source, weight=weight).items()}

    # Use BFS for unweighted graphs (O(V + E))
    if weight is None:
        return nx.single_source_shortest_path_length(G, source)

    # Dynamically decide whether to use bidirectional Dijkstra
    if G.number_of_edges() / (G.number_of_nodes() ** 2) > 0.05:  # If density > 0.05
        distances = {}
        for target in G.nodes():
            if target != source:
                try:
                    dist, _ = nx.bidirectional_dijkstra(G, source, target, weight=weight)
                    distances[target] = dist
                except nx.NetworkXNoPath:
                    distances[target] = float('inf')  # No path exists
        distances[source] = 0  # Source itself has a distance of 0
        return distances

    # Default case: use standard Dijkstra's algorithm (O(E + V log V))
    return nx.single_source_dijkstra_path_length(G, source, weight=weight)

def remove_isolated_nodes(G, inplace=False, condition=None):
    """
    Removes isolated nodes (nodes without edges) or nodes that match a custom condition.

    Args:
        G (nx.Graph): The input graph.
        inplace (bool): If True, modifies the input graph directly instead of creating a copy.
        condition (callable, optional): A function that takes (node, degree) and returns True if the node should be removed.

    Returns:
        nx.Graph: A graph without isolated nodes or nodes matching the condition.
    """
    if condition is None:
        condition = lambda node, degree: degree == 0  # Default to removing isolated nodes

    # Find nodes to remove using efficient set update
    to_remove = set()
    to_remove.update(node for node, degree in G.degree() if condition(node, degree))

    # Return early if no nodes need removal
    if not to_remove:
        return G

    # Modify in-place if required
    if inplace:
        G.remove_nodes_from(to_remove)
        return G

    # Otherwise, return a new modified copy
    new_G = G.copy()
    new_G.remove_nodes_from(to_remove)
    return new_G

def convert_to_unweighted_graph(G, preserve_direction=False, preserve_attrs=False, edge_filter=None):
    """
    Converts a weighted graph into an unweighted graph while preserving node attributes.

    Args:
        G (nx.Graph): The input graph (can be directed, undirected, or a MultiGraph).
        preserve_direction (bool): If True, keeps the graph directed if G is directed.
        preserve_attrs (bool): If True, preserves all edge attributes except weight.
        edge_filter (callable, optional): Function that filters edge attributes,
                                          taking a dictionary and returning a modified dictionary.

    Returns:
        nx.Graph or nx.DiGraph: A new unweighted graph, or the original graph if no modifications are needed.
    """
    # Determine graph type
    GraphType = nx.DiGraph if preserve_direction and G.is_directed() else nx.Graph

    # If G has no edges, return as-is
    if G.number_of_edges() == 0:
        return G

    # Check if G contains weights
    has_weights = any("weight" in data for _, _, data in G.edges(data=True))

    # If G is already unweighted and no modifications are required, return it as-is
    if not has_weights and not preserve_attrs and edge_filter is None and (
        (preserve_direction and G.is_directed()) or (not preserve_direction and not G.is_directed())
    ):
        return G  # Return the original object instead of creating a new one

    # Set default edge filtering function
    if edge_filter is None:
        if preserve_attrs:
            edge_filter = lambda attrs: {k: v for k, v in attrs.items() if k != "weight"}
        else:
            edge_filter = lambda attrs: {}

    # Create a new unweighted graph
    unweighted_G = GraphType()
    unweighted_G.add_nodes_from(G.nodes(data=True))

    # Copy edges with attribute processing in one loop
    if G.is_multigraph():
        for u, v, key, data in G.edges(keys=True, data=True):
            attrs = edge_filter(data) or {}  # Ensure attrs is always a dictionary
            unweighted_G.add_edge(u, v, **attrs)
    else:
        for u, v, data in G.edges(data=True):
            attrs = edge_filter(data) or {}  # Ensure attrs is always a dictionary
            unweighted_G.add_edge(u, v, **attrs)

    return unweighted_G

def assign_random_edge_weights(G, weight_range=(1, 10), inplace=False):
    """
    Assigns random weights to all edges in the graph.

    Args:
        G (nx.Graph): The input graph (can be directed, undirected, or a MultiGraph).
        weight_range (tuple or list): A sequence (min_weight, max_weight) defining the range of weights.
        inplace (bool): If True, modifies the input graph instead of creating a copy.

    Returns:
        nx.Graph: The graph with random edge weights assigned.

    Raises:
        ValueError: If weight_range is not a valid sequence of two numbers or min_weight >= max_weight.
    """
    # Validate weight_range
    if (
        not isinstance(weight_range, Sequence) or
        len(weight_range) != 2 or
        not all(isinstance(x, (int, float)) for x in weight_range)
    ):
        raise ValueError("weight_range must be a sequence (min_weight, max_weight) of two numbers.")

    min_weight, max_weight = weight_range
    if min_weight >= max_weight:
        raise ValueError(f"Invalid weight_range: {weight_range}. min_weight must be less than max_weight.")

    # Return early if the graph has no edges
    if G.number_of_edges() == 0:
        return G

    # Create a copy only if inplace=False
    if not inplace:
        G = G.copy()

    # Assign random weights efficiently
    if G.is_multigraph():
        for u, v, key in G.edges(keys=True):
            G[u][v][key]['weight'] = random.uniform(min_weight, max_weight)
    else:
        for u, v in G.edges():
            G[u][v]['weight'] = random.uniform(min_weight, max_weight)

    return G

def assign_node_community_labels(G, num_communities=5, community_labels=None, inplace=False):
    """
    Assigns random community labels to each node in the graph.

    Args:
        G (nx.Graph): The input graph.
        num_communities (int): The number of distinct community labels (ignored if `community_labels` is provided).
        community_labels (list, optional): A list of custom labels to assign randomly.
        inplace (bool): If True, modifies the input graph instead of creating a copy.

    Returns:
        nx.Graph: The graph with community labels assigned to nodes.

    Raises:
        ValueError: If `num_communities` is not a positive integer or `community_labels` is invalid.
    """
    # Validate input
    if community_labels is not None:
        if not isinstance(community_labels, list) or len(community_labels) == 0:
            raise ValueError("community_labels must be a non-empty list.")
        labels = community_labels
    else:
        if not isinstance(num_communities, int) or num_communities < 1:
            raise ValueError("num_communities must be a positive integer.")
        labels = list(range(1, num_communities + 1))

    # Return early if the graph has no nodes
    if not G.nodes:
        return G

    # Create a copy only if inplace=False
    if not inplace:
        G = G.copy()

    # Assign random community labels efficiently
    random_labels = random.choices(labels, k=len(G))
    for node, label in zip(G.nodes(), random_labels):
        G.nodes[node]['community'] = label

    return G
