import networkx as nx
import numpy as np
import plotly.graph_objects as go
import warnings
import types
import panel as pn
import param
import matplotlib.pyplot as plt
from collections import defaultdict
from .color_conversions import convert_to_rgb_batch

class GraphVisualization(pn.viewable.Viewer):
    """
    Visualizes a graph in 2D or 3D with nodes, edges, and optional arrows.
    """
    show_nodes        = param.Boolean(default=True,                 doc="Toggle node visibility")
    show_node_labels  = param.Boolean(default=True, doc="Show node labels.")
    node_size         = param.ClassSelector(class_=(int, float, dict, types.FunctionType),
                                    default=15, doc="Node size mapping.")
    node_color        = param.ClassSelector(class_=(str, dict, types.FunctionType),
                                     default="rgb(173,216,230)",    doc="Node color mapping.")
    node_opacity      = param.Number(default=1.0, bounds=(0, 1),    doc="Node transparency")
    node_line_width   = param.Number(default=1.0, bounds=(0, None), doc="Outline thickness")
    node_symbol       = param.String(default="circle",              doc="Node shape (e.g., 'circle', 'square')")
    node_textposition = param.String(default="middle center",       doc="Label position")
    node_textfont     = param.Dict(default={"color": "black", "size": 14, "family": "Arial"}, doc="Label font settings")

    # Node properties
    node_text = param.Parameter(default=None, doc="Node labels: str, dict, or function.")

    # Edge properties

    edge_width   = param.ClassSelector(class_=(int, float, dict, types.FunctionType),
                                     default=2.0, doc="Edge width mapping.")
    edge_color   = param.ClassSelector(class_=(str, dict, types.FunctionType),
                                     default="rgb(169,169,169)", doc="Edge color mapping.")

    edge_opacity = param.Number(default=0.8, bounds=(0, 1), doc="Edge opacity.")
    show_edges   = param.Boolean(default=True, doc="Show edges.")

    # Colormaps
    node_cmap    = param.String(default="CET_L17", doc="Colormap for node colors (if numeric).")
    edge_cmap    = param.String(default="Viridis", doc="Colormap for edge colors (if numeric).")

    # Colorbar options
    colorbar_source  = param.ObjectSelector(default=None, objects=[None, "node", "edge", "signal"],
                                           doc="Determines which colorbar to display.")
    node_showscale   = param.Boolean(default=False, doc="Show colorbar for nodes.")
    edge_showscale   = param.Boolean(default=False, doc="Show colorbar for edges.")
    signal_showscale = param.Boolean(default=False, doc="Show colorbar for signals.")

    # Signal scaling
    signal_scale     = param.Number(default=1.0, bounds=(0, None), doc="Scale for normalized signal values.")


    # Arrow parameters
    arrow_params     = param.Dict(default={}, doc="Arrow parameters (modifies arrow_spec).")

    # Camera settings (for 3D mode)
    camera_view      = param.Dict(default={
        "up": {"x": 0, "y": 0, "z": 1},
        "center": {"x": 0, "y": 0, "z": 0},
        "eye": {"x": 1.0, "y": 1.0, "z": 0.8}
    }, doc="Camera settings for 3D mode.")

    # ======================================================================
    def __init__(self, G, pos=None, fig_size=(400,400), **params):
        """
        Initializes the GraphVisualization class.

        Args:
            G (nx.Graph): The input graph.
            pos (dict, optional): Node positions as {node: (x, y) or (x, y, z)}.
            **params: Additional visualization parameters.
        """
        with param.parameterized.discard_events(self):
            super().__init__(**params)

            self.fig_size=fig_size

            self._arrow_spec = {
                "enabled": True,          # Toggle arrows on/off
                "offset_fraction": 0.52,  # Fractional Position along edge (0=start, 1=end)
                "style": 3,               # Arrowhead style
                "head_size": 1.5,         # Arrowhead scale
                "shaft_length": 0.02,     # Shaft extension beyond arrowhead (2D only)
                "arrow_width":  1.0       # shaft and arrow thickness
            }
            self._process_arrow_spec()

            # Store the graph
            self._G = G
            if pos is None:
                pos = nx.spring_layout(G, dim=3)  # Default to 3D layout
            self.modify_pos(pos)

            #self.edge_width_cmap   = plt.cm.Blues  # Default to identity mapping
            self._set_node_color()
            self._set_node_size()
            self._set_edge_color()
            self._set_edge_width()

            self.signal_values = None
    # ----------------------------------------------------------------------
    def modify_pos(self, new_pos):
        """
        Updates node positions and precomputes coordinate arrays for efficient rendering.

        - Validates input to ensure all nodes have positions.
        - Detects whether the graph is 2D or 3D.
        - Caches node and edge positions (`self.node_x`, `self.edge_x`, etc.).
        - Precomputes edge colors (`_set_edge_color()`) and edge widths (`_set_edge_width()`).
        - Updates arrow specifications (`_process_arrow_spec()`).

        Args:
            new_pos (dict): Node positions as `{node: (x, y)}` for 2D or `{node: (x, y, z)}` for 3D.

        Raises:
            Warning: If `new_pos` is invalid or missing nodes.
        """
        if not self._validate_pos(new_pos):
            warnings.warn("Invalid pos dictionary. Must be {node_id: position} for all nodes.")
            return

        # Store new positions
        self.pos       = new_pos
        self.edge_list = list(self._G.edges())  # Ensure stable edge order

        # Detect 2D vs. 3D layout
        self._is_3d = any(len(pos) == 3 for pos in new_pos.values())

        # Precompute node positions
        node_x, node_y, node_z = [], [], []
        for node in self._G.nodes():
            x, y, *z = new_pos[node]  # Handles (x, y) or (x, y, z)
            node_x.append(x)
            node_y.append(y)
            node_z.append(z[0] if z else 0)  # Default z = 0 if 2D

        self.node_x = np.array(node_x)
        self.node_y = np.array(node_y)
        self.node_z = np.array(node_z)

        # Precompute edge positions
        edge_x, edge_y, edge_z = [], [], []
        for source, target in self.edge_list:
            x0, y0, *z0 = new_pos[source]
            x1, y1, *z1 = new_pos[target]

            edge_x.extend([x0, x1, None])
            edge_y.extend([y0, y1, None])
            edge_z.extend([z0[0] if z0 else 0, z1[0] if z1 else 0, None])

        self.edge_x = np.array(edge_x)
        self.edge_y = np.array(edge_y)
        self.edge_z = np.array(edge_z)

        self.graph_bbox = {
            "min_x": np.min(self.node_x), "max_x": np.max(self.node_x),
            "min_y": np.min(self.node_y), "max_y": np.max(self.node_y),
            "min_z": np.min(self.node_z) if self._is_3d else 0,
            "max_z": np.max(self.node_z) if self._is_3d else 0
        }
       # **Cache the average edge length**
        edge_lengths = [
            np.linalg.norm([
                self.edge_x[i] - self.edge_x[i + 1],
                self.edge_y[i] - self.edge_y[i + 1],
                self.edge_z[i] - self.edge_z[i + 1]
            ])
            for i in range(0, len(self.edge_x) - 2, 3)  # Step by 3 to get valid edges
        ]
        self.avg_edge_length = np.mean(edge_lengths) if edge_lengths else np.linalg.norm([
            self.graph_bbox["max_x"] - self.graph_bbox["min_x"],
            self.graph_bbox["max_y"] - self.graph_bbox["min_y"],
            self.graph_bbox["max_z"] - self.graph_bbox["min_z"]
        ]) * 0.1  # Fallback to 10% of graph extent

        self._set_edge_color()  # Precompute edge colors (OGV method)
        self._set_edge_width()  # Precompute edge widths (missing in last version, restoring it)
        #self._process_arrow_spec()  # Ensure arrows update correctly

        if hasattr(self, "fig") and self.fig is not None and self._is_3d:
            if "scene" not in self.fig.layout:
                self.fig.layout["scene"] = {}
            self.fig.layout["scene"]["camera"] = self.camera_view
    # ----------------------------------------------------------------------
    @property
    def arrow_spec(self):
        """Read-only access to the internal `_arrow_spec` dictionary."""
        return self._arrow_spec.copy()
    # ----------------------------------------------------------------------
    @param.depends("arrow_params")
    def _process_arrow_spec(self):
        """
        Ensures `_arrow_spec` is updated based on `arrow_params`,
        while preserving valid keys and preventing invalid modifications.
        """
        if not isinstance(self.arrow_params, dict):
            warnings.warn("Invalid arrow_params format. Expected a dictionary.")
            return

        valid_keys = {"enabled", "offset_fraction", "style", "head_size", "shaft_length", "arrow_width"}
        invalid_keys = set(self.arrow_params.keys()) - valid_keys

        if invalid_keys:
            warnings.warn(f"Ignoring invalid arrow parameters: {invalid_keys}")

        # Update `_arrow_spec` without overwriting missing values
        for key in valid_keys:
            if key in self.arrow_params:
                self._arrow_spec[key] = self.arrow_params[key]
    # ----------------------------------------------------------------------
    def _validate_pos(self, pos):
        """
        Validates the pos dictionary.

        Args:
            pos (dict): Node positions dictionary.

        Returns:
            bool: True if pos is valid, False otherwise.
        """
        if not isinstance(pos, dict):
            return False

        if set(pos.keys()) != set(self._G.nodes()):
            return False

        if not all(isinstance(p, (tuple, list, np.ndarray)) and len(p) in (2, 3) for p in pos.values()):
            return False

        return True
    # ----------------------------------------------------------------------
    def _set_attribute_cache(self, attribute, default_value, cmap=None):
        """
        Caches attributes (like node_color, node_size, edge_color, edge_width)
        to avoid repeated computation.
        """
        cache_name = f"_{attribute}_cache"
        cache = {
            entity: self._get_attribute_value(attribute, entity, default_value, cmap=cmap)
            for entity in (self._G.nodes() if "node" in attribute else self._G.edges())
        }
        setattr(self, cache_name, cache)
    # ----------------------------------------------------------------------
    def _get_cached_attribute(self, attribute, entity, default_value):
        """
        Retrieves cached attribute values, ensuring efficiency.
        """
        cache_name = f"_{attribute}_cache"
        if not hasattr(self, cache_name):
            self._set_attribute_cache(attribute, default_value)  # Regenerate cache if missing
        cache = getattr(self, cache_name)
        return cache.get(entity, default_value) if cache is not None else default_value
    # ----------------------------------------------------------------------
    def _get_attribute_value(self, attribute, entity, default, cmap=None):
        """
        Generalized function to retrieve attribute values for nodes and edges.

        Supports:
        - Fixed values (int, float, str)
        - Dictionary lookups (handles undirected edges)
        - Callables (dynamically computed values)
        - Numerical values mapped via colormap (if applicable)

        Args:
            attribute (str): Name of the attribute ('node_size', 'node_color', etc.).
            entity: The node or edge being queried.
            default: Default value if no specific value is found.
            cmap (str, optional): If the attribute contains numerical values, apply this colormap.

        Returns:
            The computed value for the node or edge.
        """
        param_value = getattr(self, attribute, default)

        #  Fixed values: Return directly
        if isinstance(param_value, (int, float, str)):
            return param_value

        #  Dictionary lookup (supports undirected edges)
        if isinstance(param_value, dict):
            value = param_value.get(entity, param_value.get((entity[1], entity[0]), default) if isinstance(entity, tuple) else default)

            #  Apply colormap if numerical
            if cmap and isinstance(value, (int, float)):
                if not hasattr(self, f"_{attribute}_map"):
                    setattr(self, f"_{attribute}_map", assign_colors(param_value, cmap=cmap))
                return getattr(self, f"_{attribute}_map").get(entity, default)

            return value

        #  Function evaluation
        if isinstance(param_value, types.FunctionType):
            try:
                return param_value(entity)  # Execute function
            except Exception as e:
                warnings.warn(f"Function `{attribute}` failed for {entity}: {e}. Using default.")
                return default

        warnings.warn(f"Invalid format for `{attribute}` ({type(param_value)}). Using default.")
        return default
    # ----------------------------------------------------------------------
    def _get_node_color(self, node):
        return self._get_cached_attribute("node_color", node, "rgb(173,216,230)")
    # ----------------------------------------------------------------------
    def _get_node_size(self, node):
        return self._get_cached_attribute("node_size", node, 15)
    # ----------------------------------------------------------------------
    def _get_edge_color(self, edge):
        return self._get_cached_attribute("edge_color", edge, "rgb(10,10,10)")
    # ----------------------------------------------------------------------
    def _get_edge_width(self, edge):
        return self._get_cached_attribute("edge_width", edge, 2.0)
    # ----------------------------------------------------------------------
    def _get_edge_style(self, edge):
        """
        Returns the edge style (solid or dashed).

        Args:
            edge (tuple): Edge (source, target)

        Returns:
            str: 'solid' or 'dash'
        """
        edge_style = self._arrow_spec.get("arrow_style", "solid")

        if isinstance(edge_style, dict):
            return edge_style.get(edge, edge_style.get((edge[1], edge[0]), "solid"))

        return edge_style
    # ----------------------------------------------------------------------
    @pn.depends("node_color")
    def _set_node_color(self):
        """
        Updates cached node colors based on `node_color` parameter.
        """
        colors = {node: self._get_attribute_value("node_color", node, "rgb(173,216,230)", cmap=self.node_cmap)
                  for node in self._G.nodes()}
        self._node_color_cache = convert_to_rgb_batch(colors)
    # ----------------------------------------------------------------------
    @pn.depends("node_size")
    def _set_node_size(self):
        """
        Updates cached node sizes based on `node_size` parameter.
        """
        sizes = {node: self._get_attribute_value("node_size", node, 15) for node in self._G.nodes()}
        self._node_size_cache = sizes
    # ----------------------------------------------------------------------
    @pn.depends("edge_color")
    def _set_edge_color(self):
        """
        Updates cached edge colors based on `edge_color` parameter.
        """
        colors = {edge: self._get_attribute_value("edge_color", edge, "rgb(10,10,10)", cmap=self.edge_cmap)
                  for edge in self._G.edges()}
        self._edge_color_cache = convert_to_rgb_batch(colors)
    # ----------------------------------------------------------------------
    @pn.depends("edge_width")
    def _set_edge_width(self):
        """
        Updates cached edge widths based on `edge_width` parameter.
        """
        sizes = {edge: self._get_attribute_value("edge_width", edge, 2.0) for edge in self._G.edges()}
        self._edge_width_cache = sizes
    # ----------------------------------------------------------------------
    def _create_node_traces(self):
        """
        Creates batched node traces for visualization.

        Features:
        - Uses cached node colors and sizes (_get_node_color, _get_node_size).
        - Uses `self._G.nodes()` instead of a missing `self.node_list`.
        - Uses `self._is_3d` instead of `self.is_3d` for 3D mode detection.
        - Batches nodes by color to minimize Plotly traces.
        - Ensures compatibility with OGV while optimizing performance.

        Returns:
            list: A list of Plotly scatter traces for node visualization.
        """
        if not self.show_nodes and not self.show_node_labels:
            return []

        #  Fetch PyViz-controlled parameters
        node_symbol  = self.node_symbol
        node_opacity = self.node_opacity
        line_width   = self.node_line_width
        textposition = self.node_textposition
        textfont     = self.node_textfont

        #  Convert cached node positions to NumPy arrays
        node_x = np.array(self.node_x)
        node_y = np.array(self.node_y)
        node_z = np.array(self.node_z) if self._is_3d else None

        #  Get node list from the graph
        node_list = list(self._G.nodes())

        #  Dictionary to batch nodes by color
        node_batches = defaultdict(lambda: {"x": [], "y": [], "z": [], "nodes": [], "sizes": [], "texts": []})

        #  Process each node
        for i, node in enumerate(node_list):
            x, y = node_x[i], node_y[i]
            z    = node_z[i] if self._is_3d else None

            #  Use cached values instead of manual lookup
            node_color = self._get_node_color(node)
            node_size  = self._get_node_size(node)

            #  Determine text labels
            if self.show_node_labels:
                if isinstance(self.node_text, dict) and node in self.node_text:
                    text_label = self.node_text[node]
                elif callable(self.node_text):
                    try:
                        text_label = self.node_text(node)
                    except Exception:
                        text_label = str(node)
                else:
                    text_label = str(node)
            else:
                text_label = None

            #  Add node to its color batch
            node_batches[node_color]["x"].append(x)
            node_batches[node_color]["y"].append(y)
            node_batches[node_color]["sizes"].append(node_size)
            node_batches[node_color]["texts"].append(text_label)
            if self._is_3d:
                node_batches[node_color]["z"].append(z)
            node_batches[node_color]["nodes"].append(node)

        #  Convert batches into Plotly scatter traces
        node_traces = []
        for color, batch in node_batches.items():
            marker_dict = {
                "size": batch["sizes"],
                "symbol": node_symbol,
                "opacity": node_opacity,
                "color": color,
                "line": dict(width=line_width, color="black"),
            }

            text_params = {
                "text": batch["texts"] if self.show_node_labels else None,
                "textposition": textposition,
                "textfont": textfont,
            }

            trace_args = {
                "x": batch["x"],
                "y": batch["y"],
                "mode": "markers+text" if self.show_node_labels else "markers",
                "marker": marker_dict,
                "hoverinfo": "text",
                "name": "Nodes",
                **text_params,
            }

            if self._is_3d:
                trace_args["z"] = batch["z"]
                node_traces.append(go.Scatter3d(**trace_args))
            else:
                node_traces.append(go.Scatter(**trace_args))

        return node_traces
    # ----------------------------------------------------------------------
    def _create_edge_traces(self):
        """
        Generates edge traces for visualization using cached edge coordinates.

        Uses precomputed `self.edge_x`, `self.edge_y`, `self.edge_z` for efficiency.
        Ensures edge colors and widths match precomputed values in caches.
        Groups edges by `(color, width)` to optimize rendering.

        Returns:
            list: A list of Plotly `go.Scatter` (2D) or `go.Scatter3d` (3D) traces.
        """
        if not self.show_edges:
            return []

        if not hasattr(self, "edge_x") or not hasattr(self, "edge_y"):
            print("DBG: _create_edg_traces warns!")
            warnings.warn("Edge coordinate cache not initialized. Call `modify_pos()` first.")
            return []

        # Ensure widths are precomputed
        if not hasattr(self, "_edge_width_cache"):
            self._set_edge_width()

        # Dictionary to batch edges by (color, width)
        edge_groups = defaultdict(lambda: {"x": [], "y": [], "z": [] if self._is_3d else None})

        # Batch edges by (color, width)
        edge_index = 0
        for edge in self.edge_list:
            color     = self._get_edge_color(edge)  #  Uses cached colors
            width     = self._get_edge_width(edge)  #  Uses cached widths
            group_key = (color, width)
            batch     = edge_groups[group_key]

            # Append precomputed edge positions
            batch["x"].extend([self.edge_x[edge_index], self.edge_x[edge_index + 1], None])
            batch["y"].extend([self.edge_y[edge_index], self.edge_y[edge_index + 1], None])
            if self._is_3d:
                batch["z"].extend([self.edge_z[edge_index], self.edge_z[edge_index + 1], None])

            edge_index += 3  # Move to the next edge triplet

        # Generate traces for each batch
        edge_traces = []
        for (color, width), batch in edge_groups.items():
            trace_kwargs = {
                "x": batch["x"],
                "y": batch["y"],
                "mode": "lines",
                "line": dict(color=color, width=width),
                "opacity": self.edge_opacity,
                "name": "Edges",
            }

            if self._is_3d:
                trace_kwargs["z"] = batch["z"]
                trace = go.Scatter3d(**trace_kwargs)
            else:
                trace = go.Scatter(**trace_kwargs)

            edge_traces.append(trace)

        return edge_traces
    # ----------------------------------------------------------------------
    def _get_arrow_size(self, edge, default_size):
        """
        Returns the arrow size for a given edge.
        Supports fixed values, dictionaries, or functions.

        Args:
            edge (tuple): Edge (source, target)
            default_size (float): Default tip scale

        Returns:
            float: Computed arrow size
        """
        arrow_size = self._arrow_spec.get("arrow_size_per_edge", default_size)

        if isinstance(arrow_size, dict):
            return arrow_size.get(edge, arrow_size.get((edge[1], edge[0]), default_size))

        if callable(arrow_size):
            try:
                return arrow_size(edge)
            except Exception as e:
                warnings.warn(f"Arrow size function failed for {edge}: {e}")
                return default_size

        return arrow_size  # Single numeric value
    # ----------------------------------------------------------------------
    def _create_2d_arrows(self):
        """
        Creates 2D arrow annotations using `_arrow_spec`.

        Uses:
        - `enabled`: Toggles arrows on/off.
        - `offset_fraction`: Determines arrow placement along the edge.
        - `style`: Controls arrowhead style (Plotly enum).
        - `head_size`: Sets the arrowhead size.
        - `shaft_length`: Controls how far the shaft extends beyond the arrowhead.

        Returns:
            list: A list of Plotly annotations for 2D arrows.
        """
        if not self._arrow_spec.get("enabled", False):
            return []

        #  Fetch `_arrow_spec` values
        offset       = self._arrow_spec.get("offset_fraction", 0.525)
        style        = self._arrow_spec.get("style", 3)
        head_size    = self._arrow_spec.get("head_size", 1.5)
        shaft_length = self._arrow_spec.get("shaft_length", 0.02)
        arrow_width  = self._arrow_spec.get("arrow_width", 1.)

        #  Batch fetch all edge colors (arrowhead matches edge color)
        edge_colors = {edge: self._get_edge_color(edge) for edge in self.edge_list}

        #  Convert cached edge positions to NumPy arrays
        edge_x = np.array(self.edge_x).reshape(-1, 3)
        edge_y = np.array(self.edge_y).reshape(-1, 3)

        #  Dictionary to batch arrows by color
        arrow_batches = defaultdict(list)

        #  Process each edge
        for i, (source, target) in enumerate(self.edge_list):
            start_x, start_y = edge_x[i, 0], edge_y[i, 0]
            end_x, end_y = edge_x[i, 1], edge_y[i, 1]

            dx, dy = end_x - start_x, end_y - start_y
            d = np.hypot(dx, dy)
            if d == 0:
                continue  # Skip degenerate cases

            #  Compute normalized direction vector
            norm_dx, norm_dy = dx / d, dy / d

            #  Compute arrow position (fixed size, no scaling by edge length)
            head_x = start_x + offset * dx
            head_y = start_y + offset * dy

            #  Compute shaft position using `shaft_length`
            tail_x = head_x - (shaft_length * norm_dx)
            tail_y = head_y - (shaft_length * norm_dy)

            #  Retrieve edge color (arrowhead matches edge)
            arrow_color = edge_colors[(source, target)]

            #  Add to color batch
            arrow_batches[arrow_color].append(
                dict(
                    x=head_x, y=head_y,
                    ax=tail_x, ay=tail_y,
                    xref="x", yref="y",
                    axref="x", ayref="y",
                    showarrow=True,
                    arrowhead=style,
                    arrowsize=head_size,
                    arrowwidth=arrow_width,
                    arrowcolor=arrow_color
                )
            )

        # Convert batches into a single list of annotations
        return sum(arrow_batches.values(), [])
    # ----------------------------------------------------------------------
    def _create_3d_arrows(self, auto_scale_arrows=True):
        """
        Creates 3D arrow traces using `_arrow_spec`, batching by color.

        Uses:
        - `enabled`: Toggles arrows on/off.
        - `offset_fraction`: Determines arrow placement along the edge.
        - `head_size`: Controls arrowhead (cone) size.
        - `arrow_width`: Controls arrow shaft thickness.
        - `auto_scale_arrows`: Dynamically scales arrows based on graph size.

        Args:
            auto_scale_arrows (bool): Whether to scale arrow sizes dynamically.

        Returns:
            list: A list of Plotly cone traces for 3D arrows.
        """
        if not self._arrow_spec.get("enabled", False):
            return []

        # Fetch `_arrow_spec` values
        offset = self._arrow_spec.get("offset_fraction", 0.525)
        head_size = self._arrow_spec.get("head_size", 1.5)  # Fixed size by default
        arrow_width = self._arrow_spec.get("arrow_width", 1.0)  # Controls shaft thickness

        # **Scale arrows dynamically if auto_scale_arrows=True**
        if auto_scale_arrows:
            graph_extent = np.linalg.norm([
                self.graph_bbox["max_x"] - self.graph_bbox["min_x"],
                self.graph_bbox["max_y"] - self.graph_bbox["min_y"],
                self.graph_bbox["max_z"] - self.graph_bbox["min_z"]
            ])

            scale_factor = max(graph_extent * 0.05, self.avg_edge_length * 0.2)
            head_size   *= scale_factor
            arrow_width *= scale_factor

        # Batch fetch all edge colors (arrowhead matches edge color)
        edge_colors = {edge: self._get_edge_color(edge) for edge in self.edge_list}

        # Convert cached edge positions to NumPy arrays
        edge_x = np.array(self.edge_x).reshape(-1, 3)
        edge_y = np.array(self.edge_y).reshape(-1, 3)
        edge_z = np.array(self.edge_z).reshape(-1, 3)

        # Dictionary to batch arrows by color
        arrow_batches = defaultdict(lambda: {"x": [], "y": [], "z": [], "u": [], "v": [], "w": []})

        # Process each edge
        for i, (source, target) in enumerate(self.edge_list):
            start_x, start_y, start_z = edge_x[i, 0], edge_y[i, 0], edge_z[i, 0]
            end_x, end_y, end_z = edge_x[i, 1], edge_y[i, 1], edge_z[i, 1]

            dx, dy, dz = end_x - start_x, end_y - start_y, end_z - start_z
            d = np.linalg.norm([dx, dy, dz])
            if d == 0:
                continue  # Skip degenerate cases

            # Compute normalized direction vector
            norm_dx, norm_dy, norm_dz = dx / d, dy / d, dz / d

            # Compute arrow position (fixed size, no scaling by edge length)
            head_x = start_x + offset * dx
            head_y = start_y + offset * dy
            head_z = start_z + offset * dz

            # Retrieve edge color (arrowhead matches edge)
            arrow_color = edge_colors[(source, target)]

            # Add arrow data to batch
            arrow_batches[arrow_color]["x"].append(head_x)
            arrow_batches[arrow_color]["y"].append(head_y)
            arrow_batches[arrow_color]["z"].append(head_z)
            arrow_batches[arrow_color]["u"].append(norm_dx)
            arrow_batches[arrow_color]["v"].append(norm_dy)
            arrow_batches[arrow_color]["w"].append(norm_dz)

        # Convert batches into a single list of cone traces
        arrow_traces = []
        for color, batch in arrow_batches.items():
            arrow_traces.append(
                go.Cone(
                    x=batch["x"],
                    y=batch["y"],
                    z=batch["z"],
                    u=batch["u"],
                    v=batch["v"],
                    w=batch["w"],
                    sizemode="absolute",
                    sizeref=head_size * arrow_width,
                    anchor="tail",
                    colorscale=[[0, color], [1, color]],
                    showscale=False,
                    hoverinfo=None,
                    name="3D Arrows"
                )
            )

        return arrow_traces
    # ----------------------------------------------------------------------
    def show_arrows(self, yes=True):
        """Enable or disable arrows without modifying other arrow settings."""
        self.arrow_params = {"enabled": yes}
        self._process_arrow_spec()  # Ensure the spec updates correctly
        #self._update_arrows_only()  # Ensure the figure updates
    # ----------------------------------------------------------------------
    def create_figure(self, fig_size=None, **params):
        """
        Creates a Plotly figure with nodes, edges, and arrows.
        - integrates param settings
        - Uses precomputed edge and node coordinates for efficiency.
        - Skips traces if they are disabled (e.g., hiding nodes, edges or arrows).
        - Allows users to customize layout and 3D camera settings.

        Args:
            layout_options (dict, optional): Custom layout settings for the figure.
            camera_options (dict, optional): Custom camera settings for 3D visualization.

        Returns:
            go.Figure: A Plotly figure containing the graph visualization.
        """
        with param.parameterized.discard_events(self):
            for key, value in params.items():
                if hasattr(self, key):
                    object.__setattr__(self, key, value)

        if fig_size is not None:
            self.fig_size=fig_size

        self.fig = go.Figure()

        edges = self._create_edge_traces()
        if edges:
            self.fig.add_traces( edges )
        nodes = self._create_node_traces()
        if nodes:
            self.fig.add_traces( nodes )

        if self._arrow_spec["enabled"]:
            if self._is_3d:
                arrows = self._create_3d_arrows()
                if arrows:
                    self.fig.add_traces(arrows)
            else:
                arrows = self._create_2d_arrows()
                if arrows:
                    self.fig.update_layout(annotations=arrows)

        layout_settings = dict(
            showlegend=False,
            hovermode="closest",
            margin=dict(l=0, r=0, t=0, b=0),
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
            height=self.fig_size[0],
            width=self.fig_size[1],
        )

        if self._is_3d:
            layout_settings["scene"] = dict(
                xaxis=dict(visible=False, showgrid=False, zeroline=False, showline=False, showticklabels=False),
                yaxis=dict(visible=False, showgrid=False, zeroline=False, showline=False, showticklabels=False),
                zaxis=dict(visible=False, showgrid=False, zeroline=False, showline=False, showticklabels=False),
                bgcolor="rgba(0,0,0,0)",
                camera=self.camera_view if hasattr(self, "camera_view") else default_camera  #  Apply camera settings
            )
        else:
            layout_settings["xaxis"] = dict(visible=False, showgrid=False, zeroline=False, showline=False, showticklabels=False)
            layout_settings["yaxis"] = dict(visible=False, showgrid=False, zeroline=False, showline=False, showticklabels=False)

        self.fig.update_layout(**layout_settings)
        return self.fig
    # ----------------------------------------------------------------------
    @param.depends(
        "node_size", "node_color", "node_text", "show_nodes", "arrow_params",
        "show_node_labels", "node_opacity", "node_symbol"
    )
    def _update_nodes_only(self):
        """
        Efficiently update only the node traces without re-rendering the entire figure.

        Triggers update when any relevant node attribute changes:
        - `node_size`, `node_color`, `node_text`: Visual appearance
        - `pos`: Node positions
        - `show_nodes`, `show_node_labels`: Visibility settings
        - `node_opacity`, `node_symbol`: Style adjustments

        Ensures:
        - Old node traces are removed before adding updated ones.
        - Figure (`self.fig`) is modified in-place to avoid unnecessary re-creation.
        """
        print("DBG _update nodes fired")
        if not hasattr(self, "fig") or self.fig is None:
            return  # No figure to update

        #  Remove old node traces
        self.fig.data = [trace for trace in self.fig.data if trace.name != "Nodes"]

        #  Generate new node traces
        node_traces = self._create_node_traces()

        #  Add updated node traces to the figure
        if node_traces:
            for trace in node_traces:
                self.fig.add_trace(trace)
    # ----------------------------------------------------------------------
    @param.depends("edge_color", "edge_width", "show_edges")
    def _update_edges_only(self):
        """
        Efficiently updates the edge traces without re-rendering the entire figure.

        Triggers update when:
        - `edge_color` changes (updates edge colors).
        - `edge_width` changes (updates edge thickness).
        - `show_edges` changes (toggles edge visibility).

        Ensures:
        - Old edge traces are removed before adding updated ones.
        - If `show_edges` is False, edges are hidden instead of removed.
        - Figure (`self.fig`) is modified in-place to avoid unnecessary re-creation.
        """
        print("DBG _update edges fired")

        if not hasattr(self, "fig") or self.fig is None:
            return  # No figure to update

        #  Remove old edge traces
        self.fig.data = [trace for trace in self.fig.data if not trace.name.startswith("Edges")]

        #  If edges are disabled, return early
        if not self.show_edges:
            return

        #  Generate new edge traces
        edge_traces = self._create_edge_traces()

        #  Add updated edge traces to the figure
        if edge_traces:
            self.fig.add_traces(edge_traces)
    # ----------------------------------------------------------------------
    @param.depends("arrow_params", "edge_color")
    def _update_arrows_only(self):
        """
        Ensures that arrows are updated correctly without duplicates.

        Triggers update when:
        - `arrow_spec` changes (modifies arrow appearance).
        - `edge_color` changes (arrows inherit edge colors).
        - `arrow_params` changes (e.g., toggles arrows on/off).

        Ensures:
        - Old arrow traces are removed before adding updated ones.
        - Properly handles both 2D and 3D arrow updates.
        """
        print("DBG _update arrows fired")

        if not hasattr(self, "fig") or self.fig is None:
            return  # No figure to update

        self.fig.data = [trace for trace in self.fig.data if trace.name != "3D Arrows"]
        if "annotations" in self.fig.layout and self.fig.layout.annotations:
            self.fig.layout.annotations = [
                annotation for annotation in self.fig.layout.annotations
                if "arrowhead" not in annotation  # Keeps only non-arrow annotations
            ]

        #  If arrows are disabled, return early
        if not self._arrow_spec["enabled"]:
            return

        #  Add new arrows based on current settings
        if self._is_3d:
            arrow_traces = self._create_arrows_3d()
            if arrow_traces:
                self.fig.add_traces(arrow_traces)
        else:
            arrow_annotations = self._create_arrows_2d()
            self.fig.update_layout(annotations=arrow_annotations)
    # ----------------------------------------------------------------------
    def _update_figure(self):
        """
        Updates the entire figure by calling all `_update_*` functions.

        Ensures:
        - Nodes, edges, and arrows are updated in the correct order.
        - The entire figure is refreshed exactly **once** per change.
        """
        print("DBG _update all fired")

        if not hasattr(self, "fig") or self.fig is None:
            return
        self._update_nodes_only()
        self._update_edges_only()
        self._update_arrows_only()
        self.view()
    # ----------------------------------------------------------------------
    @param.depends(
        "node_size", "node_color", "edge_width", "edge_color",
        "arrow_params", "colorbar_source"
    )
    def view(self):
        """
        Ensures `update_figure()` is executed before rendering the figure.

        - No longer directly depends on `node_size`, `node_color`, etc.
        - Instead, it waits for `update_figure()` to complete.
        - This guarantees a fully updated figure before display.
        """
        if not hasattr(self, "fig") or self.fig is None:
            self._create_figure()
        print("DBG view fired")
        return pn.pane.Plotly(self.fig)  # Use `self.fig`, already updated
    # ----------------------------------------------------------------------
    def __panel__(self):
        return self.view
    # ----------------------------------------------------------------------
    def debug(self):
        print( "LAYOUT ==============================================================")
        print( self.fig.layout)
        print( "DATA =================================================================")
        print( self.fig.data)


