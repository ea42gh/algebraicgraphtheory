import pytest
import math
import warnings

from alg_graphs.assign_colors_and_sizes import (
    all_values_equal, normalize_values, compute_node_size, compute_edge_width, assign_colors
)
from alg_graphs.color_conversions import (
    convert_to_rgb, convert_to_rgb_batch,
    convert_rgba_to_rgb, convert_hsl_to_rgb,
    convert_hex_to_rgb, convert_named_color_to_rgb
)

def test_all_values_equal():
    """Tests if all_values_equal correctly identifies nearly equal values."""

    # ✅ All values are exactly the same
    values = {'a': 5, 'b': 5, 'c': 5}
    assert all_values_equal(values) is True

    # ✅ Values differ significantly
    values = {'a': 5, 'b': 10, 'c': 5}
    assert all_values_equal(values) is False

    # ✅ Values are within the default tolerance
    values = {'a': 5.0000001, 'b': 5.0000002, 'c': 5.0000003}
    assert all_values_equal(values) is True

    # ✅ Values are just outside tolerance
    values = {'a': 5.000001, 'b': 5.000002, 'c': 5.00001}
    assert all_values_equal(values, tolerance=1e-7) is False

    # ✅ Empty dictionary should return True
    assert all_values_equal({}) is True

    # ✅ Dictionary with None values should return True (ignores them)
    values = {'a': None, 'b': None, 'c': None}
    assert all_values_equal(values) is True

    # ✅ Dictionary with some None values but equal numbers
    values = {'a': None, 'b': 5, 'c': 5}
    assert all_values_equal(values) is True

def test_compute_node_size():
    """Tests node size computation from a dictionary."""

    # ✅ Basic case with different values
    values = {'a': 1, 'b': 3, 'c': 5}
    result = compute_node_size(values, min_size=10, max_size=50)
    assert result['a'] == 10  # Smallest value
    assert result['c'] == 50  # Largest value
    assert 10 < result['b'] < 50  # Intermediate value

    # ✅ Edge case: all values are the same
    uniform_values = {'x': 4, 'y': 4, 'z': 4}
    result = compute_node_size(uniform_values, min_size=10, max_size=50)
    assert len(set(result.values())) == 1  # All sizes should be the same

    # ✅ Test with an equalizer function (log scale)
    result = compute_node_size(values, min_size=10, max_size=50, equalizer=math.log1p)
    assert all(10 <= v <= 50 for v in result.values())


def test_compute_edge_width():
    """Tests edge width computation from a dictionary."""

    # ✅ Basic case with different values
    values = {('a', 'b'): 1, ('b', 'c'): 3, ('c', 'd'): 5}
    result = compute_edge_width(values, min_width=1, max_width=10)
    assert result[('a', 'b')] == 1  # Smallest value
    assert result[('c', 'd')] == 10  # Largest value
    assert 1 < result[('b', 'c')] < 10  # Intermediate value

    # ✅ Edge case: all values are the same
    uniform_values = {('x', 'y'): 4, ('y', 'z'): 4, ('z', 'w'): 4}
    result = compute_edge_width(uniform_values, min_width=1, max_width=10)
    assert len(set(result.values())) == 1  # All widths should be the same

    # ✅ Test with an equalizer function (log scale)
    result = compute_edge_width(values, min_width=1, max_width=10, equalizer=math.log1p)
    assert all(1 <= v <= 10 for v in result.values())

def test_assign_colors():
    """Tests color assignment with batch conversion and tolerance-based equality detection."""

    # ✅ Basic case with different values
    values = {'a': 1, 'b': 3, 'c': 5}
    result = assign_colors(values, cmap="viridis")

    assert isinstance(result, dict)
    assert all(v.startswith("rgb(") for v in result.values())  # Ensures correct RGB format

    # ✅ Edge case: all values are exactly the same
    uniform_values = {'x': 4, 'y': 4, 'z': 4}
    result = assign_colors(uniform_values, cmap="magma")
    assert len(set(result.values())) == 1  # All should have the same color

    # ✅ Edge case: all values are within tolerance
    uniform_values = {'x': 4.000001, 'y': 4.000002, 'z': 4.000003}
    result = assign_colors(uniform_values, cmap="magma", all_equal_tolerance=1e-5)
    assert len(set(result.values())) == 1  # All should have the same color

    # ✅ Values slightly outside tolerance should result in varied colors
    varied_values = {'x': 4.000001, 'y': 4.000002, 'z': 4.000001}
    result = assign_colors(varied_values, cmap="magma", all_equal_tolerance=1e-7)
    assert len(set(result.values())) > 1  # Colors should be different

    # ✅ Invalid colormap (should return gray and warn)
    with pytest.warns(UserWarning):
        result = assign_colors(values, cmap="invalid_map")
    assert all(v == "rgb(128,128,128)" for v in result.values())

    # ✅ Empty input
    assert assign_colors({}) == {}

    # ✅ Handle None values (they should be ignored)
    mixed_values = {'a': 1, 'b': None, 'c': 3}
    result = assign_colors(mixed_values, cmap="plasma")

    assert isinstance(result, dict)
    assert 'b' not in result  # 'b' should be removed
    assert 'a' in result and 'c' in result  # Other keys should remain
    assert all(v.startswith("rgb(") for v in result.values())  # Ensure correct RGB format

def test_assign_colors_single_color():
    """Test that assign_colors returns the same color for identical values."""

    uniform_values = {'a': 5, 'b': 5, 'c': 5}  # All values are identical
    result = assign_colors(uniform_values, cmap="viridis")

    assert isinstance(result, dict)
    assert len(set(result.values())) == 1  # All should have the same color
    assert all(v.startswith("rgb(") for v in result.values())  # Ensures valid format

def test_assign_colors_identical_values():
    """Test that assign_colors assigns a single color when all values normalize to the same value."""

    # All values are identical → They will normalize to the same number
    uniform_values = {'a': 5, 'b': 5, 'c': 5}

    result = assign_colors(uniform_values, cmap="viridis")

    # Ensure all entries in result have the same color
    assert isinstance(result, dict)
    assert len(set(result.values())) == 1  # All colors should be identical
    assert all(v.startswith("rgb(") for v in result.values())  # Ensure valid RGB format

def test_assign_colors_fallback_to_gray():
    """Test that assign_colors falls back to gray if convert_to_rgb_batch() returns an empty dict."""
    
    # These values normalize to the same number (all become 0.5)
    values = {'a': 3, 'b': 3, 'c': 3}

    # Force convert_to_rgb_batch() to return an empty dictionary
    from unittest.mock import patch
    with patch("alg_graphs.assign_colors_and_sizes.convert_to_rgb_batch", return_value={}):
        result = assign_colors(values, cmap="viridis")

    # Ensure all entries get the default gray color
    assert isinstance(result, dict)
    assert all(v == "rgb(128,128,128)" for v in result.values())  # Ensure fallback is triggered

def test_assign_colors_identical_normalized_values():
    """Test that assign_colors assigns a single color when all normalized values are identical."""
    
    # These values normalize to the same number (all become 0.5)
    values = {'a': 3, 'b': 3, 'c': 3}

    result = assign_colors(values, cmap="viridis")

    # Ensure all entries in result have the same color
    assert isinstance(result, dict)
    assert len(set(result.values())) == 1  # All colors should be identical
    assert all(v.startswith("rgb(") for v in result.values())  # Ensure valid RGB format

def test_normalize_values_empty_dict():
    """Test that an empty dictionary returns an empty dictionary."""
    assert normalize_values({}, 0, 1) == {}  # Case 1: Empty input

def test_normalize_values_all_invalid():
    """Test that a dictionary with only None or invalid values returns an empty dictionary with a warning."""
    with pytest.warns(UserWarning, match="All values are None or invalid. Returning an empty dictionary."):
        assert normalize_values({"a": None, "b": "invalid", "c": []}, 0, 1) == {}  # Case 2: All invalid

def test_normalize_values_with_failing_equalizer():
    """Test normalize_values with an equalizer that raises an exception."""

    def failing_equalizer(x):
        raise ValueError("Intentional failure")

    values = {'a': 1, 'b': 4, 'c': 9}

    with pytest.warns(UserWarning, match="Equalizer function failed: Intentional failure. Using raw values."):
        result = normalize_values(values, 0, 1, equalizer=failing_equalizer)

    # The function should fall back to normalizing the raw values
    expected = {'a': 0.0, 'b': 0.375, 'c': 1.0}  # Normalized without transformation
    assert result == expected

