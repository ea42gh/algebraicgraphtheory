import pytest
import networkx as nx
import random
import re

from alg_graphs.graphproperties import (
    compute_node_centrality,
    compute_graph_diameter,
    compute_shortest_paths,
    remove_isolated_nodes,
    convert_to_unweighted_graph,
    assign_random_edge_weights,
    assign_node_community_labels
)

def test_compute_node_centrality():
    G = nx.karate_club_graph()  # A well-known test graph

    # Test Betweenness Centrality
    centrality = compute_node_centrality(G, "betweenness")
    assert isinstance(centrality, dict)
    assert all(isinstance(v, float) for v in centrality.values())

    # Test Degree Centrality
    centrality = compute_node_centrality(G, "degree")
    assert isinstance(centrality, dict)
    assert all(isinstance(v, int) for v in centrality.values())

    # Test Closeness Centrality (Fails for disconnected graphs)
    centrality = compute_node_centrality(G, "closeness")
    assert isinstance(centrality, dict)
    assert all(isinstance(v, float) for v in centrality.values())

    # Test Eigenvector Centrality
    centrality = compute_node_centrality(G, "eigenvector")
    assert isinstance(centrality, dict)
    assert all(isinstance(v, float) for v in centrality.values())

    # Test Unsupported Centrality Type
    with pytest.raises(ValueError):
        compute_node_centrality(G, "invalid_type")

    # Test Disconnected Graph for Closeness Centrality
    G_disconnected = nx.Graph()
    G_disconnected.add_nodes_from(range(5))  # No edges
    with pytest.raises(RuntimeError, match="Closeness centrality is undefined for disconnected graphs"):
        compute_node_centrality(G_disconnected, "closeness")

    # Eigenvector centrality should return a dictionary with all nodes, even if the graph is disconnected
    centrality = compute_node_centrality(G_disconnected, "eigenvector")
    assert isinstance(centrality, dict)
    assert all(isinstance(v, float) for v in centrality.values())

def test_compute_graph_diameter():
    # Test empty graph (should raise an error)
    G = nx.Graph()
    with pytest.raises(ValueError, match="Graph is empty, diameter is undefined."):
        compute_graph_diameter(G)

    # Test single-node graph
    G.add_node(0)
    assert compute_graph_diameter(G) == 0

    # Test a connected cycle graph
    G = nx.cycle_graph(6)  # Diameter should be 3
    assert compute_graph_diameter(G) == 3

    # Test a star graph (central node connected to others)
    G = nx.star_graph(5)  # Diameter should be 2
    assert compute_graph_diameter(G) == 2

    # Test a disconnected graph
    G = nx.Graph()
    G.add_edges_from([(0, 1), (2, 3)])  # Two separate components
    assert compute_graph_diameter(G) == 1  # Each component has a diameter of 1

    # Test a directed graph
    G = nx.DiGraph([(0, 1), (1, 2), (2, 3)])
    assert compute_graph_diameter(G) == 3  # Should correctly convert to undirected

    # Test a weighted graph
    G = nx.Graph()
    G.add_edges_from([(0, 1, {"weight": 2}), (1, 2, {"weight": 3}), (2, 3, {"weight": 4})])
    assert compute_graph_diameter(G, weight="weight") == 9  # Weighted diameter calculation

    # Test approximation
    G = nx.erdos_renyi_graph(100, 0.1)
    approximate_diameter = compute_graph_diameter(G, approximate=True)
    assert isinstance(approximate_diameter, int)

def test_compute_shortest_paths():
    # Test unweighted graph (simple path)
    G = nx.path_graph(5)  # 0 - 1 - 2 - 3 - 4
    result = compute_shortest_paths(G, 0)
    assert result == {0: 0, 1: 1, 2: 2, 3: 3, 4: 4}

    # Test disconnected graph
    G.add_node(99)  # An isolated node
    result = compute_shortest_paths(G, 0)
    assert 99 not in result  # No path to node 99

    # Test directed graph
    DG = nx.DiGraph([(0, 1), (1, 2), (2, 3)])
    result = compute_shortest_paths(DG, 0)
    assert result == {0: 0, 1: 1, 2: 2, 3: 3}

    # Test reverse shortest paths in directed graph
    result = compute_shortest_paths(DG, 3, reverse=True)
    assert result == {3: 0, 2: 1, 1: 2, 0: 3}

    # Test weighted graph
    G = nx.Graph()
    G.add_edges_from([(0, 1, {"weight": 2}), (1, 2, {"weight": 3}), (2, 3, {"weight": 4})])
    result = compute_shortest_paths(G, 0, weight="weight")
    assert result == {0: 0, 1: 2, 2: 5, 3: 9}

    # Test bidirectional Dijkstra selection
    large_dense_G = nx.dense_gnm_random_graph(500, 25000)  # Large, dense graph
    result = compute_shortest_paths(large_dense_G, 0, weight="weight")
    assert isinstance(result, dict)  # Should return a dictionary of distances

    # Test invalid source node
    with pytest.raises(ValueError, match="Source node 10 is not in the graph."):
        compute_shortest_paths(G, 10)

    # Test isolated node (source has no outgoing edges)
    G_isolated = nx.Graph()
    G_isolated.add_nodes_from([0, 1, 2])  # No edges
    result = compute_shortest_paths(G_isolated, 0)
    assert result == {0: 0}  # Only the source is reachable

def test_remove_isolated_nodes():
    # Test a graph with isolated nodes
    G = nx.Graph()
    G.add_nodes_from([0, 1, 2, 3])
    G.add_edge(1, 2)  # Only 1 and 2 are connected, 0 and 3 are isolated
    G_cleaned = remove_isolated_nodes(G)
    assert set(G_cleaned.nodes) == {1, 2}  # Only connected nodes remain

    # Test in-place removal
    G_inplace = nx.Graph()
    G_inplace.add_nodes_from([0, 1, 2, 3])
    G_inplace.add_edge(1, 2)
    remove_isolated_nodes(G_inplace, inplace=True)  # Modify directly
    assert set(G_inplace.nodes) == {1, 2}  # Should match non-inplace version

    # Test a graph with no isolated nodes
    G = nx.path_graph(4)  # No isolated nodes
    G_cleaned = remove_isolated_nodes(G)
    assert set(G_cleaned.nodes) == set(G.nodes)  # Should be unchanged

    # Test a fully isolated graph (all nodes isolated)
    G = nx.Graph()
    G.add_nodes_from(range(5))  # No edges, all nodes are isolated
    G_cleaned = remove_isolated_nodes(G)
    assert len(G_cleaned.nodes) == 0  # Should remove all nodes

    # Test an empty graph
    G = nx.Graph()
    G_cleaned = remove_isolated_nodes(G)
    assert len(G_cleaned.nodes) == 0  # Should remain empty

    # Test removing nodes with degree < 2 (custom condition)
    G = nx.Graph()
    G.add_edges_from([(0, 1), (1, 2), (2, 3)])  # A linear chain
    G_cleaned = remove_isolated_nodes(G, condition=lambda node, degree: degree < 2)
    assert set(G_cleaned.nodes) == {1, 2}  # Nodes at the ends should be removed

    # Performance test for large graphs
    G_large = nx.erdos_renyi_graph(10000, 0.0001)  # Large sparse graph with many isolated nodes
    G_large_cleaned = remove_isolated_nodes(G_large)
    assert len(G_large_cleaned.nodes) <= len(G_large.nodes)  # Should remove some nodes

def test_convert_to_unweighted_graph():
    # ✅ Test an already unweighted graph (should return the same object)
    G_unweighted_already = nx.Graph()
    G_unweighted_already.add_edge(0, 1)
    G_unweighted_result = convert_to_unweighted_graph(G_unweighted_already)
    assert G_unweighted_result is G_unweighted_already  # Should return the same object

    # ✅ Test an already unweighted directed graph (should return the same object)
    DG_unweighted_already = nx.DiGraph()
    DG_unweighted_already.add_edge(0, 1)
    DG_unweighted_result = convert_to_unweighted_graph(DG_unweighted_already, preserve_direction=True)
    assert DG_unweighted_result is DG_unweighted_already  # Should return the same object

    # ✅ Test a weighted graph (should return a new object)
    G_weighted = nx.Graph()
    G_weighted.add_edge(0, 1, weight=5)
    G_weighted_result = convert_to_unweighted_graph(G_weighted)
    assert G_weighted_result is not G_weighted  # Should be a new object
    assert 'weight' not in G_weighted_result[0][1]  # Weight should be removed

    # ✅ Test a weighted directed graph (should return a directed graph if preserve_direction=True)
    DG_weighted = nx.DiGraph()
    DG_weighted.add_edge(0, 1, weight=10)
    DG_weighted_result = convert_to_unweighted_graph(DG_weighted, preserve_direction=True)
    assert isinstance(DG_weighted_result, nx.DiGraph)  # Should still be directed
    assert set(DG_weighted_result.edges) == {(0, 1)}
    assert 'weight' not in DG_weighted_result[0][1]  # Weight should be removed

    # ✅ Test MultiGraph conversion (multiple edges should be retained)
    MG = nx.MultiGraph()
    MG.add_edge(0, 1, weight=2)
    MG.add_edge(0, 1, weight=3)
    MG_unweighted = convert_to_unweighted_graph(MG)
    assert set(MG_unweighted.edges) == {(0, 1)}  # Should still keep one edge

    # ✅ Test MultiGraph with attribute preservation
    MG_preserved = convert_to_unweighted_graph(MG, preserve_attrs=True)
    assert len(MG_preserved.edges) == 1  # Edge count should remain the same

    # ✅ Test preserving attributes (except weight)
    G_attrs = nx.Graph()
    G_attrs.add_edge(0, 1, weight=3, color="red", label="A")
    G_preserved = convert_to_unweighted_graph(G_attrs, preserve_attrs=True)
    assert 'color' in G_preserved[0][1]  # Should still have color
    assert 'label' in G_preserved[0][1]  # Should still have label
    assert 'weight' not in G_preserved[0][1]  # Should remove weight

    # ✅ Test using a custom filter to remove only "color"
    G_filtered = convert_to_unweighted_graph(G_attrs, edge_filter=lambda attrs: {k: v for k, v in attrs.items() if k != "color"})
    assert 'color' not in G_filtered[0][1]
    assert 'label' in G_filtered[0][1]  # Other attributes should be kept

    # ✅ Test empty graph (should return as-is)
    G_empty = nx.Graph()
    G_unweighted_empty = convert_to_unweighted_graph(G_empty)
    assert len(G_unweighted_empty.nodes) == 0
    assert len(G_unweighted_empty.edges) == 0

    # ✅ Test graph with no edges but with nodes (should return as-is)
    G_nodes_only = nx.Graph()
    G_nodes_only.add_nodes_from([0, 1, 2])
    G_nodes_only_result = convert_to_unweighted_graph(G_nodes_only)
    assert G_nodes_only_result is G_nodes_only  # Should return the same object

    # ✅ Test edge_filter returning invalid values (should not break execution)
    G_invalid_filter = convert_to_unweighted_graph(G_attrs, edge_filter=lambda attrs: None)
    assert set(G_invalid_filter.edges) == set(G_attrs.edges())  # Should keep same edges

    # ✅ Test performance on a large graph
    G_large = nx.fast_gnp_random_graph(10000, 0.0005, seed=42)  # Sparse graph with many edges
    G_large_unweighted = convert_to_unweighted_graph(G_large)
    assert len(G_large_unweighted.nodes) == len(G_large.nodes)  # Nodes should be unchanged
    assert len(G_large_unweighted.edges) == len(G_large.edges)  # Edges should be unchanged

    G = nx.Graph()
    G.add_edge(0, 1, weight=5, color="blue")

    # Edge filter that returns None (should be converted to {})
    G_filtered = convert_to_unweighted_graph(G, edge_filter=lambda attrs: None)
    assert set(G_filtered.edges) == {(0, 1)}  # Edges should remain
    assert len(G_filtered[0][1]) == 0  # No attributes should remain

def test_assign_node_community_labels():
    # ✅ Test assigning numeric labels to a simple graph
    G = nx.Graph()
    G.add_nodes_from(range(5))  # Nodes: 0, 1, 2, 3, 4
    G_labeled = assign_node_community_labels(G, num_communities=3)
    assert all(1 <= G_labeled.nodes[n]['community'] <= 3 for n in G_labeled.nodes())

    # ✅ Test custom community labels
    G_custom = assign_node_community_labels(G, community_labels=["Red", "Blue", "Green"])
    assert all(G_custom.nodes[n]['community'] in ["Red", "Blue", "Green"] for n in G_custom.nodes())

    # ✅ Test that different runs give different labels (randomization check)
    G_labeled_2 = assign_node_community_labels(G, num_communities=3)
    assert any(G_labeled.nodes[n]['community'] != G_labeled_2.nodes[n]['community'] for n in G_labeled.nodes())

    # ✅ Test empty graph (should return as-is)
    G_empty = nx.Graph()
    assert assign_node_community_labels(G_empty) is G_empty  # No changes

    # ✅ Test graph with only one node
    G_single = nx.Graph()
    G_single.add_node(0)
    G_single_labeled = assign_node_community_labels(G_single, num_communities=2)
    assert 1 <= G_single_labeled.nodes[0]['community'] <= 2

    # ✅ Test inplace modification
    G_inplace = nx.Graph()
    G_inplace.add_nodes_from(range(5))
    assign_node_community_labels(G_inplace, num_communities=4, inplace=True)
    assert 'community' in G_inplace.nodes[0]  # Check if inplace modification happened

    # ✅ Test invalid num_communities values
    with pytest.raises(ValueError, match="num_communities must be a positive integer"):
        assign_node_community_labels(G, num_communities=0)  # Zero is not allowed

    with pytest.raises(ValueError, match="num_communities must be a positive integer"):
        assign_node_community_labels(G, num_communities=-5)  # Negative values are invalid

    with pytest.raises(ValueError, match="num_communities must be a positive integer"):
        assign_node_community_labels(G, num_communities="three")  # Non-integer values are invalid

    # ✅ Test invalid custom community labels
    with pytest.raises(ValueError, match="community_labels must be a non-empty list."):
        assign_node_community_labels(G, community_labels="Red,Blue,Green")  # Must be a list

    with pytest.raises(ValueError, match="community_labels must be a non-empty list."):
        assign_node_community_labels(G, community_labels=[])  # Empty list is not allowed

