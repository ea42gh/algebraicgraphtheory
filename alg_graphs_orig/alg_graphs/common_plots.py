import numpy as np
import holoviews as hv
from IPython.display import Markdown, display
hv.extension('bokeh', logo=False)
# -----------------------------------------------------------------------------------------------
def _pr( txt, color='blue',sz=20,ht=25): return f'<strong style="font-size:{sz}px;height:{ht}px;color:{color};">{txt}</strong>'
def pr( txt, color='blue',sz=20,ht=25):  display(Markdown(f'<strong style="font-size:{sz}px;height:{ht}px;color:{color};">{txt}</strong>'))
# -----------------------------------------------------------------------------------------------
def is_iterable_of_iterables(obj):
    from collections.abc import Iterable

    if isinstance(obj, Iterable) and not isinstance(obj, (str, bytes)):
        return any(isinstance(item, Iterable) for item in obj)
    return False

def stem_plot(data, curve=False, baseline=True, basevalue=None, marker=True, kdims=["x","y"], **args):
    if basevalue is None:  basevalue = 0

    if is_iterable_of_iterables(data):
        x = data[0]
        e = data[1]
    else:
        x = list( range(1,len(data)+1))
        e = data
        
    e = np.array(e).reshape(len(e))
    y = np.full( len(e), basevalue)

    vlines = [ np.array( [[x[i], y[i]], [x[i], e[i]]]) for i in range(len(x)) ]

    hs = hv.Path( vlines, kdims, **args ).opts( show_legend=True, muted_alpha=0.)
    if baseline: hs = hs * hv.HLine(basevalue).opts(line_width=0.4)
    if marker:   hs = hs * hv.Scatter((x,e), *kdims, **args).opts(size=4, muted_alpha=0.)
    if curve:    hs = hs * hv.Curve((x,e), *kdims, **args).opts(line_width=0.8, muted_alpha=0.)

    return hs
# -----------------------------------------------------------------------------------------------
def raster(a): return hv.Raster(np.array(a)).opts(cmap="Greys",xaxis=None,yaxis=None)
# -----------------------------------------------------------------------------------------------
def show_eigval(evals,i):
    h = hv.Scatter( (np.real(evals), np.imag(evals)), "Re( λ )", "Im( λ )").opts( size=8, aspect='equal', tools=['hover'])*\
         hv.HLine(0).opts(line_width=0.4)*hv.VLine(0).opts(line_width=0.4)*\
         hv.Ellipse(0,0,2).opts(line_width=0.5)*\
         hv.Curve( ([0,np.real(evals[i])], [0, np.imag(evals[i])]))
    return h
# -----------------------------------------------------------------------------------------------
def show_eigdecomposition(evecs, omega, i):
    if np.all(np.iscomplex( evecs )):
        re = np.real(evecs[:,[i]]).reshape(evecs.shape[0])
        im = np.imag(evecs[:,[i]]).reshape(evecs.shape[0])
        return stem_plot( re, curve=False, label="Real Part( λ )")*\
               stem_plot( im, curve=False, label="Imag Part( λ )")\
                 .opts("Path", title=f"Graph Fourier Basis Eigenvector {i},   ω = {np.round(omega[i],3)}", height=355, width=500, legend_position='top')# -----------------------------------------------------------------------------------------------
    else:
        re = evecs[:,[i]].reshape(evecs.shape[0])
        return stem_plot( re, curve=False, label="λ")\
                 .opts("Path", title=f"Graph Fourier Basis Eigenvector {i},   ω = {np.round(omega[i],3)}", height=355, width=500, legend_position='top')# -----------------------------------------------------------------------------------------------
