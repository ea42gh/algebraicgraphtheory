import math
import warnings
import colorcet as cc
import matplotlib.pyplot as plt
from .color_conversions import convert_to_rgb_batch

def all_values_equal(value_dict:dict, tolerance=1e-6):
    """
    Checks if all numerical values in a dictionary are equal within a given tolerance.

    Args:
        value_dict (dict): Dictionary of numerical values.
        tolerance (float): Acceptable numerical difference.

    Returns:
        bool: True if all values are approximately equal, False otherwise.
    """
    values = [v for v in value_dict.values() if isinstance(v, (int, float))]
    if not values:
        return True  # Default to True for empty input
    return max(values) - min(values) <= tolerance

def normalize_values(value_dict: dict, min_value, max_value, scale_factor=1.0, equalizer=None):
    """Normalizes numerical values between min_value and max_value with an optional transformation."""

    if not value_dict:
        return {}

    # Remove None and non-numeric values
    filtered_values = {k: v for k, v in value_dict.items() if isinstance(v, (int, float))}

    if not filtered_values:
        warnings.warn("All values are None or invalid. Returning an empty dictionary.")
        return {}

    # Apply equalizer if provided
    original_values = filtered_values.copy()
    if equalizer:
        try:
            filtered_values = {k: equalizer(v) for k, v in filtered_values.items()}
        except Exception as e:
            warnings.warn(f"Equalizer function failed: {e}. Using raw values.")
            filtered_values = original_values  # Revert to original values

    v_min, v_max = min(filtered_values.values()), max(filtered_values.values())

    # If all values are the same, return a uniform value
    if v_max == v_min:
        uniform_value = (min_value + max_value) / 2  # Compute midpoint
        return {key: uniform_value * scale_factor for key in value_dict}  # Apply scale after

    # Normalize values
    normalize = lambda v: ((v - v_min) / (v_max - v_min)) * (max_value - min_value) + min_value
    return {key: normalize(value) * scale_factor for key, value in filtered_values.items()}

def compute_node_size(value_dict:dict, min_size=5, max_size=20, scale_factor=1.0, equalizer=None):
    return normalize_values(value_dict, min_size, max_size, scale_factor, equalizer)

def compute_edge_width(value_dict:dict, min_width=0.5, max_width=5.0, scale_factor=1.0, equalizer=None):
    return normalize_values(value_dict, min_width, max_width, scale_factor, equalizer)

def assign_colors(value_dict: dict, cmap="viridis", equalizer=None, all_equal_tolerance=1e-6):
    """Assigns colors to entities (nodes, edges) based on a colormap and numerical values."""

    if not value_dict:
        return {key: "rgb(0,0,0)" for key in value_dict}  # Ensure a valid dictionary is returned

    equal = all_values_equal(value_dict, tolerance=all_equal_tolerance)

    if equal:
        colormap   = cc.cm.get(cmap, plt.get_cmap(cmap))
        base_color = convert_to_rgb_batch({k: colormap(0.5) for k in value_dict})
        return base_color if base_color else {key: "rgb(128,128,128)" for key in value_dict}

    normalized_values = normalize_values(value_dict, 0, 1, equalizer=equalizer)

    # Handle case where all values normalize to the same value
    if len(set(normalized_values.values())) == 1:
        colormap = cc.cm.get(cmap, plt.get_cmap(cmap))
        base_color = convert_to_rgb_batch({k: colormap(0.5) for k in value_dict})
        return base_color if base_color else {key: "rgb(128,128,128)" for key in value_dict}

    # Generate color mapping
    try:
        colormap = cc.cm.get(cmap, plt.get_cmap(cmap.lower()))
    except (KeyError, ValueError):
        warnings.warn(f"Colormap '{cmap}' not found. Defaulting to gray.", UserWarning)
        return {key: "rgb(128,128,128)" for key in value_dict}

    # Convert colormap output to valid "rgb(R,G,B)" format using convert_to_rgb_batch()
    color_mapping = {
        key: f"rgb({int(r * 255)},{int(g * 255)},{int(b * 255)})"
        for key, (r, g, b, *_) in (  # Extracts first 3 values, ignoring alpha if present
            (k, colormap(v)[:3]) for k, v in normalized_values.items()
        )
    }

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", UserWarning)
        result = convert_to_rgb_batch(color_mapping)

    return result if result else {key: "rgb(128,128,128)" for key in value_dict}  # Ensure valid return
# 18-21, 26-54, 57, 60, 65-102
