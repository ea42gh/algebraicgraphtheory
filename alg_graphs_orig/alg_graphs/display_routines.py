import holoviews as hv; hv.extension('bokeh', 'matplotlib', logo=False)
import panel as pn;     pn.extension('katex', 'mathjax')
import hvplot.networkx as hvnx

import matplotlib.pyplot as plt
from matplotlib.figure import Figure   # <- necessary to use panel!

#import graphblas as gb
#from graphblas import Matrix, Vector, Scalar
#from graphblas import unary, binary, monoid, semiring

import pygsp as pg
import networkx as nx
import numpy as np
import scipy as sp
import param

from IPython.display import HTML as html_print
from IPython.display import Markdown, display
# -----------------------------------------------------------------------------------------------
def nx_from_pygsp(G, layout=nx.spring_layout, signal=None):
    ''' Convert pygsp graph to a networkx graph TODO: find the pygsp ustility to do so'''
    nxG = nx.Graph()
    for k in range(G.N): nxG.add_node(k)
    u,v,w = G.get_edge_list()
    for i in range(G.Ne): nxG.add_edge( u[i], v[i], weight=w[i])
    
    #nxG.add_weighted_edges_from( list( zip(*G.get_edge_list())) )
    if hasattr(G, 'coords'):
        nx.set_node_attributes(nxG, {str(i): (p[0],p[1]) for (i,p) in enumerate(G.coords)}, 'coord')
    else:
        nx.set_node_attributes( nxG, {str(i): p for (i,p) in enumerate( layout( nxG ))}, 'coord')
    if signal is not None:
        nx.set_node_attributes(nxG, {str(i): signal[i] for i in range(len(signal))}, 'signal') 

    return nxG
# -----------------------------------------------------------------------------------------------
# hvnx drawing routines
# -----------------------------------------------------------------------------------------------
def make_colorbar(cmap, clim, orientation = 'horizontal', position = 'top', colorbar_opts = {}, **kwargs):
    ## create an invisible hv.Heatmap plot just to use its colorbar
    hm = hv.HeatMap([(0, 0, clim[0]), (0, 1, clim[1])])
    kwargs.update(dict(colorbar=True, 
                   colorbar_opts=colorbar_opts, 
                   clim=clim,
                   alpha=0,
                   show_frame=False,
                   frame_height=0,
                   colorbar_position=position, 
                   toolbar="disable",
                   margin=(-10,0),
                   cmap=cmap,))

    return hm.opts(**kwargs)
# -----------------------------------------------------------------------------------------------
def _plot_nx( nxG, pos=None, node_size=250, node_color="lightgreen",  node_cmap='fire',
                   edge_width=hv.dim('weight'), edge_color=hv.dim('weight'), edge_cmap='fire',
                   arrows=True, arrowhead_length=0.05, custom_colorbar=True, font_weight='bold', font_color='black',
             with_labels=True, colorbar=True ):

    #print("node", node_size, node_color, node_cmap)
    #print("edge", edge_width, edge_color, edge_cmap)

    h   = hvnx.draw(nxG, pos,  node_size=node_size, node_color=node_color,node_cmap=node_cmap,
                    edge_width=edge_width, edge_color=edge_color, edge_cmap=edge_cmap,
                    arrows=arrows, arrowhead_length=arrowhead_length,
                    with_labels=with_labels, font_weight=font_weight, font_color=font_color, #font_size=14,
                   #hv.dim('size')*70,th
                    colorbar=not custom_colorbar ).opts(width=600, tools=['hover', 'box_select'])
    # ------------------------------------------------------ make colorbars
    node_cbar = hv.Curve([]).opts(height=1, xaxis=None, yaxis=None, show_frame=False, toolbar="disable" )
    edge_cbar = hv.Curve([]).opts(height=1, xaxis=None, yaxis=None, show_frame=False, toolbar="disable" )
    if custom_colorbar:
        if isinstance(node_color, list) or isinstance(node_color, np.ndarray):
            node_clim = (min(node_color), max(node_color))
            node_cbar = make_colorbar( node_cmap, node_clim, colorbar_opts = {'title': 'node color'}, xaxis=None, yaxis=None, width=200)
        if not isinstance(edge_color, str):
            if isinstance( edge_color, hv.util.transform.dim ):
                edge_clim = nx.get_edge_attributes(nxG,'weight').values()
                edge_clim = (min(edge_clim), max(edge_clim))
            else:
                edge_clim = (min(edge_color), max(edge_color))
            edge_cbar = make_colorbar( edge_cmap, edge_clim, colorbar_opts = {'title': 'edge color'}, xaxis=None, yaxis=None, width=200)

    return h, node_cbar, edge_cbar

def _plot_pygsp(G, node_size=250, node_color="lightgreen",  node_cmap='fire',
                   edge_width=hv.dim('weight'), edge_color=hv.dim('weight'), edge_cmap='fire',
                   arrows=True, arrowhead_length=0.05, custom_colorbar=True, font_weight='bold', font_color='black', with_labels=True):
    nxG = nx_from_pygsp(G)
    pos = {i: (p[0],p[1]) for (i,p) in enumerate(G.coords)} if  hasattr(G, 'coords') else None
    h,cn,ce = _plot_nx( nxG, pos, node_size=node_size, node_color=node_color,node_cmap=node_cmap,
                    edge_width=edge_width, edge_color=edge_color, edge_cmap=edge_cmap,
                    arrows=arrows, arrowhead_length=arrowhead_length,
                    with_labels=with_labels, font_weight=font_weight, font_color=font_color, #font_size=14,
                   #hv.dim('size')*70,th
                    colorbar=not custom_colorbar )
    return h.opts(width=600, tools=['hover', 'box_select']), cn, ce

def plot_pygsp(G, node_size=250, node_color="lightgreen",  node_cmap='fire',
                  edge_width=hv.dim('weight'), edge_color=hv.dim('weight'), edge_cmap='fire',
                  arrows=True, arrowhead_length=0.05, custom_colorbar=True, font_weight='bold', font_color='black', with_labels=True):
    h,_,_ = _plot_pygsp(G, node_size=node_size, node_color=node_color,node_cmap=node_cmap,
                    edge_width=edge_width, edge_color=edge_color, edge_cmap=edge_cmap,
                    arrows=arrows, arrowhead_length=arrowhead_length, custom_colorbar=custom_colorbar, font_weight=font_weight, font_color=font_color, with_labels=with_labels)
    return h
# -----------------------------------------------------------------------------------------------
class NxDisplay(param.Parameterized):
    #height       = param.Integer(precedence = -1)
    
    layout      = param.Selector(objects = [ #"bipartite_layout", , "rescale_layout", "rescale_layout_dict", "multipartite_layout"
                                             "spring_layout", "spectral_layout", "planar_layout",
                                             "circular_layout", "kamada_kawai_layout",
                                             "random_layout",
                                             "shell_layout", "fruchterman_reingold_layout",
                                             "spiral_layout", "arf_layout",], default="spring_layout")

    node_color  = param.String(precedence=-1)
    edge_color  = param.String(precedence=-1)

    node_cmap   = param.Selector(objects = ['None', 'cwr', 'kb', 'kbc', 'BrBG', 'bwy', 'gwv', 'PuOr', 'Spectral', 'Plasma', 'Viridis', 'blues'], default = 'cwr')
    edge_cmap   = param.Selector(objects = ['None', 'cwr', 'kb', 'kbc', 'BrBG', 'bwy', 'gwv', 'PuOr', 'Spectral', 'Plasma', 'Viridis', 'blues', 'dimgray', 'gray'], default = 'blues')

    node_size   = param.Number(250, bounds=(0,None), softbounds=(None,2000))
    edge_width  = param.Number(1,   bounds=(0,5),    softbounds=(None,5))

    def __init__(self, G, f, s=None, **params):
        super(NxDisplay, self).__init__(**params)
        self.set_data(G, f, s, update_plot=False)
        self._set_pos()
        self.arrowhead_length=0.01

        self.graph_pane      = pn.pane.HoloViews()
        self.node_cbar_pane  = pn.pane.HoloViews()
        self.edge_cbar_pane  = pn.pane.HoloViews()

        self.c_width   = 250
        self.tot_width = 800
        self.height    = 800

    def set_data( self, G, f, s, update_plot=True):
        if G is not None: self.G = G
        if f is not None: self.f = f
        self.s = s
        if hasattr(self, 'title') and update_plot: self._plot()

    def set_nxgraph( self, G, update_plot=False): self.set_data( G, None, None, update_plot)
    def set_node_signal( self, f, update_plot=False): self.set_data( None, f, None, update_plot)
    def set_edge_signal( self, s, update_plot=False): self.set_data( None, None, s, update_plot)


    @param.depends('layout', watch=True)
    def _set_pos(self):
        self.coords = eval(f"nx.{self.layout}(self.G)")
        if hasattr(self, 'title'): self._plot()

    @param.depends('node_cmap', 'edge_cmap', 'node_size', 'edge_width', watch=True)
    def _plot(self):

        def set( cmap, col, sz, d_cmap, d_col, d_sz ):
            if cmap != "None":
                return cmap, col, sz
            else:
                return d_cmap, d_col, d_sz

        edata = hv.dim('weight') if self.s is None else self.s

        ncmap, ncol, nsz = set( self.node_cmap, self.f,                           self.node_size*0.9*(self.f-min(self.f)),
                                "cwr",          "lightgreen",                     self.node_size   )
        ecmap, ecol, esz = set( self.edge_cmap, edata,                            self.edge_width*edata,
                                "cwr",          "black",                          self.edge_width  )

        plot_opts = { 'arrows': self.G.is_directed(), 'arrowhead_length': self.arrowhead_length,
                      'node_cmap': ncmap, 'node_color': ncol, 'node_size':  nsz,
                      'edge_cmap': ecmap, 'edge_color': ecol, 'edge_width': esz }
        #print(plot_opts)
        h,cn,ce = _plot_nx( self.G, self.coords, **plot_opts)
        self.graph_pane.object     = h.opts(height=self.height, width=self.tot_width, title = self.title)
        self.node_cbar_pane.object = cn.opts(width=self.c_width+10)
        self.edge_cbar_pane.object = ce.opts(width=self.c_width+10)

    def plot_layout(self, c_width=250, tot_width=800, height=800, title=''):
        self.height    = height
        self.c_width   = c_width
        self.tot_width = tot_width
        self.title     = title
        self._plot()

        return pn.Column( self.graph_pane,
                   pn.Row( self.node_cbar_pane, self.edge_cbar_pane),
                   width = tot_width )
# -----------------------------------------------------------------------------------------------
def pr( txt, color='blue',sz=20,ht=25):  display(Markdown(f'<strong style="font_size:{sz}px;height:{ht}px;color:{color};">{txt}</strong>'))
# -----------------------------------------------------------------------------------------------
def nxgraph_from_adjacencymatrix( adjacency_matrix, graph_type=nx.Graph ):
    am         = np.array(adjacency_matrix)
    rows, cols = np.where(am == 1)
    edges      = zip([i+1 for i in rows.tolist()], [j+1 for j in cols.tolist()])
    gr         = graph_type()
    nxG        = nx.Graph()
    for k in range(am.shape[0]): nxG.add_node(k)
    gr.add_edges_from(edges)
    return gr
# -----------------------------------------------------------------------------------------------
def draw_nxgraph(G, layout=None, figsize=(4,3), options=None):
    if options is None:
        options = {
            'node_color': 'lightgreen',
            'node_size': 200,
            'width': 1,
            'font_weight': 'bold',
        }
    fig  = Figure(figsize=figsize)
    ax   = fig.subplots()
    pos  = None if layout is None else layout(G)
    nx.draw(G, pos=pos, with_labels=True, **options, ax=ax)
    return fig
# -----------------------------------------------------------------------------------------------
def draw_nxmultigraph(G, layout=nx.random_layout, figsize=(4,3), arrows='->',
                      node_options=None, label_options=None, e_lbl=None, elabel_options=None ):
    pos   = layout(G)
    names = {name: name for name in G.nodes}

    if node_options is None:
        node_options = {
            'node_color': 'lightgreen',
            'node_size': 200,
        }
    if label_options is None:
        label_options = { #font_size=12,font_color='w', 'font_weight': 'bold',
            'font_color':  'b',
        }
    if elabel_options is None:
        elabel_options = { #font_size=12,font_color='w', 'font_weight': 'bold',
            'font_color':  'b',
        }
    fig  = Figure(figsize=figsize)
    ax   = fig.subplots()

    nx.draw_networkx_nodes( G, pos, **node_options, ax=ax) #node_color = 'b', node_size = 250, alpha = 1)
    nx.draw_networkx_labels(G, pos, names, **label_options, ax=ax )

    # draw loops, if any
    loops = [e for e in G.edges if e[0]==e[1]]
    if len(loops) > 0:
        loop_vertices = {e[0] for e in loops}
        G_loops = nx.MultiGraph()
        G_loops.add_edges_from(loops)
        if arrows is None or arrows=='-':
            nx.draw_networkx_edges( G_loops, pos={k: pos[k] for k in loop_vertices}, ax=ax)
        else:
            nx.draw_networkx_edges( G_loops, pos={k: pos[k] for k in loop_vertices}, arrows=(arrows!='-'), arrowstyle=arrows, ax=ax)

    if e_lbl is not None:
        edge_labels = dict([((n1, n2), d[e_lbl])
                    for n1, n2, d in G.edges(data=True)])
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels,
                                     label_pos=0.5, **elabel_options, ax=ax )

    for e in G.edges:
        l = 0 if len(e)==2 else e[2]
        ax.annotate("",
                    xy         = pos[e[1]], xycoords='data',
                    xytext     = pos[e[0]], textcoords='data',
                    arrowprops = dict(arrowstyle=arrows, color="0",
                                    shrinkA=10, shrinkB=10,
                                    patchA=None, patchB=None,
                                    connectionstyle=f"arc3,rad={str(0.3*l)}",
                                    ),
                    )
    ax.axis("off")

    return fig
# -----------------------------------------------------------------------------------------------
##def format_matrix(matrix, environment="pmatrix", formatter=str, col_partition=None, row_partition = None):
##    """Format a matrix using LaTeX syntax"""
##    if col_partition is not None:
##        environment="array"
##        if isinstance( col_partition, int): col_partition=[col_partition]
##    if row_partition is not None:
##        if isinstance( row_partition, int): row_partition=[row_partition]
##
##    if not isinstance(matrix, np.ndarray):
##        try:
##            matrix = np.array(matrix)
##        except Exception:
##            raise TypeError("Could not convert to Numpy array")
##
##    if len(shape := matrix.shape) == 1:
##        matrix = matrix.reshape(1, shape[0])
##    elif len(shape) > 2:
##        raise ValueError("Array must be 2 dimensional")
##
##    body_lines = [" & ".join(map(formatter, row)) for row in matrix]
##    if environment == "array":
##        if col_partition is None:
##            fmt = str(matrix.shape[1]*'r')
##        else:
##            diff = [col_partition[i]-col_partition[i-1] for i in range(1,len(col_partition))]
##            fmt = "|".join([str(col_partition[0]*"r")]+
##                  [k*"r" for k in diff] +
##                  [str((matrix.shape[1]-col_partition[-1])*"r")])
##    if row_partition is None:
##        body = "\\\\".join(body_lines)
##    else:
##        num = len(row_partition)
##        diff = [row_partition[i]-row_partition[i-1] for i in range(1,num)]
##        mid  = [body_lines[row_partition[k-1]:row_partition[k]] for k in range(1,num) ]
##        if not mid:
##            body = " \\\\ \\hline ".join([ body_lines[0:row_partition[0]][0],
##                   body_lines[row_partition[-1]:][0]
##            ])
##        else:
##            body = " \\\\ \\hline ".join([ body_lines[0:row_partition[0]][0],
##                   mid,
##                   body_lines[row_partition[-1]:][0]
##            ])
##
##    if environment=='array':
##        txt = r'\left(\begin{array}{'+ fmt +'}'+body+r'\end{array}\right)'
##    else:
##        txt = f"""\\begin{{{environment}}}
##{body}
##\\end{{{environment}}}"""
##    return txt
##def _format_matrix(matrix, environment="pmatrix", formatter=str, col_partition=None, row_partition = None):
##    """Format a matrix using LaTeX syntax"""
##    if col_partition is not None: environment="array"
##
##    if not isinstance(matrix, np.ndarray):
##        try:
##            matrix = np.array(matrix)
##        except Exception:
##            raise TypeError("Could not convert to Numpy array")
##
##    if len(shape := matrix.shape) == 1:
##        matrix = matrix.reshape(1, shape[0])
##    elif len(shape) > 2:
##        raise ValueError("Array must be 2 dimensional")
##
##    body_lines = [" & ".join(map(formatter, row)) for row in matrix]
##    if environment == "array":
##        if col_partition is not None:
##            fmt = str(matrix.shape[1]*'r')
##        else:
##            diff = [col_partition[i]-col_partition[i-1] for i in range(1,len(col_partition))]
##            fmt = str(col_partition[0])*"r" +\
##                  "|".join([k*"r" for k in diff] ) +\
##                  str((matrix.shape[1]-col_partition[-1])*"r")
##        print(fmt)
##
##    body = "\\\\".join(body_lines)
##    if environment=='array':
##        txt = r'\left(\begin{array}{'+ str(matrix.shape[1]*'r')+'}'+body+r'\end{array}\right)'
##    else:
##        txt = f"""\\begin{{{environment}}}
##{body}
##\\end{{{environment}}}"""
##    return txt
