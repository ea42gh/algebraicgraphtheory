import logging
import asyncio
import pytest
import param
import os
import sys
import json
from alg_graphs.state_transitions import ParamStateMachine, logger

from unittest.mock import Mock, MagicMock, AsyncMock, patch
from unittest.mock import mock_open, patch

@pytest.fixture(autouse=True)
def log_test_name(request, caplog):
    """Logs the test function name before execution."""
    test_name = request.node.name
    logging.info(f"[TEST START] ================================================= Running {test_name}")

@pytest.fixture
def mock_viz():
    """Creates a mock visualization object with mock functions."""
    mock = MagicMock()
    mock.test_function = AsyncMock()
    return mock

@pytest.fixture
def state_machine():
    """Creates a fresh instance of ParamStateMachine for each test."""
    mock_viz = MagicMock()
    instance = ParamStateMachine(viz=mock_viz)

    #  Save the original attributes before modification
    original_has_state_sequences = hasattr(instance, "state_sequences")
    original_state_sequences = instance.state_sequences.copy() if original_has_state_sequences else None

    yield instance  #  Give the test access to `instance`

    #  Restore `state_sequences` to its original state
    if original_has_state_sequences:
        instance.state_sequences = original_state_sequences
    else:
        delattr(instance, "state_sequences")  #  Ensures it's fully removed


# 1️⃣ Initialization Tests
def test_initialization(state_machine):
    """Ensure that the state machine initializes with the correct default values."""
    assert state_machine.current_state == state_machine.State.INIT
    assert state_machine.execution_queue.qsize() == 0
    assert state_machine.running is False

# 2️⃣ Parameter Management Tests
def test_add_param(state_machine):
    """Verify that parameters are correctly added."""
    state_machine.add_param("param1", "state1")
    assert state_machine.param_to_state["param1"] == "state1"

def test_add_param_overwrite_warning(state_machine, caplog):
    """Ensure that adding an existing parameter logs a warning."""
    state_machine.add_param("param1", "state1")
    state_machine.add_param("param1", "state2")  # Overwriting
    assert "WARNING" in caplog.text
    assert "Parameter 'param1' is already registered" in caplog.text

@pytest.mark.asyncio
async def test_add_param_auto_registers_state(state_machine, caplog):
    """
    Ensures that when a parameter is added, its state is automatically registered
    and a self-loop transition is created if missing.
    """

    param_name = "param1"
    state_name = "stateA"

    #  Step 1: Add the parameter
    state_machine.add_param(param_name, state_name)

    #  Step 2: Verify the parameter is stored correctly
    assert state_machine.param_to_state[param_name] == state_name, \
        f"[TEST FAILED] Parameter '{param_name}' was not registered correctly."

    #  Step 3: Check if the state transition was added
    assert (param_name, state_name) in state_machine.state_transitions, \
        f"[TEST FAILED] State '{state_name}' was not auto-registered for parameter '{param_name}'."

    #  Step 4: Ensure the transition is a self-loop
    assert state_machine.state_transitions[(param_name, state_name)] == state_name, \
        f"[TEST FAILED] Auto-registered transition for '{state_name}' is incorrect."

    #  Step 5: Verify logging messages
    assert f"[SUCCESS] Parameter '{param_name}' registered with state '{state_name}'." in caplog.text, \
        "[TEST FAILED] Expected success message missing in logs."

    assert f"[AUTO REGISTER] Added missing state '{state_name}' as self-transition." in caplog.text, \
        "[TEST FAILED] Auto-registration log missing."


def test_define_param_transition(state_machine):
    """Test defining a parameter transition with a function sequence."""

    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])

    # Ensure the transition is stored correctly
    assert ("param1", "state1") in state_machine.state_sequences
    assert state_machine.state_sequences[("param1", "state1")]["functions"] == ["test_function"]
    assert state_machine.state_sequences[("param1", "state1")]["end_state"] == "state2"

    # Ensure state transition is correctly recorded
    assert state_machine.state_transitions[("param1", "state1")] == "state2"

    # Ensure the parameter is correctly registered with its start state
    assert state_machine.param_to_state["param1"] == "state1"

# 6️⃣ Test: Handling of an Invalid Transition Definition
def test_define_invalid_param_transition(state_machine, caplog):
    """Test defining a parameter transition with an invalid start state."""

    state_machine.define_param_transition("param1", None, "state2", ["test_function"])

    # Ensure the expected error message is in the logs
    assert "Invalid transition definition: param=param1, start_state=None" in caplog.text

@pytest.mark.asyncio
async def test_param_changed_enqueue(state_machine, caplog):
    """Ensure param_changed enqueues execution properly."""

    state_machine.add_param("param1", "state1")  # Ensure param1 is registered before triggering change
    await state_machine.param_changed("param1")

    assert state_machine.execution_queue.qsize() == 1, "[TEST FAILED] Expected parameter to be enqueued."
    assert "[QUEUED]" in caplog.text, "[TEST FAILED] Log did not indicate enqueuing."

@pytest.mark.asyncio
async def test_run_execution_flow(state_machine, caplog):
    """Verify that the execution cycle processes queued parameters."""
    state_machine.running = False
    await state_machine.execution_queue.put("param1")
    await state_machine.run()
    assert "[RUN COMPLETE]" in caplog.text

@pytest.mark.asyncio
async def test_run_already_running(state_machine, caplog):
    """Ensure run() doesn't duplicate execution if already running."""
    state_machine.running = True
    await state_machine.run()
    assert "[RUN] Already running. Exiting." in caplog.text

@pytest.mark.asyncio  #  Ensures async test runs properly
async def test_execute_function_sequence(state_machine):
    """Test that execute_function_sequence calls the expected function."""
    mock_func = MagicMock()

    #  Register function correctly
    state_machine.state_sequences = {"test_state": {"functions": ["test_function"]}}
    state_machine.viz.test_function = mock_func  #  Ensure accessibility

    #  Run execution sequence
    function_sequence = state_machine.state_sequences["test_state"]["functions"]
    await state_machine._execute_function_sequence(function_sequence)  #  Now it's a list

    #  Ensure the function was called
    mock_func.assert_called_once()

@pytest.mark.asyncio
async def test_execute_function_sequence_failure(state_machine, caplog):
    """Test handling function execution failure."""

    caplog.set_level(logging.DEBUG, logger="ParamStateMachine")

    logger = logging.getLogger("ParamStateMachine")
    logger.propagate = True  # Ensure logs propagate to caplog

    #  Remove existing handlers to avoid conflicts
    for handler in logger.handlers[:]:
        logger.removeHandler(handler)

    #  Add caplog.handler explicitly
    logger.addHandler(caplog.handler)

    print("\n Active log handlers before test (after attaching caplog.handler):")
    for handler in logger.handlers:
        print(f"  - {handler}")

    #  Define a real function that raises an exception
    async def failing_function():
        raise Exception("Function Error")

    state_machine.viz.test_function = failing_function  # Assign real function

    #  Add a parameter and execution chain
    state_machine.add_param("param1", "state1")
    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])

    #  Call param_changed() to queue execution
    await state_machine.param_changed("param1")

    #  Ensure all logs are captured before assertion
    await asyncio.sleep(0.5)  # Short delay to allow async logs to appear
    sys.stdout.flush()
    sys.stderr.flush()

    #  Print captured logs for debugging
    print("\n Captured logs:\n", caplog.text)

    assert any(
        msg in caplog.text
        for msg in [
            "Execution error in function: test_function",
            "Execution error in function: test_function | Function Error",
            "Execution error in function: test_function | Simulated Execution Failure"
        ]
    ), f"Expected log not found. Captured logs:\n{caplog.text}"


@pytest.mark.asyncio
async def test_execute_valid_function_sequence(state_machine, caplog):
    """Test execution of a valid function sequence."""
    
    # Define mock functions
    async def mock_func1():
        return "Executed mock_func1"

    async def mock_func2():
        return "Executed mock_func2"

    # Attach them dynamically to the state machine
    setattr(state_machine, "mock_func1", mock_func1)
    setattr(state_machine, "mock_func2", mock_func2)

    function_sequence = ["mock_func1", "mock_func2"]

    with caplog.at_level(logging.INFO):
        success = await state_machine._execute_function_sequence(function_sequence)

    assert success is True, "[TEST FAILED] Expected success for valid function sequence."
    assert "[EXECUTED] mock_func1" in caplog.text, "[TEST FAILED] mock_func1 was not executed."
    assert "[EXECUTED] mock_func2" in caplog.text, "[TEST FAILED] mock_func2 was not executed."


@pytest.mark.asyncio
async def test_execute_function_sequence_with_non_existent_function(state_machine, caplog):
    """Test execution of a sequence where one function is missing."""
    
    async def mock_func():
        return "Executed mock_func"

    setattr(state_machine, "mock_func", mock_func)

    function_sequence = ["mock_func", "non_existent_func"]

    with caplog.at_level(logging.WARNING):
        success = await state_machine._execute_function_sequence(function_sequence)

    assert success is False, "[TEST FAILED] Expected failure due to missing function."
    assert "[EXECUTED] mock_func" in caplog.text, "[TEST FAILED] mock_func was not executed."
    assert "[MISSING] Function 'non_existent_func' is not found or not callable." in caplog.text, \
        "[TEST FAILED] Missing function was not logged correctly."


@pytest.mark.asyncio
async def test_execute_function_sequence_with_invalid_function_name(state_machine, caplog):
    """Test execution when function names are invalid (not strings)."""

    function_sequence = [123, None, {"key": "value"}]

    with caplog.at_level(logging.ERROR):
        success = await state_machine._execute_function_sequence(function_sequence)

    assert success is False, "[TEST FAILED] Expected failure due to invalid function names."
    assert "[INVALID] Function name must be a string" in caplog.text, \
        "[TEST FAILED] Invalid function names were not logged correctly."


@pytest.mark.asyncio
async def test_execute_function_sequence_with_exception(state_machine, caplog):
    """Test execution when a function raises an exception."""
    
    async def failing_func():
        raise Exception("Simulated Failure")

    setattr(state_machine, "failing_func", failing_func)

    function_sequence = ["failing_func"]

    with caplog.at_level(logging.CRITICAL):
        success = await state_machine._execute_function_sequence(function_sequence)

    assert success is False, "[TEST FAILED] Expected failure due to function exception."
    assert "[EXECUTION ERROR] Function 'failing_func' failed: Simulated Failure" in caplog.text, \
        "[TEST FAILED] Function failure was not logged correctly."


@pytest.mark.asyncio
async def test_execute_empty_function_sequence(state_machine, caplog):
    """Test execution with an empty function sequence."""

    function_sequence = []

    with caplog.at_level(logging.INFO):
        success = await state_machine._execute_function_sequence(function_sequence)

    assert success is True, "[TEST FAILED] Expected success for empty function sequence."
    assert "[COMPLETE] Execution finished. All functions executed successfully." in caplog.text, \
        "[TEST FAILED] Empty function sequence was not logged correctly."


@pytest.mark.asyncio
async def test_execute_function_sequence_with_mixed_async_and_sync(state_machine, caplog):
    """Test execution with a mix of async and sync functions."""

    async def async_func():
        return "Executed async_func"

    def sync_func():
        return "Executed sync_func"

    setattr(state_machine, "async_func", async_func)
    setattr(state_machine, "sync_func", sync_func)

    function_sequence = ["sync_func", "async_func"]

    with caplog.at_level(logging.INFO):
        success = await state_machine._execute_function_sequence(function_sequence)

    assert success is True, "[TEST FAILED] Expected success for mixed async/sync function sequence."
    assert "[EXECUTED] sync_func" in caplog.text, "[TEST FAILED] sync_func was not executed."
    assert "[EXECUTED] async_func" in caplog.text, "[TEST FAILED] async_func was not executed."



# 6️⃣  File I/O Tests
@patch("builtins.open", new_callable=MagicMock)
def test_save_to_file(mock_open, state_machine, caplog):
    """Ensure state machine can save to a file without error."""
    state_machine.save_to_file("test.json")
    mock_open.assert_called_once_with("test.json", "w")
    assert "[SAVE SUCCESS]" in caplog.text

@patch("builtins.open", new_callable=MagicMock)
@patch("json.load", return_value={
    "state_transitions": {},
    "param_to_state": {},
    "state_priorities": {},
    "state_sequences": {},
    "recovery_chain": []
})
def test_load_from_file(mock_json_load, mock_open, state_machine, caplog):
    """Ensure state machine can load from a file without error."""
    state_machine.load_from_file("test.json")
    mock_open.assert_called_once_with("test.json", "r")
    assert "[LOAD SUCCESS]" in caplog.text

# 8️⃣ Debugging Tests
def test_debug_execution_flow(state_machine, caplog):
    """Ensure execution flow logging is triggered correctly."""
    state_machine.debug_execution_flow()
    assert "[DEBUG FLOW COMPLETE]" in caplog.text

@pytest.mark.asyncio
async def test_run_with_queued_param(state_machine, caplog):
    """Test that run() processes a queued parameter correctly."""

    state_machine.add_param("param1", "state1")
    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])

    await state_machine.execution_queue.put("param1")  # Queue a parameter
    await state_machine.run()

    assert "[RUN COMPLETE]" in caplog.text, "[TEST FAILED] Expected successful execution log."
    assert state_machine.current_state == "state2", "[TEST FAILED] State transition did not occur correctly."

@pytest.mark.asyncio
async def test_run_when_already_running(state_machine, caplog):
    """Ensure run() does nothing if already running."""

    state_machine.running = True
    await state_machine.run()

    assert "[RUN] Already running. Exiting." in caplog.text, "[TEST FAILED] Expected log for already running state."

@pytest.mark.asyncio
async def test_run_with_empty_queue(state_machine, caplog):
    """Ensure run() completes gracefully when there are no queued updates."""

    await state_machine.run()

    # Updated expected log message to match actual output
    assert "[RUN COMPLETE] Execution cycle finished." in caplog.text, "[TEST FAILED] Expected completion log for empty queue."
    assert "[RUN END] Execution finished. Resetting state." in caplog.text, "[TEST FAILED] Expected execution reset log."

@pytest.mark.asyncio
async def test_run_handles_execution_failure(state_machine, mock_viz, caplog):
    """Ensure run() handles function execution failures correctly."""

    #  Simulate function failure
    mock_viz.test_function.side_effect = Exception("Simulated Execution Failure")
    state_machine.viz = mock_viz  #  Ensure the mock is set

    #  Register a transition with `test_function`
    state_machine.add_param("param1", "state1")
    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])

    await state_machine.execution_queue.put("param1")  #  Queue a parameter
    await state_machine.run()

    #  Ensure execution failure was logged
    error_log = "[CRITICAL] Execution error in function: test_function | Simulated Execution Failure"
    assert error_log in caplog.text, f"Expected log '{error_log}' but got: {caplog.text}"

@pytest.mark.asyncio
async def test_run_resets_running_state(state_machine, caplog):
    """Ensure run() resets the running state after execution."""

    state_machine.add_param("param1", "state1")
    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])

    await state_machine.execution_queue.put("param1")
    await state_machine.run()

    assert state_machine.running is False, "[TEST FAILED] Expected `running` to be reset after execution."
    assert state_machine.active_task is None, "[TEST FAILED] Expected `active_task` to be cleared."

# 1️⃣ Test: Handling of Undefined Parameter Transitions
@pytest.mark.asyncio
async def test_param_changed_with_undefined_transition(state_machine, caplog):
    """Ensure param_changed() handles parameters with no defined transitions."""
    await state_machine.param_changed("undefined_param")

    assert "[IGNORE] Unknown parameter" in caplog.text, "[TEST FAILED] Undefined parameters should be ignored."

@pytest.mark.asyncio
async def test_param_changed_functionless_transition(state_machine, caplog):
    """
    Ensures that a parameter can transition states even when no function sequence is provided.
    """

    #  Step 1: Register a parameter
    state_machine.add_param("param1", "state1")

    #  Step 2: Define a functionless state transition
    state_machine.define_param_transition("param1", "state1", "state2")

    #  Step 3: Verify state before transition
    assert state_machine.param_to_state["param1"] == "state1"

    #  Step 4: Trigger parameter change
    result = await state_machine.param_changed("param1")

    #  Step 5: Verify transition occurred
    assert result is True, "[TEST FAILED] Expected transition to succeed."
    assert state_machine.param_to_state["param1"] == "state2", "[TEST FAILED] State transition did not occur."

    #  Step 6: Check logs
    assert "[STATE TRANSITION] 'param1' moved from 'state1' → 'state2' without function execution." in caplog.text


@pytest.mark.asyncio
async def test_param_changed_no_transition_defined(state_machine, caplog):
    """
    Ensures that if no transition or function sequence exists, no changes occur.
    """

    #  Step 1: Register a parameter without transitions
    state_machine.add_param("param2", "state1")

    #  Step 2: Trigger parameter change
    result = await state_machine.param_changed("param2")

    #  Step 3: Verify nothing happened
    assert result is False, "[TEST FAILED] Expected no transition or queueing."
    assert state_machine.param_to_state["param2"] == "state1", "[TEST FAILED] State should remain unchanged."

    #  Step 4: Check logs
    assert "[NO EXECUTION] No function sequence or state transition for 'param2' in state 'state1'. Skipping." in caplog.text


@pytest.mark.asyncio
async def test_param_changed_with_execution_sequence(state_machine, caplog):
    """
    Ensures that parameters with execution sequences are queued correctly.
    """

    #  Step 1: Register a parameter
    state_machine.add_param("param3", "state1")

    #  Step 2: Define a function sequence transition
    state_machine.add_execution_chain("param3", "state1", ["mock_function"], end_state="state2")

    #  Step 3: Trigger parameter change
    result = await state_machine.param_changed("param3")

    #  Step 4: Verify the parameter was queued
    assert result is True, "[TEST FAILED] Expected parameter to be queued."
    assert not state_machine.execution_queue.empty(), "[TEST FAILED] Expected execution queue to contain 'param3'."

    #  Step 5: Check logs
    assert "[QUEUE] Parameter 'param3' added to execution queue." in caplog.text


@pytest.mark.asyncio
async def test_param_changed_invalid_param(state_machine, caplog):
    """
    Ensures that an invalid parameter does not trigger state transitions or execution.
    """

    #  Step 1: Trigger parameter change on a non-existent parameter
    result = await state_machine.param_changed("invalid_param")

    #  Step 2: Verify no transition occurred
    assert result is False, "[TEST FAILED] Invalid parameter should not trigger any changes."

    #  Step 3: Check logs
    assert "[INVALID PARAM] 'invalid_param' is not registered. Ignoring change." in caplog.text

# 2️⃣ Test: Empty Function Sequence Execution
@pytest.mark.asyncio
async def test_execute_function_sequence_with_empty_list(state_machine, caplog):
    """Ensure empty function sequences are handled gracefully."""
    await state_machine._execute_function_sequence([])

    assert "[COMPLETE] Function sequence execution finished. Executed: []" in caplog.text, "[TEST FAILED] Expected empty execution log."

# 3️⃣ Test: Handling of Non-callable Function Names
from unittest.mock import MagicMock

@pytest.mark.asyncio
async def test_execute_function_sequence_with_non_callable(state_machine, caplog):
    """Ensure that non-callable function names in a sequence are ignored."""

    state_machine.viz.non_callable_func = "this is a string, not a function..."

    await state_machine._execute_function_sequence(["non_callable_func"])

    expected_log = "[WARNING] [INVALID] Function 'non_callable_func' is not callable. Skipping."
    assert expected_log in caplog.text, f"[TEST FAILED] Expected non-callable function warning, but got:\n{caplog.text}"


# 4️⃣ Test: Handling of Missing Function in Visualization Module
@pytest.mark.asyncio
async def test_execute_function_sequence_with_missing_function(state_machine, caplog):
    """Ensure that missing functions trigger an error log and are not executed."""

    # Ensure missing_function is NOT in the state_machine
    if hasattr(state_machine, "missing_function"):
        delattr(state_machine, "missing_function")
    if hasattr(state_machine.viz, "missing_function"):
        delattr(state_machine.viz, "missing_function")

    # Execute function sequence with missing function
    await state_machine._execute_function_sequence(["missing_function"])

    # Verify the correct log message
    expected_log = "[MISSING] Function 'missing_function' not found. Skipping."

    assert expected_log in caplog.text, f"[TEST FAILED] Expected missing function warning, but got:\n{caplog.text}"

# 5️⃣ Test: Running Execution with Multiple Parameters
@pytest.mark.asyncio
async def test_run_with_multiple_queued_params(state_machine, caplog):
    """Ensure multiple queued parameters are processed correctly."""
    state_machine.add_param("param1", "state1")
    state_machine.add_param("param2", "state1")
    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])
    state_machine.define_param_transition("param2", "state1", "state2", ["test_function"])

    await state_machine.execution_queue.put("param1")
    await state_machine.execution_queue.put("param2")
    await state_machine.run()

    assert "[RUN COMPLETE]" in caplog.text, "[TEST FAILED] Expected successful run with multiple parameters."
    assert state_machine.param_to_state["param1"] == "state2", "[TEST FAILED] Expected param1 to transition to state2."
    assert state_machine.param_to_state["param2"] == "state2", "[TEST FAILED] Expected param2 to transition to state2."


@patch("json.load", side_effect=FileNotFoundError("File 'test.json' not found."))
def test_load_from_corrupt_file(mock_json_load, state_machine, caplog):
    """Ensure loading a corrupt file fails gracefully."""
    state_machine.load_from_file("test.json")

    assert "[LOAD ERROR] File 'test.json' not found." in caplog.text, "[TEST FAILED] Expected file load failure log."

@pytest.mark.asyncio
async def test_handle_execution_error(state_machine, caplog):
    """Comprehensive test for handling execution errors with and without recovery sequences."""

    # Mocking the logging
    caplog.set_level("DEBUG")

    # Case 1: Basic Error Logging
    await state_machine._handle_execution_error(Exception("Test Error"), "test_function")

    # Updated to match actual log format
    assert "[ERROR] Executing recovery function: test_function: Test Error" in caplog.text
    assert "Exception: Test Error" in caplog.text  # Ensure traceback is captured
    assert "[TRACEBACK] Full error details:" in caplog.text  # Ensure traceback marker exists

    # Reset logs
    caplog.clear()

    # Case 2: Execution Error without Function Name
    await state_machine._handle_execution_error(Exception("Unhandled Test Error"))

    # Updated assertion to match actual log format
    assert "[ERROR] Executing recovery function: Unhandled execution error: Unhandled Test Error" in caplog.text
    assert "[TRACEBACK] Full error details:" in caplog.text

    # Reset logs
    caplog.clear()

    # Case 3: Error Handling with a Recovery Sequence
    state_machine.viz = Mock()
    state_machine.viz.recover_func = Mock()
    state_machine.recovery_chain = ["recover_func"]

    await state_machine._handle_execution_error(Exception("Test Error"))

    # Updated to match actual log format
    assert "[INFO] [RECOVERY] Initiating recovery sequence." in caplog.text
    assert "[INFO] [RECOVERY] Executing 'recover_func'" in caplog.text
    assert "[INFO] [RECOVERY COMPLETE] Recovery process finished." in caplog.text

    # Ensure the recovery function was actually called
    state_machine.viz.recover_func.assert_called_once()

    # Reset logs
    caplog.clear()

    # Case 4: Recovery Function Fails
    state_machine.viz.recover_func.side_effect = Exception("Recovery Failed")

    await state_machine._handle_execution_error(Exception("Test Error"))

    # Updated assertion to match actual log output
    assert "[INFO] [RECOVERY] Initiating recovery sequence." in caplog.text
    assert "[CRITICAL] [RECOVERY ERROR] Failed 'recover_func': Recovery Failed" in caplog.text

    # Reset logs
    caplog.clear()

    # Case 5: No Recovery Sequence Defined
    state_machine.recovery_chain = []

    await state_machine._handle_execution_error(Exception("Test Error"))

    # Updated assertion
    assert "[INFO] [RECOVERY] No recovery sequence defined. Execution will not be retried." in caplog.text


def test_add_execution_chain_valid(state_machine):
    """Test adding a valid execution chain."""

    state_machine.add_execution_chain("param1", "stateA", ["funcA", "funcB"], priority=2)

    assert ("param1", "stateA") in state_machine.state_sequences
    assert state_machine.state_sequences[("param1", "stateA")]["functions"] == ["funcA", "funcB"]
    assert state_machine.state_sequences[("param1", "stateA")]["priority"] == 2

    # Check if the graph contains correct edges
    assert state_machine.execution_graph.has_edge("funcA", "funcB")

def test_add_execution_chain_overwrite(state_machine, caplog):
    """Test that adding a new execution chain overwrites the previous one."""

    # First execution chain
    state_machine.add_execution_chain("param1", "stateA", ["funcA"])

    # Overwrite with a new function sequence
    state_machine.add_execution_chain("param1", "stateA", ["funcB", "funcC"])

    # Verify that the function sequence has been updated
    assert state_machine.state_sequences[("param1", "stateA")]["functions"] == ["funcB", "funcC"]
    assert "[WARNING] Execution chain for (param='param1', state='stateA') already exists. Overwriting." in caplog.text

def test_add_execution_chain_empty_sequence(state_machine, caplog):
    """Test adding an execution chain with an empty function sequence."""

    state_machine.add_execution_chain("param1", "stateA", [])

    assert ("param1", "stateA") in state_machine.state_sequences
    assert state_machine.state_sequences[("param1", "stateA")]["functions"] == []
    assert state_machine.state_sequences[("param1", "stateA")]["priority"] == 0

    # Ensure the execution graph has no edges for an empty sequence
    assert len(state_machine.execution_graph.edges()) == 0

    assert "[SUCCESS] Execution chain for (param='param1', state='stateA') registered: []" in caplog.text

def test_add_execution_chain_further_invalid_inputs(state_machine, caplog):
    """Test handling of invalid inputs."""
    # Test None as state
    state_machine.add_execution_chain(None, ["funcA", "funcB"])
    assert None not in state_machine.state_sequences
    assert "State 'None' already has an execution chain." not in caplog.text  # No overwrite warning should appear

    # Test None as function sequence
    state_machine.add_execution_chain("state1", None)
    assert "state1" not in state_machine.state_sequences  # Should not be added

    # Test non-list function sequence
    state_machine.add_execution_chain("state1", "not_a_list")
    assert "state1" not in state_machine.state_sequences  # Should not be added

def test_execution_graph_updates_correctly(state_machine):
    """Test if the execution graph correctly registers function dependencies."""
    state_machine.add_execution_chain("state1", ["funcX", "funcY", "funcZ"])

    assert ("funcX", "funcY") in state_machine.execution_graph.edges
    assert ("funcY", "funcZ") in state_machine.execution_graph.edges

    # Ensure node count is correct
    assert len(state_machine.execution_graph.nodes) == 3
    assert len(state_machine.execution_graph.edges) == 2

def test_add_execution_chain_multiple_states(state_machine):
    """Test adding execution chains for multiple states."""

    state_machine.add_execution_chain("param1", "stateA", ["funcA", "funcB"])
    state_machine.add_execution_chain("param2", "stateB", ["funcC", "funcD"])

    assert ("param1", "stateA") in state_machine.state_sequences
    assert ("param2", "stateB") in state_machine.state_sequences

    assert ("funcA", "funcB") in state_machine.execution_graph.edges
    assert ("funcC", "funcD") in state_machine.execution_graph.edges

def test_add_execution_chain_no_duplicate_edges(state_machine):
    """Ensure duplicate execution chains do not create redundant graph edges."""

    state_machine.add_execution_chain("param1", "stateA", ["funcA", "funcB"])
    state_machine.add_execution_chain("param1", "stateA", ["funcA", "funcB"])  # Adding same chain twice

    # Ensure edges exist but no duplicates are added
    assert ("funcA", "funcB") in state_machine.execution_graph.edges
    assert len(state_machine.execution_graph.edges) == 1

def test_add_execution_chain_different_states_same_param(state_machine):
    """Test that different function chains can be assigned to the same param in different states."""
    state_machine.add_execution_chain("param1", "stateA", ["funcA", "funcB"], priority=1)
    state_machine.add_execution_chain("param1", "stateB", ["funcC", "funcD"], priority=2)

    assert ("param1", "stateA") in state_machine.state_sequences
    assert ("param1", "stateB") in state_machine.state_sequences
    assert state_machine.state_sequences[("param1", "stateA")]["functions"] == ["funcA", "funcB"]
    assert state_machine.state_sequences[("param1", "stateB")]["functions"] == ["funcC", "funcD"]

def test_add_execution_chain_different_params_same_state(state_machine):
    """Test that different parameters can have different execution chains even if they start in the same state."""
    state_machine.add_execution_chain("param1", "stateX", ["funcA"])
    state_machine.add_execution_chain("param2", "stateX", ["funcB"])

    assert ("param1", "stateX") in state_machine.state_sequences
    assert ("param2", "stateX") in state_machine.state_sequences
    assert state_machine.state_sequences[("param1", "stateX")]["functions"] == ["funcA"]
    assert state_machine.state_sequences[("param2", "stateX")]["functions"] == ["funcB"]


@pytest.mark.asyncio
async def test_add_execution_chain_registers_missing_states(state_machine, caplog):
    """
    Ensures that missing start and end states are automatically registered
    when adding an execution chain.
    """
    param = "param1"
    start_state = "missing_start_state"
    end_state = "missing_end_state"
    function_sequence = ["func_a", "func_b"]

    assert start_state not in state_machine.state_sequences
    assert end_state not in state_machine.state_sequences

    # Add execution chain with missing states
    result = state_machine.add_execution_chain(param, start_state, function_sequence, end_state=end_state)

    assert result is True, "[TEST FAILED] Expected execution chain registration to succeed."
    assert start_state in state_machine.state_sequences, "[TEST FAILED] Start state was not auto-registered."
    assert end_state in state_machine.state_sequences, "[TEST FAILED] End state was not auto-registered."

    # Check logs
    assert "[AUTO-REGISTER] Start state 'missing_start_state' was missing. Adding it to state sequences." in caplog.text
    assert "[AUTO-REGISTER] End state 'missing_end_state' was missing. Adding it to state sequences." in caplog.text


@pytest.mark.asyncio
async def test_add_execution_chain_rejects_invalid_function_sequences(state_machine, caplog):
    """
    Ensures that the system rejects execution chains with invalid function sequences.
    """
    param = "param1"
    start_state = "state1"
    function_sequence = ["func_a", 123, None]  # Invalid function names

    # Ensure adding an invalid sequence fails
    result = state_machine.add_execution_chain(param, start_state, function_sequence)

    assert result is False, "[TEST FAILED] Expected execution chain registration to fail due to invalid function names."
    assert "[INVALID SEQUENCE] Execution chain for 'param1' must be a list of function names." in caplog.text


@pytest.mark.asyncio
async def test_add_execution_chain_overwrites_existing_chain(state_machine, caplog):
    """
    Ensures that if an execution chain already exists for a (param, state) pair,
    a warning is logged and the chain is overwritten.
    """
    param = "param1"
    start_state = "state1"
    function_sequence_1 = ["func_a", "func_b"]
    function_sequence_2 = ["func_x", "func_y"]  # New chain to overwrite

    # Add the first execution chain
    state_machine.add_execution_chain(param, start_state, function_sequence_1)

    # Add a second execution chain for the same (param, start_state)
    state_machine.add_execution_chain(param, start_state, function_sequence_2)

    # Ensure the second chain overwrote the first
    assert state_machine.state_sequences[(param, start_state)]["functions"] == function_sequence_2, \
        "[TEST FAILED] Execution chain was not correctly overwritten."

    assert f"Execution chain for (param='{param}', state='{start_state}') already exists. Overwriting." in caplog.text


@pytest.mark.asyncio
async def test_add_execution_chain_updates_execution_graph(state_machine):
    """
    Ensures that adding an execution chain correctly updates the execution graph.
    """
    param = "param1"
    start_state = "state1"
    function_sequence = ["func_a", "func_b", "func_c"]

    # Ensure graph starts empty
    assert len(state_machine.execution_graph.nodes()) == 0
    assert len(state_machine.execution_graph.edges()) == 0

    # Add execution chain
    state_machine.add_execution_chain(param, start_state, function_sequence)

    # Verify graph updates
    assert len(state_machine.execution_graph.nodes()) > 0, "[TEST FAILED] Execution graph nodes were not updated."
    assert ("func_a", "func_b") in state_machine.execution_graph.edges(), "[TEST FAILED] Missing expected edge (func_a → func_b)."
    assert ("func_b", "func_c") in state_machine.execution_graph.edges(), "[TEST FAILED] Missing expected edge (func_b → func_c)."

def test_execution_graph_updates_two_chains_correctly(state_machine):
    """Test if execution graph updates correctly for multiple (param, state) pairs."""
    state_machine.add_execution_chain("param1", "stateA", ["funcX", "funcY"])
    state_machine.add_execution_chain("param2", "stateB", ["funcZ", "funcW"])

    assert ("funcX", "funcY") in state_machine.execution_graph.edges
    assert ("funcZ", "funcW") in state_machine.execution_graph.edges

def test_add_execution_chain_invalid_inputs(state_machine, caplog):
    """Test handling of invalid inputs (None, empty values)."""

    # Test None as param
    state_machine.add_execution_chain(None, "stateA", ["funcA", "funcB"])
    assert (None, "stateA") not in state_machine.state_sequences
    assert "[ERROR] Invalid parameter or state: param='None', state='stateA'" in caplog.text

    # Test None as state
    state_machine.add_execution_chain("param1", None, ["funcA", "funcB"])
    assert ("param1", None) not in state_machine.state_sequences
    assert "[ERROR] Invalid parameter or state: param='param1', state='None'" in caplog.text

    # Test empty function sequence
    state_machine.add_execution_chain("param1", "stateA", [])
    assert state_machine.state_sequences[("param1", "stateA")]["functions"] == []

@pytest.mark.asyncio
async def test_run_with_param_state_function_sequence(state_machine, caplog):
    """Test that the correct function sequence executes based on (param, state) pair."""

    # Mock function execution
    state_machine.viz.funcA = MagicMock()
    state_machine.viz.funcB = MagicMock()
    state_machine.add_execution_chain("param1", "stateA", ["funcA", "funcB"])

    # Simulate a param change
    state_machine.param_to_state["param1"] = "stateA"
    await state_machine.execution_queue.put("param1")  #  Corrected: use `await`

    # Run execution properly
    await state_machine.run()  #  Corrected: use `await`

    # Verify that the mock functions were called
    state_machine.viz.funcA.assert_called_once()
    state_machine.viz.funcB.assert_called_once()

    # Ensure logs confirm execution
    assert "[COMPLETE] Function sequence execution finished. Executed: ['funcA', 'funcB']" in caplog.text

@pytest.mark.asyncio
async def test_run_handles_execution_error(state_machine, caplog):
    """Test that run() correctly handles exceptions during execution."""

    # Mock `_execute_function_sequence` to raise an exception
    state_machine._execute_function_sequence = AsyncMock(side_effect=Exception("Simulated failure"))
    state_machine._handle_execution_error = AsyncMock()

    # Setup a parameter and transition
    state_machine.add_param("param1", "state1")
    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])

    # Queue the parameter
    await state_machine.execution_queue.put("param1")

    # Run state machine (which should hit the exception)
    await state_machine.run()

    # Verify that the correct error message was logged
    assert "Function sequence failed for 'param1'. Triggering recovery." in caplog.text
    assert "[RECOVERY] Recovery initiated for 'param1'." in caplog.text

    # Ensure `_handle_execution_error` was called
    state_machine._handle_execution_error.assert_called_once_with("param1")

@pytest.mark.asyncio
async def test_run_handles_function_sequence_failure(state_machine, caplog):
    """
    Test that run() correctly handles failures in function sequence execution
    and triggers the appropriate recovery mechanism.
    """

    # Mock `_execute_function_sequence` to raise an exception
    state_machine._execute_function_sequence = AsyncMock(side_effect=Exception("Simulated function failure"))
    state_machine._handle_execution_error = AsyncMock()

    # Setup a parameter and transition
    state_machine.add_param("param1", "state1")
    state_machine.define_param_transition("param1", "state1", "state2", ["test_function"])

    # Queue the parameter for execution
    await state_machine.execution_queue.put("param1")

    # Run state machine (which should trigger failure)
    await state_machine.run()

    # Verify that the correct failure message is logged
    assert "Function sequence failed for 'param1'. Aborting execution." in caplog.text, \
        "[TEST FAILED] Expected failure message not found in logs."

    # Verify that the recovery process was triggered
    assert "[RECOVERY] Recovery process completed. Execution halted." in caplog.text, \
        "[TEST FAILED] Recovery process did not execute as expected."

    # Ensure `_handle_execution_error` was actually called
    state_machine._handle_execution_error.assert_called_once()

@pytest.mark.asyncio
async def test_handle_execution_error_function_not_callable(state_machine, caplog):
    """Test that _handle_execution_error() logs a warning when the recovery function is not callable."""

    state_machine.recovery_chain = ["recovery_func"]

    state_machine.viz = MagicMock()
    state_machine.viz.recovery_func = "I am not callable"  # Non-callable value

    # Run the error handler
    await state_machine._handle_execution_error("recovery_func")

    assert "[WARNING] Recovery function 'recovery_func' not found or is not callable in visualization module. Skipping." in caplog.text

@pytest.mark.asyncio
async def test_handle_execution_error_function_not_found(state_machine, caplog):
    """Test that _handle_execution_error() logs a warning when the recovery function does not exist."""

    state_machine.recovery_chain = ["recovery_func"]
    state_machine.viz = MagicMock()
    del state_machine.viz.recovery_func  # Ensure attribute does not exist

    # Run the error handler
    await state_machine._handle_execution_error("recovery_func")

    assert "[WARNING] Recovery function 'recovery_func' not found or is not callable in visualization module. Skipping." in caplog.text

@pytest.mark.asyncio
async def test_handle_execution_error_logs_and_invokes_recovery(state_machine, caplog):
    """
    Tests that the _handle_execution_error function correctly logs the error,
    executes the recovery functions, and does not retry the failed function.
    """

    #  Simulated error to be handled
    test_error = Exception("Simulated Function Execution Error")

    #  Mock recovery functions
    async def mock_recovery_success():
        return "[MOCK RECOVERY] Executed Successfully"

    async def mock_recovery_fail():
        raise Exception("[MOCK RECOVERY] Simulated Failure")

    #  Add mock recovery functions to the state machine
    setattr(state_machine, "mock_recovery_success", mock_recovery_success)
    setattr(state_machine, "mock_recovery_fail", mock_recovery_fail)

    #  Set the recovery chain
    state_machine.recovery_chain = ["mock_recovery_success", "mock_recovery_fail"]

    #  Invoke error handling
    await state_machine._handle_execution_error(test_error, function_name="failing_function")

    #  Assertions: Logs the error
    assert "Executing recovery function: failing_function" in caplog.text
    assert "Simulated Function Execution Error" in caplog.text
    assert "[TRACEBACK] Full error details:" in caplog.text

    #  Assertions: Recovery functions executed
    assert "[RECOVERY] Executing 'mock_recovery_success'" in caplog.text
    assert "[MOCK RECOVERY] Executed Successfully" in caplog.text

    #  Assertions: Recovery failure is logged but does not retry
    assert "[RECOVERY] Executing 'mock_recovery_fail'" in caplog.text
    assert "[RECOVERY ERROR] Failed 'mock_recovery_fail': [MOCK RECOVERY] Simulated Failure" in caplog.text

    #  Assertions: Final confirmation log
    assert "[RECOVERY COMPLETE] Recovery process finished." in caplog.text


@pytest.mark.asyncio
async def test_handle_execution_error_no_recovery_defined(state_machine, caplog):
    """
    Ensures that if no recovery functions are defined, the system logs the error and exits cleanly.
    """

    #  Simulated execution failure
    test_error = Exception("Simulated Function Execution Error")

    #  Ensure recovery chain is empty
    state_machine.recovery_chain = []

    #  Invoke error handling
    await state_machine._handle_execution_error(test_error, function_name="failing_function")

    #  Assertions: Error is logged
    assert "Executing recovery function: failing_function" in caplog.text
    assert "Simulated Function Execution Error" in caplog.text
    assert "[TRACEBACK] Full error details:" in caplog.text

    #  Assertions: Logs that no recovery is available
    assert "[RECOVERY] No recovery sequence defined. Execution will not be retried." in caplog.text


@pytest.mark.asyncio
async def test_handle_execution_error_invalid_recovery_function(state_machine, caplog):
    """
    Ensures that invalid recovery function names do not cause crashes, but are logged and skipped.
    """

    #  Simulated execution failure
    test_error = Exception("Simulated Execution Failure")

    #  Add invalid recovery function name
    state_machine.recovery_chain = ["non_existent_function"]

    #  Invoke error handling
    await state_machine._handle_execution_error(test_error, function_name="failing_function")

    #  Assertions: Logs missing recovery function
    assert "[RECOVERY] Executing 'non_existent_function'" in caplog.text
    assert "Recovery function 'non_existent_function' not found or is not callable in visualization module. Skipping." in caplog.text

    #  Assertions: No retries or crash
    assert "[RECOVERY COMPLETE] Recovery process finished." in caplog.text


@pytest.mark.asyncio
async def test_param_changed_avoids_duplicate_queue(state_machine, caplog):
    """Test that param_changed() does not enqueue a parameter if it's already in the execution queue."""

    param_name = "test_param"

    #  Register the parameter first
    state_machine.add_param(param_name, "initial_state")

    #  Ensure execution_queue is an asyncio.Queue and add the parameter
    await state_machine.execution_queue.put(param_name)  # Pre-add to queue

    #  Call param_changed() properly
    await state_machine.param_changed(param_name)

    #  Ensure param_name is still only in the queue once
    queue_contents = list(state_machine.execution_queue._queue)  # Direct queue access
    assert queue_contents.count(param_name) == 1, "[TEST FAILED] param_name was added to queue again."

    #  Ensure the correct log message appears
    assert f"[SKIP] '{param_name}' is already in the queue. Avoiding duplicate execution." in caplog.text


@pytest.mark.parametrize(
    "input_sequence, expected_warning, expected_error",
    [
        #  Test: Normal case - valid recovery sequence
        (["funcA", "funcB"], False, False),

        #  Test: Invalid case - recovery sequence is not a list
        ("not_a_list", False, True),

        #  Test: Overwriting an existing recovery chain
        (["new_func"], True, False),
    ],
)
def test_set_recovery_chain(state_machine, caplog, input_sequence, expected_warning, expected_error):
    """Test set_recovery_chain() for all execution paths."""

    # 🛠 Step 1: Set an initial recovery chain to test overwriting
    if expected_warning:
        state_machine.set_recovery_chain(["old_func"])

    # 🛠 Step 2: Call the method with test input
    state_machine.set_recovery_chain(input_sequence)

    #  Ensure the setup message is logged
    assert "[RECOVERY SETUP] Defining recovery chain:" in caplog.text

    #  Ensure error logging if input is not a list
    if expected_error:
        assert "Recovery sequence must be a list of function names." in caplog.text
        return  #  Skip further checks for this case

    #  Ensure warning is logged if overwriting
    if expected_warning:
        assert "Overwriting existing recovery chain." in caplog.text

    #  Ensure each function in the recovery sequence is logged
    if isinstance(input_sequence, list):
        for func in input_sequence:
            assert f"[RECOVERY FUNCTION] Added '{func}' to recovery chain." in caplog.text

    #  Ensure final confirmation message
    assert "[RECOVERY SET] Recovery sequence successfully defined." in caplog.text

import pytest
import logging

@pytest.mark.parametrize(
    "input_chain,expected_chain,force,expected_result",
    [
        (["valid_func1", "valid_func2"], ["valid_func1", "valid_func2"], True, True),  # ✅ Valid functions
        (["invalid_func"], [], True, False),  # ❌ Invalid function should fail
        (["valid_func1", "invalid_func"], ["valid_func1"], True, True),  # ✅ Partial valid should succeed
        ([], [], True, False),  # ❌ Empty list should fail
        (["valid_func1"], ["valid_func1"], False, True),  # ✅ Valid with force=False
    ]
)
def test_set_recovery_chain_valid_and_invalid(state_machine, caplog, input_chain, expected_chain, force, expected_result):
    """Tests valid and invalid recovery function registrations."""
    
    # Mock callable functions
    setattr(state_machine, "valid_func1", lambda: None)
    setattr(state_machine, "valid_func2", lambda: None)

    caplog.clear()

    result = state_machine.set_recovery_chain(input_chain, force=force)

    assert result == expected_result, "[TEST FAILED] Function did not return the expected result."
    
    if expected_result:
        assert state_machine.recovery_chain == expected_chain, "[TEST FAILED] Recovery chain mismatch."
    else:
        assert "No valid recovery functions found" in caplog.text, "[TEST FAILED] Missing error log for invalid functions."


def test_set_recovery_chain_prevents_overwrite(state_machine, caplog):
    """Tests that the function prevents overwriting recovery chain when `force=False`."""
    
    # Set initial recovery chain
    state_machine.recovery_chain = ["valid_func1"]
    setattr(state_machine, "valid_func1", lambda: None)

    caplog.clear()

    # Attempt overwrite with `force=False`
    result = state_machine.set_recovery_chain(["valid_func2"], force=False)

    assert result is False, "[TEST FAILED] Expected overwrite prevention."
    assert "Overwrite prevented" in caplog.text, "[TEST FAILED] No log for overwrite prevention."
    assert state_machine.recovery_chain == ["valid_func1"], "[TEST FAILED] Recovery chain should not have changed."


def test_set_recovery_chain_logs_errors(state_machine, caplog):
    """Tests error logging when an invalid function is provided."""
    
    caplog.clear()

    result = state_machine.set_recovery_chain(["invalid_func"])

    assert result is False, "[TEST FAILED] Function should fail with an invalid function."
    assert "Recovery function 'invalid_func' not found" in caplog.text, "[TEST FAILED] Error log missing for invalid function."


def test_set_recovery_chain_successfully_overwrites(state_machine, caplog):
    """Tests that `set_recovery_chain` properly overwrites when `force=True`."""
    
    state_machine.recovery_chain = ["valid_func1"]
    setattr(state_machine, "valid_func2", lambda: None)

    caplog.clear()

    result = state_machine.set_recovery_chain(["valid_func2"], force=True)

    assert result is True, "[TEST FAILED] Overwrite should have succeeded."
    assert state_machine.recovery_chain == ["valid_func2"], "[TEST FAILED] Recovery chain was not updated."
    assert "Overwriting existing recovery chain" in caplog.text, "[TEST FAILED] No overwrite warning logged."


@pytest.mark.asyncio
async def test_param_changed_execution_already_running(state_machine, caplog):
    """Test that param_changed logs an update message when execution is already in progress."""

    param_name = "test_param"

    # Mock the state machine with a valid parameter
    state_machine.param_to_state[param_name] = "state1"

    # Simulate execution already in progress
    state_machine.running = True  #  Simulates that the execution cycle is already running

    # Call param_changed while execution is in progress
    await state_machine.param_changed(param_name)

    #  Ensure param_name was added to the queue
    assert param_name in state_machine.execution_queue._queue

    #  Check that the expected log message was generated
    assert "[RUN] Execution already in progress, queued update." in caplog.text

@pytest.mark.asyncio
async def test_param_changed_direct_state_transition(state_machine, caplog):
    """Test if `param_changed` performs a direct state transition when no function execution is required."""

    #  Register parameter and state
    state_machine.add_param("param1", "state1")

    #  Define a state transition (but no function execution)
    state_machine.define_param_transition("param1", "state1", "state2")

    #  Call `param_changed` and check if transition occurs
    result = await state_machine.param_changed("param1")

    assert result is True, "[TEST FAILED] Direct state transition did not return True."
    assert state_machine.param_to_state["param1"] == "state2", "[TEST FAILED] Parameter did not transition correctly."
    assert "[STATE TRANSITION] 'param1' moved from 'state1' → 'state2'" in caplog.text, "[TEST FAILED] Expected transition log message missing."

@pytest.mark.asyncio
async def test_param_changed_execution_queueing(state_machine, caplog):
    """Test if `param_changed` correctly queues the parameter when an execution chain exists."""

    #  Register parameter and state
    state_machine.add_param("param1", "state1")

    #  Add an execution chain
    state_machine.add_execution_chain("param1", "state1", ["test_function"])

    #  Call `param_changed`
    result = await state_machine.param_changed("param1")

    assert result is True, "[TEST FAILED] Expected function to return True when queueing execution."
    assert not state_machine.execution_queue.empty(), "[TEST FAILED] Execution queue should contain queued parameter."
    assert "[QUEUE] Parameter 'param1' added to execution queue." in caplog.text, "[TEST FAILED] Expected queue log message missing."

@pytest.mark.asyncio
async def test_param_changed_no_transition_no_execution(state_machine, caplog):
    """Test if `param_changed` correctly handles cases where no execution chain or transition exists."""

    # 🔹 Register parameter but don't define transitions or executions
    state_machine.add_param("param1", "state1")

    #  Call `param_changed`
    result = await state_machine.param_changed("param1")

    assert result is False, "[TEST FAILED] Expected function to return False when no execution or transition exists."
    assert "[NO EXECUTION] No function sequence or state transition for 'param1' in state 'state1'." in caplog.text, "[TEST FAILED] Expected missing transition log message."

@pytest.mark.asyncio
async def test_param_changed_queue_overflow(state_machine, caplog):
    """Test if `param_changed` correctly handles queue overflow errors."""

    #  Register parameter and state
    state_machine.add_param("param1", "state1")

    #  Add an execution chain
    state_machine.add_execution_chain("param1", "state1", ["test_function"])

    #  Fill the execution queue to capacity
    for _ in range(state_machine.execution_queue.maxsize):
        await state_machine.execution_queue.put("dummy_param")

    #  Attempt to queue another parameter
    result = await state_machine.param_changed("param1")

    assert result is False, "[TEST FAILED] Expected function to return False when queue is full."
    assert "[QUEUE FULL] Cannot queue 'param1'. Execution queue is at capacity." in caplog.text, "[TEST FAILED] Expected queue full log message missing."


@pytest.mark.asyncio
async def test_run_handles_execution_error(state_machine, caplog):
    """Test that run() logs an execution error and initiates recovery when an exception occurs."""

    #  Ensure the parameter exists
    state_machine.add_param("test_param", "state1")

    #  Define a transition that will fail
    state_machine.define_param_transition("test_param", "state1", "state2", ["test_func"])

    #  Mock `_execute_function_sequence()` to simulate failure
    async def failing_func(_):
        raise Exception("Simulated failure")

    state_machine._execute_function_sequence = AsyncMock(side_effect=failing_func)

    #  Mock `_handle_execution_error` to prevent real recovery
    state_machine._handle_execution_error = AsyncMock()

    #  Enqueue the parameter to trigger execution
    await state_machine.execution_queue.put("test_param")

    #  Run the state machine
    await state_machine.run()

    #  Ensure the expected critical error log is captured
    assert "Execution error: Simulated failure" in caplog.text, \
        "Expected execution error log message not found!"

    #  Ensure that `_handle_execution_error()` was triggered with the correct parameter
    state_machine._handle_execution_error.assert_awaited_with("test_param")

    #  Ensure the recovery process is logged
    assert "[RECOVERY] Recovery initiated for 'test_param'" in caplog.text, \
        "Expected recovery initiation log not found!"

    #  Ensure state is reset after failure
    assert state_machine.running is False
    assert state_machine.active_task is None

def setup_state_machine(state_machine):
    """Helper to add parameters and transitions for testing."""
    state_machine.state_transitions = {
        ("param1", "state1"): "state2",
        ("param2", "state2"): "state3",
    }

    #  Ensure a fresh copy is used every time
    state_machine.state_sequences = {
        "state1": {"functions": ["func1", "func2"]},
        "state2": {"functions": ["func3"]},
        "state3": {"functions": []},  # Valid empty sequence
    }.copy()

@pytest.mark.parametrize("modify_machine, expected_result, expected_log", [
    #  Case 1: All transitions valid
    (lambda sm: setup_state_machine(sm), True, "[VALIDATION SUCCESS] All transitions and function sequences are valid."),

    #  Case 2: Undefined end state
    (lambda sm: sm.state_transitions.update({("param3", "stateX"): "undefined_state"}),
     False, "[INVALID TRANSITION] 'param3': stateX → undefined_state (End state undefined)."),

    #  Case 3: Execution sequence is not a list
    (lambda sm: sm.state_sequences.update({"state1": {"functions": "not a list"}}),
     False, "[WARNING] [INVALID SEQUENCE] Execution sequence for state 'state1' is not a list."),

    #  Case 4: Execution sequence contains non-string function
    (lambda sm: sm.state_sequences["state1"]["functions"].append(123),
     False, "[WARNING] [INVALID FUNCTION] Function '123' in state 'state1' should be a string."),
])
def test_validate_transitions(state_machine, caplog, modify_machine, expected_result, expected_log):
    """Test validate_transitions() under different scenarios."""

    #  Set up valid transitions first
    setup_state_machine(state_machine)

    # 🛠️ Modify machine for specific test case
    modify_machine(state_machine)

    #  Run validation
    result = state_machine.validate_transitions()

    #  Check the expected result
    assert result == expected_result, f"Expected {expected_result} but got {result}"

    #  Ensure the expected log message is captured
    assert expected_log in caplog.text, f"Expected log not found: {expected_log}"

@pytest.mark.asyncio
async def test_load_from_file_json_decode_error(state_machine, caplog):
    """Test load_from_file() handles JSONDecodeError correctly."""

    file_path = "invalid.json"

    # 🛠️ Mock open() to simulate invalid JSON
    mock_invalid_json = mock_open(read_data="{ invalid_json: }")  # Improper JSON format

    with patch("builtins.open", mock_invalid_json):
        with patch("json.load", side_effect=json.JSONDecodeError("Expecting value", "doc", 0)):
            state_machine.load_from_file(file_path)

    #  Check if error log was triggered
    assert f"[LOAD ERROR] Failed to parse JSON from '{file_path}'" in caplog.text, \
        "Expected JSONDecodeError log not found!"

@pytest.mark.parametrize("param_name, start_state, end_state, function_sequence, priority, expected_success, expected_logs", [
    #  Valid transition with function sequence
    ("param1", "state1", "state2", ["func_a", "func_b"], 1, True, 
     ["[TRANSITION] Defining transition", "[TRANSITION REGISTERED] 'param1' (state1 → state2) via ['func_a', 'func_b']"]),

    #  Valid transition with empty function sequence
    ("param2", "state1", "state3", [], 0, True, 
     ["[TRANSITION] Defining transition", "[TRANSITION REGISTERED] 'param2' (state1 → state3) via []"]),

    #  Auto-register missing states
    ("param3", "new_state", "unknown_state", ["func_x"], 2, True, 
     ["[AUTO-REGISTER] Start state 'new_state' was missing", "[AUTO-REGISTER] End state 'unknown_state' was missing"]),

    #  Invalid: param_name not a string
    (None, "state1", "state2", ["func_a"], 1, False, ["[INVALID] Transition definition failed"]),

    #  Invalid: start_state not a string
    ("param4", None, "state2", ["func_b"], 1, False, ["[INVALID] Transition definition failed"]),

    #  Invalid function sequence (not a list)
    ("param5", "state1", "state2", "not_a_list", 1, False, ["[INVALID SEQUENCE] Function sequence must be a list"]),

    #  Invalid function sequence (contains non-string elements)
    ("param6", "state1", "state2", [123, "func_b"], 1, False, ["[INVALID SEQUENCE] Function sequence must be a list"]),

    #  Overwriting an existing transition (warning expected)
    ("param7", "state1", "state2", ["func_a"], 1, True, ["[DUPLICATE] Transition for 'param7' from 'state1' already exists"])
])
def test_define_param_transition(state_machine, param_name, start_state, end_state, function_sequence, priority, expected_success, expected_logs, caplog):
    """
    Tests the define_param_transition method to ensure correct behavior for valid and invalid inputs.
    """
    caplog.clear()

    # 🏗 Execute the function
    success = state_machine.define_param_transition(param_name, start_state, end_state, function_sequence, priority)

    #  Check if the function returns the expected success value
    assert success == expected_success, f"[TEST FAILED] Expected {expected_success} but got {success}"

    #  Validate logs for key events
    for log in expected_logs:
        assert any(log in record.message for record in caplog.records), f"[TEST FAILED] Missing expected log: {log}"

def test_define_param_transition_defaults(state_machine):
    """Test define_param_transition() sets default values for None inputs."""

    param_name = "test_param"
    start_state = "state1"

    # Call define_param_transition() with None values
    state_machine.define_param_transition(param_name, start_state, None, None)

    # Retrieve stored transition data
    transition_key = (param_name, start_state)
    transition_data = state_machine.state_sequences.get(transition_key, {})

    #  Ensure transition_data is a dictionary
    assert isinstance(transition_data, dict), f"Expected dictionary, got {type(transition_data)}"

    #  Ensure function_sequence defaults to an empty list
    assert transition_data.get("functions", []) == [], "Expected function_sequence to default to empty list."

    #  Ensure end_state defaults to start_state
    assert transition_data.get("end_state", start_state) == start_state, "Expected end_state to default to start_state."

    def add_execution_chain(self, param, start_state, function_sequence, end_state=None, priority=0):
        """
        Registers an execution chain for a given parameter and start state.

        :param param: The parameter associated with the execution chain.
        :param start_state: The state in which the execution chain is defined.
        :param function_sequence: List of function names (strings) to execute.
        :param end_state: The state the parameter transitions to after execution.
        :param priority: Priority level for execution.
        """

        logger.debug(f"[ADD CHAIN] Registering execution chain for state '{start_state}' with priority {priority}.")

        #  Validate `param` and `start_state`
        if not isinstance(param, str) or not isinstance(start_state, str):
            logger.error(f"Invalid parameter or state: param='{param}', state='{start_state}'. Execution chain not registered.")
            return False  # Prevent invalid inputs

        logger.debug(f"[ADD CHAIN] Registering execution chain for (param='{param}', state='{start_state}') with priority {priority}.")

        #  Validate `function_sequence`
        if not isinstance(function_sequence, list) or not all(isinstance(f, str) for f in function_sequence):
            logger.error(f"[INVALID SEQUENCE] Execution chain for '{param}' must be a list of function names.")
            return False  #  Reject invalid sequence

        #  Default `end_state` to `start_state` if not provided
        if end_state is None:
            end_state = start_state

        key = (param, start_state)  # Keying function sequences by (param, state)

        if key in self.state_sequences:
            logger.warning(f"Execution chain for (param='{param}', state='{start_state}') already exists. Overwriting.")

        #  Register the execution chain
        self.state_sequences[key] = {
            "functions": function_sequence,
            "priority": priority,
            "end_state": end_state,
        }

        #  Ensure a valid transition exists
        self.state_transitions[key] = end_state

        #  Update execution graph for dependencies
        for i in range(len(function_sequence) - 1):
            self.execution_graph.add_edge(function_sequence[i], function_sequence[i + 1])

        logger.info(f"[SUCCESS] Execution chain for (param='{param}', state='{start_state}') → '{end_state}' registered: {function_sequence}")
        logger.debug(f"[GRAPH] Execution graph updated. Nodes: {len(self.execution_graph.nodes())}, Edges: {len(self.execution_graph.edges())}")

        return True

@pytest.mark.asyncio
async def test_verify_provisioning_valid(state_machine):
    """ Ensures verify_provisioning() returns True when everything is correctly set up."""

    #  Register state transitions
    state_machine.add_param("param1", "state1")
    state_machine.add_execution_chain("param1", "state1", ["funcA", "funcB"], priority=0)

    #  Add valid function implementations
    setattr(state_machine, "funcA", lambda: None)
    setattr(state_machine, "funcB", lambda: None)

    #  Check provisioning
    assert state_machine.verify_provisioning() == True, "[ERROR] Expected valid provisioning to pass."


@pytest.mark.asyncio
async def test_verify_provisioning_missing_state(state_machine):
    """ Ensures verify_provisioning() fails when an end state is not defined."""

    #  Missing state
    state_machine.state_transitions[("param1", "state1")] = "state2"

    assert state_machine.verify_provisioning() == False, "[ERROR] Expected provisioning to fail due to missing state."


@pytest.mark.asyncio
async def test_verify_provisioning_missing_function_sequence(state_machine):
    """ Ensures verify_provisioning() fails when a function sequence is missing."""

    state_machine.add_param("param1", "state1")

    #  No execution chain added
    assert state_machine.verify_provisioning() == False, "[ERROR] Expected failure due to missing execution chain."


@pytest.mark.asyncio
async def test_verify_provisioning_function_not_callable( state_machine ):
    """ Ensures verify_provisioning() fails when a function in a sequence is not callable."""

    state_machine.add_param("param1", "state1")
    state_machine.add_execution_chain("param1", "state1", ["not_callable_func"], priority=0)

    #  Function exists but is not callable
    setattr(state_machine, "not_callable_func", "This is a string, not a function")

    assert state_machine.verify_provisioning() == False, "[ERROR] Expected failure due to non-callable function."


@pytest.mark.asyncio
async def test_verify_provisioning_non_list_execution_sequence( state_machine ):
    """ Ensures verify_provisioning() fails when the function sequence is not a list."""

    state_machine.add_param("param1", "state1")

    #  Invalid function sequence (not a list)
    state_machine.state_sequences[("param1", "state1")] = {"functions": "funcA"}

    assert state_machine.verify_provisioning() == False, "[ERROR] Expected failure due to execution sequence not being a list."


@pytest.mark.asyncio
async def test_verify_provisioning_invalid_function_name( state_machine ):
    """ Ensures verify_provisioning() fails when function names in sequence are not strings."""

    state_machine.add_param("param1", "state1")
    state_machine.add_execution_chain("param1", "state1", [123, "valid_func"], priority=0)

    setattr(state_machine, "valid_func", lambda: None)

    #  Function name '123' is not a string
    assert state_machine.verify_provisioning() == False, "[ERROR] Expected failure due to invalid function name."


@pytest.mark.asyncio
async def test_verify_provisioning_multiple_failures(state_machine):
    """ Ensures verify_provisioning() correctly detects multiple provisioning failures."""

    state_machine.add_param("param1", "state1")

    #  Missing state in transition
    state_machine.state_transitions[("param1", "state1")] = "state2"

    #  Execution sequence is not a list
    state_machine.state_sequences[("param1", "state1")] = {"functions": "invalid"}

    assert state_machine.verify_provisioning() == False, "[ERROR] Expected failure due to multiple provisioning issues."

@pytest.mark.asyncio
async def test_run_aborts_on_function_failure(state_machine, caplog):
    """
    Ensures that if a function in the sequence fails, execution is aborted,
    the queue is cleared, and recovery is triggered.
    """

    #  Register the parameter
    state_machine.add_param("param1", "state1")

    #  Define a function that raises an exception (failure simulation)
    async def failing_function():
        raise Exception("Simulated Execution Failure")

    #  Define a function that should NEVER run if failure occurs
    async def should_not_run():
        logger.error("[ERROR] This function should not have executed!")

    #  Dynamically add the functions to the state machine
    setattr(state_machine, "failing_function", failing_function)
    setattr(state_machine, "should_not_run", should_not_run)

    #  Add an execution chain where failing_function should cause a stop
    state_machine.add_execution_chain("param1", "state1", ["failing_function", "should_not_run"], priority=0)

    #  Verify provisioning before execution
    assert state_machine.verify_provisioning(), "[ERROR] Provisioning verification failed before execution."

    #  Queue the parameter for execution
    await state_machine.execution_queue.put("param1")

    #  Run the state machine
    await state_machine.run()

    #  The second function should NOT execute
    assert "[ERROR] This function should not have executed!" not in caplog.text, \
        "[TEST FAILED] Execution did not stop after the first function failed."

    #  The execution queue should be cleared after failure
    assert state_machine.execution_queue.qsize() == 0, \
        "[TEST FAILED] Execution queue was not cleared after failure."

    #  Ensure failure and recovery were logged
    assert "[FAILURE] Function sequence failed for 'param1'. Aborting execution." in caplog.text, \
        "[TEST FAILED] Failure was not detected correctly."

    assert "Execution error: Simulated Execution Failure" in caplog.text, \
        "[TEST FAILED] The expected execution failure was not logged."

    assert "[RECOVERY] Recovery process completed. Execution halted." in caplog.text, \
        "[TEST FAILED] Recovery was not executed after failure."


@pytest.mark.asyncio
async def test_run_handles_multiple_failures(state_machine, caplog):
    """
    Ensures that if multiple parameters are queued, and one fails, 
    the queue is cleared, and recovery runs only once.
    """

    #  Register two parameters
    state_machine.add_param("param1", "state1")
    state_machine.add_param("param2", "state2")

    #  Define a function that fails for param1
    async def failing_function():
        raise Exception("Simulated Failure for param1")

    #  Define a valid function for param2
    async def valid_function():
        logger.info("[EXECUTED] valid_function executed successfully.")

    #  Add functions dynamically
    setattr(state_machine, "failing_function", failing_function)
    setattr(state_machine, "valid_function", valid_function)

    #  Add execution chains
    state_machine.add_execution_chain("param1", "state1", ["failing_function"])
    state_machine.add_execution_chain("param2", "state2", ["valid_function"])

    #  Queue both parameters
    await state_machine.execution_queue.put("param1")
    await state_machine.execution_queue.put("param2")

    #  Run the state machine
    await state_machine.run()

    #  Ensure param2 was NEVER executed because of failure in param1
    assert "[EXECUTED] valid_function executed successfully." not in caplog.text, \
        "[TEST FAILED] Execution did not halt after failure."

    #  Ensure queue was cleared after failure
    assert state_machine.execution_queue.qsize() == 0, \
        "[TEST FAILED] Queue was not cleared after function failure."

    #  Ensure recovery was triggered
    assert "[RECOVERY] Recovery process completed. Execution halted." in caplog.text, \
        "[TEST FAILED] Recovery did not execute after function failure."

    #  Ensure failure was logged
    assert "[FAILURE] Function sequence failed for 'param1'. Aborting execution." in caplog.text, \
        "[TEST FAILED] Function failure was not properly logged."


@pytest.mark.asyncio
async def test_run_handles_no_recovery_sequence(state_machine, caplog):
    """
    Ensures that if a function sequence fails but no recovery sequence is defined, 
    execution still stops, but without a recovery attempt.
    """

    #  Register the parameter
    state_machine.add_param("param1", "state1")

    #  Define a function that raises an exception
    async def failing_function():
        raise Exception("Simulated Execution Failure")

    setattr(state_machine, "failing_function", failing_function)

    #  Add execution chain with a failing function
    state_machine.add_execution_chain("param1", "state1", ["failing_function"])

    #  Ensure no recovery sequence is set
    state_machine.recovery_chain = []

    #  Queue the parameter for execution
    await state_machine.execution_queue.put("param1")

    #  Run the state machine
    await state_machine.run()

    #  Ensure failure was logged
    assert "[FAILURE] Function sequence failed for 'param1'. Aborting execution." in caplog.text, \
        "[TEST FAILED] Failure was not logged correctly."

    assert "Execution error: Simulated Execution Failure" in caplog.text, \
        "[TEST FAILED] The expected execution failure was not logged."

    #  Ensure that recovery was NOT triggered
    assert "[RECOVERY] Initiating recovery sequence." not in caplog.text, \
        "[TEST FAILED] Recovery should not have been attempted."

    assert "[RECOVERY COMPLETE] Recovery process finished." not in caplog.text, \
        "[TEST FAILED] Recovery should not have executed."

    #  Ensure queue was cleared
    assert state_machine.execution_queue.qsize() == 0, \
        "[TEST FAILED] Queue was not cleared after failure."

