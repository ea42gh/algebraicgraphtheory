import warnings
import re
import colorsys
from matplotlib.colors import to_rgb, CSS4_COLORS

def convert_to_rgb(color):
    """Convert a color from various formats to an RGB string 'rgb(R,G,B)', ensuring values are within range.

    - Issues warnings instead of raising exceptions.
    - Returns 'rgb(0,0,0)' for invalid inputs.
    """
    try:
        if not isinstance(color, (str, tuple, list, int, float)):
            warnings.warn(f"Invalid type {type(color)} for color: {color}. Returning black.", UserWarning)
            return "rgb(0,0,0)"

        if isinstance(color, str):
            color = color.strip().lower()

            if color in CSS4_COLORS:
                return convert_named_color_to_rgb(color)

            elif color.startswith("rgb("):
                return convert_rgb_to_rgb(color)

            elif color.startswith("rgba("):
                return convert_rgba_to_rgb(color)

            elif color.startswith("#"):
                return convert_hex_to_rgb(color)

            elif color.startswith("hsl("):
                return convert_hsl_to_rgb(color)

        elif isinstance(color, (tuple, list, int, float)):
            return convert_tuple_or_numeric_to_rgb(color)

        warnings.warn(f"Unable to convert '{color}' to RGB. Returning black.", UserWarning)
        return "rgb(0,0,0)"

    except Exception as e:
        warnings.warn(f"Error processing '{color}': {str(e)}. Returning black.", UserWarning)
        return "rgb(0,0,0)"

def convert_to_rgb_batch(colors: dict) -> dict:
    """
    Convert a dictionary of colors from various formats to RGB.

    Args:
        colors (dict): A dictionary of colors in a uniform format.

    Returns:
        dict: A dictionary where each input color is converted to 'rgb(R,G,B)'.

    Notes:
        - Assumes all values in the input dictionary follow the same format.
        - Catches errors from lower-level functions, issues warnings, and falls back to convert_to_rgb.
        - Never raises exceptions.
    """

    if not isinstance(colors, dict):
        warnings.warn("Expected a dictionary of colors. Returning an empty dictionary.", UserWarning)
        return {}

    if not colors:
        return {}

    sample_color = next(iter(colors.values()), None)

    if sample_color is None:
        return {}  # Ensure an empty dictionary is returned if all values are None

    try:
        if isinstance(sample_color, str):
            sample_color = sample_color.strip().lower()

            if sample_color in CSS4_COLORS:
                return {k: convert_named_color_to_rgb(v) for k, v in colors.items()}

            if sample_color.startswith("rgb("):
                return {k: convert_rgb_to_rgb(v) for k, v in colors.items()}

            if sample_color.startswith("rgba("):
                return {k: convert_rgba_to_rgb(v) for k, v in colors.items()}

            if sample_color.startswith("hsl("):
                return {k: convert_hsl_to_rgb(v) for k, v in colors.items()}

            if sample_color.startswith("#"):
                return {k: convert_hex_to_rgb(v) for k, v in colors.items()}

            warnings.warn(f"Unknown color format: {sample_color}.", UserWarning)
            return {k: convert_to_rgb(v) for k, v in colors.items()}

    except Exception as e:
        #warnings.warn(f"Error converting colors: {e}. Falling back to convert_to_rgb.", UserWarning)
        return {k: convert_to_rgb(v) for k, v in colors.items()} if colors else {}

    return {}

def convert_tuple_or_numeric_to_rgb(color):
    """
    Converts a tuple, list, or single numeric value to an RGB string.

    Raises:
        ValueError: If any RGB values are out of range or if input format is invalid.
    """
    if isinstance(color, bool):  # Explicitly reject boolean values
        raise ValueError(f"Unsupported input type: {color}. Expected tuple, list, int, or float.")

    if isinstance(color, (tuple, list)):
        if len(color) not in (3, 4):
            raise ValueError(f"Invalid tuple/list length: {color}. Expected (R,G,B) or (R,G,B,A).")

        if not all(isinstance(c, (int, float)) for c in color[:3]):
            raise ValueError(f"Invalid value types in {color}. RGB values must be integers or floats.")

        r, g, b = color[:3]

        # Strict check: No clamping, must be valid range
        if not (0 <= r <= 255 and 0 <= g <= 255 and 0 <= b <= 255):
            raise ValueError(f"RGB values out of range in {color}. Must be between 0 and 255.")

        return f"rgb({int(r)},{int(g)},{int(b)})"

    elif isinstance(color, (int, float)):  # Single numeric input (gray-scale)
        if not (0 <= color <= 255):
            raise ValueError(f"Gray-scale value {color} out of range (0-255).")
        gray = int(round(color))
        return f"rgb({gray},{gray},{gray})"

    else:
        raise ValueError(f"Unsupported input type: {color}. Expected tuple, list, int, or float.")

def convert_rgb_to_rgb(color: str) -> str:
    """
    Verifies that a given string is a properly formatted 'rgb(R,G,B)' string.

    Args:
        color (str): A string in the format 'rgb(R,G,B)'.

    Returns:
        str: The same input string if valid.

    Raises:
        ValueError: If the input is not a valid 'rgb(R,G,B)' string.
    """
    # ✅ Regular expression to match 'rgb(R,G,B)' format
    rgb_pattern = re.compile(r"^rgb\((-?\d{1,3}),\s*(-?\d{1,3}),\s*(-?\d{1,3})\)$")

    match = rgb_pattern.match(color)
    if not match:
        raise ValueError(f"Invalid RGB format: {color}. Expected 'rgb(R,G,B)'.")

    # ✅ Extract numeric values and ensure they are within range 0-255
    r, g, b = map(int, match.groups())
    if not (0 <= r <= 255 and 0 <= g <= 255 and 0 <= b <= 255):
        raise ValueError(f"RGB values out of range: {color}. Must be between 0-255.")

    return color  # ✅ Return unchanged if valid

def convert_str_triplet_to_rgb(color: str) -> tuple[int, int, int]:
    """
    Converts a string of the form "(R,G,B)" to an (R, G, B) tuple.

    Args:
        color (str): A string in the format "(R,G,B)".

    Returns:
        tuple[int, int, int]: A validated RGB triplet.

    Raises:
        ValueError: If the input is not a valid (R,G,B) formatted string.
    """
    if not isinstance(color, str):
        raise ValueError(f"Invalid RGB format: {color}. Expected '(R,G,B)'.")

    rgb_pattern = re.compile(r"^\(\s*(-?\d{1,3})\s*,\s*(-?\d{1,3})\s*,\s*(-?\d{1,3})\s*\)$")

    match = rgb_pattern.match(color)
    if not match:
        raise ValueError(f"Invalid RGB format: {color}. Expected '(R,G,B)'.")

    r, g, b = map(int, match.groups())

    if not (0 <= r <= 255 and 0 <= g <= 255 and 0 <= b <= 255):
        raise ValueError(f"RGB values out of range: {color}. Must be between 0-255.")

    return (r, g, b)

def convert_triplet_to_rgb(color: tuple[int, int, int]) -> str:
    """
    Converts an (R, G, B) tuple to an 'rgb(R,G,B)' string.

    Args:
        color (tuple[int, int, int]): A tuple with three integers (R, G, B).

    Returns:
        str: A properly formatted 'rgb(R,G,B)' string.

    Raises:
        ValueError: If the input is not a valid (R, G, B) triplet.
    """
    if not (isinstance(color, tuple) and len(color) == 3):
        raise ValueError(f"Invalid format: {color}. Expected a tuple of (R,G,B).")

    if not all(isinstance(c, int) for c in color):
        raise ValueError(f"Invalid values: {color}. RGB values must be integers.")

    if not all(0 <= c <= 255 for c in color):
        raise ValueError(f"RGB values out of range: {color}. Must be between 0-255.")

    return f"rgb({color[0]},{color[1]},{color[2]})"

def convert_rgba_to_rgb(rgba_color: str) -> str:
    """Convert an RGBA string to an RGB string by removing the alpha channel."""
    # Extract numerical values from the RGBA string
    rgba_values = re.findall(r'-?\d+(?:\.\d+)?', rgba_color)
    rgba = [int(x) if i < 3 else float(x) for i, x in enumerate(rgba_values)]  # Convert first 3 values to int, last to float

    if len(rgba) != 4:
        raise ValueError(f"Invalid RGBA color format: '{rgba_color}'. Expected format: 'rgba(R,G,B,A)'.")

    r, g, b, a = rgba  # Extract values

    if not all(0 <= c <= 255 for c in (r, g, b)):
        raise ValueError(f"RGB values out of range in '{rgba_color}': ({r}, {g}, {b}). Must be between 0–255.")

    if not (0 <= a <= 1):
        raise ValueError(f"Alpha value out of range in '{rgba_color}': {a}. Must be between 0–1.")

    return f"rgb({r},{g},{b})"

def convert_hsl_to_rgb(hsl_color:str) -> str:
    """Converts an HSL string to an RGB string."""
    hsl_match = re.match(r"hsl\((-?\d+),(-?\d+)%?,(-?\d+)%?\)", hsl_color.strip().lower())
    if not hsl_match or not re.match(r"hsl\(-?\d+,\d+%,\d+%\)", hsl_color.strip().lower()):
        raise ValueError(f"Invalid HSL color format: {hsl_color}")

    h, s, l = map(int, hsl_match.groups())
    if not (0 <= h <= 360):
        raise ValueError(f"Hue {h} is out of range (0-360).")
    if not (0 <= s <= 100):
        raise ValueError(f"Saturation {s}% is out of range (0-100%).")
    if not (0 <= l <= 100):
        raise ValueError(f"Lightness {l}% is out of range (0-100%).")

    h = max(0, min(360, h))  # Ensure hue is between 0-360
    s = max(0, min(100, s)) / 100  # Normalize saturation
    l = max(0, min(100, l)) / 100  # Normalize lightness

    if s == 0:
        r = g = b = int(l * 255)
    else:
        def hue_to_rgb(p, q, t):
            if t < 0:
                t += 1
            if t > 1:
                t -= 1
            if t < 1/6:
                return p + (q - p) * 6 * t
            if t < 1/2:
                return q
            if t < 2/3:
                return p + (q - p) * (2/3 - t) * 6
            return p

        q = l * (1 + s) if l < 0.5 else l + s - l * s
        p = 2 * l - q
        r = hue_to_rgb(p, q, h / 360 + 1/3)
        g = hue_to_rgb(p, q, h / 360)
        b = hue_to_rgb(p, q, h / 360 - 1/3)

        r = int(round(r * 255))
        g = int(round(g * 255))
        b = int(round(b * 255))

    return f'rgb({r},{g},{b})'

def convert_hex_to_rgb(hex_color):
    """Convert a HEX color to an RGB string."""
    if not isinstance(hex_color, str):
        raise ValueError(f"Invalid HEX color: {hex_color}")

    hex_color = hex_color.strip()

    if hex_color.startswith("#"):  # Ensure only valid shorthand HEX gets expanded
        hex_color = hex_color.lstrip("#")
        if len(hex_color) == 3:  # Expand shorthand only if it had '#'
            hex_color = "".join([c * 2 for c in hex_color])

    if not re.match(r"^[0-9A-Fa-f]{6}$", hex_color):
        raise ValueError(f"Invalid HEX color: {hex_color}")

    r, g, b = [int(hex_color[i:i+2], 16) for i in (0, 2, 4)]
    return f"rgb({r},{g},{b})"

def convert_named_color_to_rgb(named_color:str) -> str:
    rgb_tuple = to_rgb(named_color)
    return f'rgb({int(rgb_tuple[0]*255)},{int(rgb_tuple[1]*255)},{int(rgb_tuple[2]*255)})'

def convert_rgb_to_hex(rgb:str) -> str:
    """
    Converts an RGB tuple to HEX format.

    Args:
        rgb (tuple[int, int, int]): A tuple containing three integers (R, G, B).

    Returns:
        str: The HEX representation of the color.

    Raises:
        ValueError: If the input is not a valid (R, G, B) tuple.
    """
    # Ensure input is a tuple of exactly three values
    if not (isinstance(rgb, tuple) and len(rgb) == 3):
        raise ValueError(f"Invalid RGB format: {rgb}. Expected a tuple of three integers (R, G, B).")

    # Ensure all values are integers in the range [0, 255]
    if not all(isinstance(c, int) and 0 <= c <= 255 for c in rgb):
        raise ValueError(f"RGB values out of range or invalid: {rgb}. Must be integers between 0 and 255.")

    # Convert to HEX format
    return "#{:02x}{:02x}{:02x}".format(rgb[0], rgb[1], rgb[2])
