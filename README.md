# AlgebraicGraphTheory

## Description
Jupyter Notebooks introducing some ideas from Algebraic Gaph Theory.
Provide just enough explanations and code to serve as an introduction
to an application of linear algebra.

**Work in progress** teaching myself some graph theory,
and bulding up some library routines...

The backends are both python as well as julia.

The example graphs displayed in the notebooks are drawn with various tools, including
* holoviews Graphs
* plotly
* matplotlib graphs created by various libraries
* cytoscape graphs
* GraphMakie

The algorithms are drawn from
* NetworkX
* GraphBLAS

