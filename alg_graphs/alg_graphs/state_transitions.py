import asyncio
import logging
import networkx as nx
import json
import traceback
from datetime import datetime

# ===================================================================================================================
# Structured Logging Setup (Writes to state_machine.log)
# -------------------------------------------------------------------------------------------------------------------
def log_exception(exception, msg=""):
    """Logs exceptions with stack trace."""
    logger.critical(f"{msg} | {str(exception)}")
    logger.critical(traceback.format_exc())
# -------------------------------------------------------------------------------------------------------------------
def setup_logging():
    """Configures structured logging for state transitions."""
    import sys

    logger = logging.getLogger("ParamStateMachine")
    logger.setLevel(logging.DEBUG)

    if logger.hasHandlers():
        logger.handlers.clear()

    file_handler = logging.FileHandler("state_machine.log", mode="w")
    file_handler.setLevel(logging.DEBUG)
    file_formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S")
    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler(sys.stdout)
    console_formatter = logging.Formatter("[%(levelname)s] %(message)s")
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(console_formatter)
    logger.addHandler(console_handler)

    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG, force=True)

    return logger


logger = setup_logging()


# ===================================================================================================================
# Param Statemachine Implementation
# ===================================================================================================================
class ParamStateMachine:
    """
    ParamStateMachine: A Finite State Machine (FSM) for Controlling Execution Order in Graph-Based Visualization.

    Design Summary:
    - Uses an execution graph (NetworkX `DiGraph`) to enforce dependency constraints.
    - Implements finite state transitions to manage function execution sequences.
    - Supports asynchronous execution (`asyncio.Lock()`) for UI performance.
    - Provides priority-based execution to handle parameter updates effectively.
    - Can be saved/loaded from JSON for persistence.
    - Supports recovery chains for failed executions (without raising errors).

    Enhanced Debugging:
    - Structured logging (`logging.Logger`).
    - Graph-based visualization of execution dependencies.
    - Interactive debug console for live state inspection.
    """
    # ---------------------------------------------------------------------------------------------------------------
    class State:
        """Defines the finite states of the execution process."""
        INIT         = 0
        IDLE         = 1
        RUNNING      = 2
        ERROR        = 3
        FINAL_RENDER = 4
    # ---------------------------------------------------------------------------------------------------------------
    def __init__(self, viz):
        """Initializes ParamStateMachine."""
        self.viz                 = viz
        self.param_to_state      = {}
        self.state_sequences     = {}
        self.state_priorities    = {}
        self.state_transitions   = {}
        self.pending_updates     = set()
        self.custom_states       = {}
        self.execution_graph     = nx.DiGraph()
        self._lock               = asyncio.Lock()
        self.current_state       = self.State.INIT
        self.recovery_chain      = []
        self.running             = False
        self.execution_queue     = asyncio.Queue()

        self.verify_provisioning()

        # Debug log to verify attribute setup
        logger.info("[INIT] ParamStateMachine initialized.")
        logger.debug(f"[INIT] Execution graph created. Nodes: {len(self.execution_graph.nodes())} | Edges: {len(self.execution_graph.edges())}")
        logger.debug(f"[INIT] State dictionaries initialized with {len(self.param_to_state)} parameters.")

        # List all member functions (IC Compliance: Indented by 4 spaces)
        methods = [method for method in dir(self) if callable(getattr(self, method)) and not method.startswith("__")]
        logger.debug("[INIT] Available member functions:")
        for method in sorted(methods):
            logger.debug(f"    {method}")

        logger.info("[INIT] ParamStateMachine initialization complete.")
    # ---------------------------------------------------------------------------------------------------------------
    def verify_provisioning(self, param_name=None):
        """
        Validates that all necessary transitions, execution sequences, and functions exist.

        If a specific parameter is given, it ensures that parameter's provisioning is correct.

        Returns:
            bool: True if checks pass, False otherwise.
        """
        logger.info("[PROVISIONING CHECK] Verifying system integrity.")

        missing_states = []
        invalid_transitions = []
        execution_issues = []
        function_errors = []

        parameters_to_check = [param_name] if param_name else self.param_to_state.keys()

        for param in parameters_to_check:
            if param not in self.param_to_state:
                logger.error(f"[PROVISIONING ERROR] Parameter '{param}' is not registered.")
                function_errors.append(param)
                continue

            start_state = self.param_to_state[param]
            transition_key = (param, start_state)

            #  Check if the transition is defined
            if transition_key not in self.state_transitions:
                logger.error(f"[PROVISIONING ERROR] No transition defined for '{param}' from '{start_state}'.")
                invalid_transitions.append(param)
                continue

            end_state = self.state_transitions[transition_key]

            #  Ensure the end state exists
            if end_state not in self.state_sequences:
                logger.error(f"[PROVISIONING ERROR] State '{end_state}' for transition '{param}' is undefined.")
                missing_states.append((param, start_state, end_state))

            #  Validate function sequence
            execution_chain = self.state_sequences.get(start_state, {}).get("functions", [])
            if not isinstance(execution_chain, list):
                logger.error(f"[PROVISIONING ERROR] Execution sequence is not a list for state '{start_state}'.")
                execution_issues.append(param)
                continue

            for func in execution_chain:
                if not isinstance(func, str):
                    logger.error(f"[PROVISIONING ERROR] Function name '{func}' should be a string.")
                    function_errors.append(func)
                    continue

                #  Check if the function is actually callable
                if not hasattr(self, func) or not callable(getattr(self, func)):
                    logger.error(f"[PROVISIONING ERROR] Function '{func}' is not callable or does not exist.")
                    function_errors.append(func)

        #  If **any** errors were found, provisioning fails
        if missing_states or invalid_transitions or execution_issues or function_errors:
            logger.warning(f"[PROVISIONING FAILED] {len(missing_states)} missing states, {len(invalid_transitions)} invalid transitions, {len(execution_issues)} execution issues, {len(function_errors)} function errors.")
            return False

        logger.info("[PROVISIONING SUCCESS] All state transitions and function sequences are valid.")
        return True

    # ---------------------------------------------------------------------------------------------------------------
    def add_param(self, param_name, state):
        """Registers a parameter with an initial state and ensures state registration."""
        logger.debug(f"[ADD PARAM] Attempting to register parameter '{param_name}' with state '{state}'.")

        if param_name in self.param_to_state:
            logger.warning(f"[WARNING] Parameter '{param_name}' already exists. Overwriting.")

        self.param_to_state[param_name] = state
        logger.info(f"[SUCCESS] Parameter '{param_name}' registered with state '{state}'.")

        # 🔹 Ensure the state is registered in transitions
        if (param_name, state) not in self.state_transitions:
            self.state_transitions[(param_name, state)] = state  # Default self-loop
            logger.debug(f"[AUTO REGISTER] Added missing state '{state}' as self-transition.")

    # ---------------------------------------------------------------------------------------------------------------
    def add_execution_chain(self, param, start_state, function_sequence, end_state=None, priority=0):
        """
        Registers an execution chain for a given parameter and start state.

        :param param: The parameter associated with the execution chain.
        :param start_state: The state in which the execution chain is defined.
        :param function_sequence: List of function names (strings) to execute.
        :param end_state: The state the parameter transitions to after execution.
        :param priority: Priority level for execution.
        :return: True if registration is successful, False otherwise.
        """

        logger.debug(f"[ADD CHAIN] Registering execution chain for state '{start_state}' with priority {priority}.")

        #  **Validate `param` and `start_state`**
        if not isinstance(param, str) or not isinstance(start_state, str):
            logger.error(f"Invalid parameter or state: param='{param}', state='{start_state}'. Execution chain not registered.")
            return False

        #  **Validate `function_sequence`**
        if not isinstance(function_sequence, list) or not all(isinstance(f, str) for f in function_sequence):
            logger.error(f"[INVALID SEQUENCE] Execution chain for '{param}' must be a list of function names.")
            return False  #   Reject invalid sequence

        #  **Default `end_state` to `start_state` if not provided**
        if end_state is None:
            end_state = start_state

        # 🏗 **Ensure states exist in the system before registration**
        if start_state not in self.state_sequences:
            logger.warning(f"[AUTO-REGISTER] Start state '{start_state}' was missing. Adding it to state sequences.")
            self.state_sequences[start_state] = {"functions": [], "priority": 0, "end_state": start_state}

        if end_state not in self.state_sequences:
            logger.warning(f"[AUTO-REGISTER] End state '{end_state}' was missing. Adding it to state sequences.")
            self.state_sequences[end_state] = {"functions": [], "priority": 0, "end_state": end_state}

        # 🔑 **Create execution chain key**
        key = (param, start_state)

        if key in self.state_sequences:
            logger.warning(f"Execution chain for (param='{param}', state='{start_state}') already exists. Overwriting.")

        # 🔗 **Register the execution chain**
        self.state_sequences[key] = {
            "functions": function_sequence,
            "priority": priority,
            "end_state": end_state,
        }

        #  **Ensure a valid state transition exists**
        self.state_transitions[key] = end_state

        #  **Update execution graph for function dependencies**
        for i in range(len(function_sequence) - 1):
            self.execution_graph.add_edge(function_sequence[i], function_sequence[i + 1])

        logger.info(f"[SUCCESS] Execution chain for (param='{param}', state='{start_state}') → '{end_state}' registered: {function_sequence}")
        logger.debug(f"[GRAPH] Execution graph updated. Nodes: {len(self.execution_graph.nodes())}, Edges: {len(self.execution_graph.edges())}")

        return True

    # ---------------------------------------------------------------------------------------------------------------
    async def param_changed(self, param_name):
        """Handles parameter changes by validating, logging, and queueing them for execution or direct state transition."""
        
        logger.debug(f"[PARAM CHANGE] Change detected for '{param_name}'.")

        #  Validate Parameter
        if param_name not in self.param_to_state:
            logger.warning(f"[INVALID PARAM] '{param_name}' is not registered. Ignoring change.")
            return False  # Exit early

        current_state = self.param_to_state[param_name]
        transition_key = (param_name, current_state)

        #  Check for Direct State Transition
        if transition_key in self.state_transitions:
            new_state = self.state_transitions[transition_key]
            self.param_to_state[param_name] = new_state  # Apply state change
            logger.info(f"[STATE TRANSITION] '{param_name}' moved from '{current_state}' → '{new_state}' without function execution.")
            return True  # Successfully transitioned

        #  Check for Execution Sequence
        if transition_key in self.state_sequences:
            try:
                if self.execution_queue.full():
                    logger.error(f"[QUEUE FULL] Cannot queue '{param_name}'. Execution queue is at capacity.")
                    return False  # Queue is full

                await self.execution_queue.put(param_name)
                logger.info(f"[QUEUE] Parameter '{param_name}' added to execution queue. Current state: '{current_state}'.")
                return True  # Successfully queued

            except asyncio.QueueFull:
                logger.error(f"[QUEUE ERROR] Failed to queue '{param_name}'. Queue overflow detected.")
                return False  # Failure due to queue overflow

        #  No Transition or Execution Defined
        logger.info(f"[NO EXECUTION] No function sequence or state transition for '{param_name}' in state '{current_state}'. Skipping.")
        return False

    # ---------------------------------------------------------------------------------------------------------------
    async def run(self):
        """Executes function sequences for queued updates."""
    
        logger.debug(f"[RUN] Entering run(). self.running: {self.running}")
    
        if self.running:
            logger.debug("[RUN] Already running. Exiting.")
            return
    
        self.running = True  # Ensure we track the running state correctly
    
        try:
            logger.debug("[LOCK] Attempting to acquire execution lock.")
            async with self._lock:
                logger.debug("[LOCK] Execution lock acquired.")
                logger.info(f"[RUN] Processing queued updates. Queue size: {self.execution_queue.qsize()}")
    
                while not self.execution_queue.empty():
                    param_name = await self.execution_queue.get()
                    logger.debug(f"[QUEUE] Dequeued parameter: {param_name}. Remaining queue: {list(self.execution_queue._queue)}")
    
                    if param_name not in self.param_to_state:
                        logger.warning(f"[SKIP] Invalid or removed parameter '{param_name}'. Skipping execution.")
                        continue
    
                    start_state = self.param_to_state[param_name]
                    transition_key = (param_name, start_state)
                    function_sequence = self.state_sequences.get(transition_key, {}).get("functions", [])
                    end_state = self.state_sequences.get(transition_key, {}).get("end_state", start_state)
                    logger.debug(f"[TRANSITION] Checking transition for '{param_name}' (Start: {start_state}, End: {end_state})")
    
                    if function_sequence:
                        logger.info(f"[EXECUTION] Starting function sequence for '{param_name}': {function_sequence}")
    
                        try:
                            await self._execute_function_sequence(function_sequence)
                            logger.debug(f"[EXECUTION COMPLETE] Function sequence finished for '{param_name}'.")
    
                        except Exception as e:
                            logger.error(f"[FAILURE] Function sequence failed for '{param_name}'. Aborting execution.")
                            logger.critical(f"Execution error: {str(e)}", exc_info=True)
    
                            #  Clear queue and stop further processing
                            while not self.execution_queue.empty():
                                self.execution_queue.get_nowait()
                                self.execution_queue.task_done()
    
                            #  Trigger recovery sequence
                            await self._handle_execution_error(e, param_name)
                            logger.debug(f"[RECOVERY] Recovery process completed. Execution halted.")
                            return  #  Stop processing further queue items
    
                    else:
                        logger.info(f"[NO EXECUTION] No function sequence for '{param_name}', transitioning to '{end_state}'.")
    
                    self.param_to_state[param_name] = end_state  # Update parameter state
                    logger.info(f"[STATE UPDATE] '{param_name}' moved to state '{end_state}'.")
    
                    self.execution_queue.task_done()
                    logger.debug(f"[QUEUE] Execution task completed for '{param_name}'. Remaining queue: {self.execution_queue.qsize()}.")
    
                logger.info(f"[RUN COMPLETE] Execution cycle finished. Final state: {self.param_to_state}.")
    
        except Exception as e:
            logger.critical(f"Execution error: {str(e)}", exc_info=True)
            await self._handle_execution_error(e, None)
            logger.debug("[ERROR HANDLING] Recovery process initiated.")
    
        finally:
            self.running = False
            self.active_task = None
            logger.info(f"[RUN END] Execution finished. Resetting state. Final state: {self.param_to_state}.")

    # ---------------------------------------------------------------------------------------------------------------
    def define_param_transition(self, param_name, start_state, end_state=None, function_sequence=None, priority=0):
        """Defines a state transition for a specific parameter with an optional function sequence."""

        #  Validate required parameters
        if not isinstance(param_name, str) or not isinstance(start_state, str):
            logger.error(f"[INVALID] Transition definition failed: param='{param_name}', start_state='{start_state}' must be strings.")
            return False  #  Prevent invalid input

        logger.debug(f"[TRANSITION] Defining transition for (param='{param_name}', start_state='{start_state}') → '{end_state or start_state}'.")

        #  Default `end_state` to `start_state` if not provided
        if end_state is None:
            end_state = start_state

        #  Ensure `function_sequence` is a list
        if function_sequence is None:
            function_sequence = []
        elif not isinstance(function_sequence, list) or not all(isinstance(f, str) for f in function_sequence):
            logger.error(f"[INVALID SEQUENCE] Function sequence must be a list of function names: {function_sequence}")
            return False

        key = (param_name, start_state)

        #  Ensure `start_state` and `end_state` exist
        if start_state not in self.state_sequences:
            logger.warning(f"[AUTO-REGISTER] Start state '{start_state}' was missing. Adding it.")
            self.state_sequences[start_state] = {"functions": [], "priority": 0}

        if end_state not in self.state_sequences:
            logger.warning(f"[AUTO-REGISTER] End state '{end_state}' was missing. Adding it.")
            self.state_sequences[end_state] = {"functions": [], "priority": 0}

        #  Handle redundant transitions
        if key in self.state_transitions:
            logger.warning(f"[DUPLICATE] Transition for '{param_name}' from '{start_state}' already exists. Overwriting.")

        #  Register transition & priority
        self.state_transitions[key] = end_state
        self.param_to_state[param_name] = start_state
        self.state_priorities[param_name] = priority

        #  Store transition in execution sequence mapping
        self.state_sequences[key] = {
            "functions": function_sequence,
            "end_state": end_state,
            "priority": priority,
        }

        logger.info(f"[TRANSITION REGISTERED] '{param_name}' ({start_state} → {end_state}) via {function_sequence} (Priority: {priority}).")

        return True

    # ---------------------------------------------------------------------------------------------------------------
    async def _execute_function_sequence(self, function_sequence):
        """Executes a sequence of visualization update functions asynchronously, ensuring resilience and proper state tracking."""

        # Validate function_sequence type
        if not isinstance(function_sequence, list):
            logger.error(f"Invalid function sequence: Expected list, got {type(function_sequence)} - Value: {function_sequence}")
            return False  # Indicate failure

        logger.info(f"[START] Executing function sequence: {function_sequence}")

        executed_functions = []
        failed_functions = []

        for func_name in function_sequence:
            logger.debug(f"[CHECK] Validating function: {func_name}")

            # Validate function name
            if not isinstance(func_name, str):
                logger.error(f"[INVALID] Function name must be a string, found {type(func_name)} - Value: {func_name}")
                failed_functions.append(func_name)
                continue

            # Fetch function reference
            func = getattr(self.viz, func_name, None) or getattr(self, func_name, None)

            if not callable(func):
                logger.warning(f"[MISSING] Function '{func_name}' is not found or not callable. Skipping.")
                failed_functions.append(func_name)
                continue

            # Attempt execution
            try:
                result = func()
                if asyncio.iscoroutine(result):
                    await result  # Await async functions

                executed_functions.append(func_name)
                logger.info(f"[EXECUTED] {func_name}")

            except Exception as e:
                logger.critical(f"[EXECUTION ERROR] Function '{func_name}' failed: {e}", exc_info=True)
                failed_functions.append(func_name)

        # Log final execution summary
        if failed_functions:
            logger.warning(f"[COMPLETE] Execution finished. Success: {executed_functions}, Failed: {failed_functions}")
        else:
            logger.info(f"[COMPLETE] Execution finished. All functions executed successfully.")

        return len(failed_functions) == 0  # Return success status

    # ---------------------------------------------------------------------------------------------------------------
    async def _handle_execution_error(self, error, function_name=None):
        """Handles execution errors and attempts recovery."""

        function_name = function_name or "Unhandled execution error"

        # Capture and log the error traceback
        error_traceback = traceback.format_exc()
        logger.error(f"[EXECUTION ERROR] {function_name} failed: {error}\n[TRACEBACK]\n{error_traceback}")

        # Validate the recovery chain
        if not isinstance(self.recovery_chain, list) or not all(isinstance(func, str) for func in self.recovery_chain):
            logger.warning("[RECOVERY] Invalid recovery sequence format. Skipping recovery.")
            return False

        if not self.recovery_chain:
            logger.info("[RECOVERY] No recovery sequence defined. Execution will not be retried.")
            return False

        logger.info("[RECOVERY] Initiating recovery sequence.")

        success = True  # Track if recovery is successful
        for recovery_func in self.recovery_chain:
            func = getattr(self.viz, recovery_func, None)

            if not callable(func):
                logger.warning(f"[RECOVERY WARNING] Recovery function '{recovery_func}' not found or not callable. Skipping.")
                continue

            try:
                logger.info(f"[RECOVERY] Executing '{recovery_func}'")
                result = func()
                if asyncio.iscoroutine(result):
                    await result  # Await if coroutine
                logger.info(f"[RECOVERY SUCCESS] '{recovery_func}' executed successfully.")
            except Exception as recovery_error:
                logger.critical(f"[RECOVERY ERROR] Failed '{recovery_func}': {recovery_error}", exc_info=True)
                success = False  # Mark recovery as failed

        logger.info("[RECOVERY COMPLETE] Recovery process finished.")
        return success  # Return whether recovery was successful

    # ---------------------------------------------------------------------------------------------------------------
    def set_recovery_chain(self, recovery_sequence, force=True):
        """Defines a recovery sequence to be executed in case of failure."""
    
        logger.info(f"[RECOVERY SETUP] Defining recovery chain: {recovery_sequence}")
    
        if not isinstance(recovery_sequence, list):
            logger.error("[ERROR] Recovery sequence must be a list of function names.")
            return False  #  Reject invalid input
    
        # Prevent accidental overwrites unless `force=True`
        if self.recovery_chain and not force:
            logger.warning("[WARNING] Recovery chain already exists. Overwrite prevented.")
            return False  #  Do not overwrite unless forced
    
        # Validate all functions before assignment
        valid_recovery_chain = []
        for recovery_func in recovery_sequence:
            func = getattr(self.viz, recovery_func, None) or getattr(self, recovery_func, None)
    
            if not callable(func):
                logger.error(f"[ERROR] Recovery function '{recovery_func}' not found or not callable.")
                continue  #  Skip invalid functions
    
            valid_recovery_chain.append(recovery_func)
            logger.debug(f"[RECOVERY FUNCTION] Added '{recovery_func}' to recovery chain.")
    
        if not valid_recovery_chain:
            logger.critical("[ERROR] No valid recovery functions found. Recovery chain not set.")
            return False  #  Fail if no valid functions
    
        # Assign validated recovery chain
        self.recovery_chain = valid_recovery_chain
        logger.info("[RECOVERY SET] Recovery sequence successfully defined.")
        
        return True  #  Indicate success

    # ---------------------------------------------------------------------------------------------------------------
    def save_to_file(self, file_path):
        """Saves the current state machine to a JSON file."""

        logger.info(f"[SAVE] Saving state machine to file: {file_path}")

        try:
            state_data = {
                "state_transitions": self.state_transitions,
                "param_to_state": self.param_to_state,
                "state_priorities": self.state_priorities,
                "state_sequences": self.state_sequences,
                "recovery_chain": self.recovery_chain
            }

            with open(file_path, "w") as f:
                json.dump(state_data, f, indent=4)

            logger.info(f"[SAVE SUCCESS] State machine successfully saved to '{file_path}'.")

        except Exception as e:
            logger.critical(f"[SAVE ERROR] Failed to save state machine to '{file_path}': {e}", exc_info=True)

    # ---------------------------------------------------------------------------------------------------------------
    def load_from_file(self, file_path):
        """Loads the state machine from a JSON file."""

        logger.info(f"[LOAD] Loading state machine from file: {file_path}")

        try:
            with open(file_path, "r") as f:
                state_data = json.load(f)

            # Log before applying state data
            logger.debug(f"[LOAD DEBUG] Loaded state data from '{file_path}': {state_data}")

            self.state_transitions = state_data.get("state_transitions", {})
            self.param_to_state    = state_data.get("param_to_state", {})
            self.state_priorities  = state_data.get("state_priorities", {})
            self.state_sequences   = state_data.get("state_sequences", {})
            self.recovery_chain    = state_data.get("recovery_chain", [])

            self.verify_provisioning()

            logger.info(f"[LOAD SUCCESS] State machine successfully loaded from '{file_path}'.")

        except FileNotFoundError:
            logger.error(f"[LOAD ERROR] File '{file_path}' not found.")
        except json.JSONDecodeError:
            logger.critical(f"[LOAD ERROR] Failed to parse JSON from '{file_path}'.", exc_info=True)
        except Exception as e:
            logger.critical(f"[LOAD ERROR] Unexpected error while loading from '{file_path}': {e}", exc_info=True)

    # ---------------------------------------------------------------------------------------------------------------
    def validate_transitions(self):
        """Validates all defined state transitions and execution sequences for consistency."""

        logger.info("[VALIDATION] Starting transition validation.")

        invalid_transitions = []
        invalid_sequences = []

        #  Validate state transitions
        for (param, start_state), end_state in self.state_transitions.items():
            logger.debug(f"[VALIDATION CHECK] '{param}': {start_state} → {end_state}")

            if end_state not in self.state_sequences:
                logger.error(f"[INVALID TRANSITION] '{param}': {start_state} → {end_state} (End state undefined).")
                invalid_transitions.append((param, start_state, end_state))

        #  Validate function execution sequences
        for state, sequence in self.state_sequences.items():
            execution_chain = sequence.get("functions", [])

            if not isinstance(execution_chain, list):
                logger.warning(f"[INVALID SEQUENCE] Execution sequence for state '{state}' is not a list.")
                invalid_sequences.append(state)
                continue  # Skip further validation for this state

            for func in execution_chain:
                if not isinstance(func, str):
                    logger.warning(f"[INVALID FUNCTION] Function '{func}' in state '{state}' should be a string.")
                    invalid_sequences.append(state)

        #  Final validation result
        if invalid_transitions or invalid_sequences:
            logger.warning(f"[VALIDATION FAILED] {len(invalid_transitions)} invalid transitions, {len(invalid_sequences)} invalid sequences.")
            for param, start, end in invalid_transitions:
                logger.debug(f"[INVALID] '{param}': {start} → {end} (End state undefined).")
            return False

        logger.info("[VALIDATION SUCCESS] All transitions and function sequences are valid.")
        return True

    # ---------------------------------------------------------------------------------------------------------------
    # EGB FIX  need to add graphviz output
    def debug_execution_flow(self):
        """Logs the execution flow structure for debugging purposes."""

        logger.info("[DEBUG FLOW] Execution flow analysis started.")

        # Log graph details
        node_count = len(self.execution_graph.nodes())
        edge_count = len(self.execution_graph.edges())
        logger.debug(f"[GRAPH INFO] Execution graph has {node_count} nodes and {edge_count} edges.")

        # Log each transition dependency
        if edge_count == 0:
            logger.warning("[GRAPH EMPTY] No execution dependencies found.")

        for start, end in self.execution_graph.edges():
            logger.debug(f"[DEPENDENCY] {start} → {end}")

        logger.info("[DEBUG FLOW COMPLETE] Execution flow analysis finished.")

    # ---------------------------------------------------------------------------------------------------------------
    def debug_console(self):
        """Launches an interactive debug console."""
        print("\nEnter 'exit' to quit the debug console.")
        while True:
            cmd = input("Debug> ").strip().lower()
            if cmd == "exit":
                break
            elif cmd == "states":
                print(f"Current State: {self.current_state}")
            elif cmd == "params":
                print(f"Parameters: {self.param_to_state}")
            elif cmd == "graph":
                self.debug_execution_flow()
            else:
                print("Unknown command. Try: states, params, graph, exit.")  # Fix: Ensured valid commands


# ===================================================================================================================
#if __name__ == "__main__":
#    """
#    Example usage of the ParamStateMachine state machine with a Panel-based GUI.
#
#    - Initializes a mock visualization system with Panel and Param.
#    - Sets up parameter-state mappings.
#    - Defines execution chains for rendering updates.
#    - Provides an interactive UI for triggering state changes.
#    - Ensures execution terminates within 10 seconds.
#    """
#
#    import asyncio
#    import panel as pn
#    import param
#
#    class MockVisualization(param.Parameterized):
#        """Mock visualization class with Panel-viewable parameters."""
#
#        node_color = param.Integer(default=1, bounds=(1, 10), label="Node Color")
#        edge_color = param.Integer(default=2, bounds=(1, 10), label="Edge Color")
#        status = param.String(default="Idle", label="Status")
#
#        def __init__(self, state_machine):
#            super().__init__()
#            self.state_machine = state_machine
#            self.state_machine.add_param("node_color", ParamStateMachine.State.FINAL_RENDER)
#            self.state_machine.add_param("edge_color", ParamStateMachine.State.FINAL_RENDER)
#            self.state_machine.add_execution_chain(ParamStateMachine.State.FINAL_RENDER, ["_set_node_color", "view"])
#            self.state_machine.add_execution_chain(ParamStateMachine.State.FINAL_RENDER, ["_update_nodes", "_set_node_color", "view"])
#
#        def _set_node_color(self):
#            """Mock function for setting node color."""
#            self.status = "Updating Node Color"
#            print("Executed: _set_node_color")
#
#        def _update_nodes(self):
#            """Mock function for updating nodes."""
#            self.status = "Updating Nodes"
#            print("Executed: _update_nodes")
#
#        def view(self):
#            """Mock function for updating visualization."""
#            self.status = "Rendering View"
#            print("Executed: view")
#
#        def trigger_update(self, event):
#            """Callback function to trigger state machine execution."""
#            self.state_machine.param_changed(event.name)
#
#    async def main():
#        """Main execution function with enforced timeout."""
#        state_machine = ParamStateMachine(viz=None)
#        viz           = MockVisualization(state_machine)
#
#        # Create a Panel UI
#        ui = pn.Column(
#            "# State Machine Example",
#            pn.Param(viz, widgets={"node_color": pn.widgets.IntSlider,
#                                   "edge_color": pn.widgets.IntSlider}),
#            viz.param.status,
#            "### Controls",
#            pn.widgets.Button(name="Trigger Node Update", button_type="primary",
#               on_click=lambda: viz.trigger_update(param.parameterized.Event(name="node_color"))),
#            pn.widgets.Button(name="Trigger Edge Update", button_type="primary",
#               on_click=lambda: viz.trigger_update(param.parameterized.Event(name="edge_color"))),
#        )
#
#        print("\n--- Event loop. It will run for 5 secs ---")
#        try:
#            await asyncio.wait_for(asyncio.sleep(5), timeout=5)
#        except asyncio.TimeoutError:
#            print("\n Execution timed out after 5 seconds.")
#
#        # Debugging output
#        state_machine.debug_execution_flow()
#
#        # Save and reload state machine
#        state_machine.save_to_file("state_machine.json")
#        print("\nState machine saved.")
#
#        loaded_state_machine = ParamStateMachine.load_from_file("state_machine.json")
#        print("\nState machine reloaded.")
#
#        # Interactive debug console with timeout
#        print("\nStarting debug console (auto-exits in 10 sec)...")
#        try:
#            await asyncio.wait_for(asyncio.to_thread(state_machine.debug_console), timeout=10)
#        except asyncio.TimeoutError:
#            print("\nDebug console auto-exited after 10 seconds.")
#
#    # Run the main function with timeout
#    asyncio.run(main())
#
#13-14, 114-116, 120-121, 125-130, 134-135,
# 259-262, 369,
# 426-427, 455-456, 511, 518-530
